# Liste des licences utilisées sur le projet

## Licences Logiciel

* CEA CNRS INRIA Licence Libre — CeCILL v2.1
* GNU General Public Licence version 3

## Licences Hardware

* CERN Open Hardware Licence version 1.2

## Licences Documentation

* Creative Commons CC-BY-SA version 3
 
