<br/>
<p style="font-size:40px; font-weight:900; color:#FFF; text-align:center;">README</p>

--------

**Table des Matières**{style="font-size:x-large;"}

[[TOC]]

# Présentation

Ce projet d'IHM est un projet Libre et OpenHardWare, développé (quasiment, sauf à des fins pédagogiques) uniquement avec des logiciels libre, conjointement avec le service électronique du laboratoire LAPLACE du CNRS de Toulouse et un apprenti technicien supérieur issu d'une formation ETD (Électronicien de Test et de Développement) à Balma.

!!! info Informations à prendre en compte

    Le projet étant en cours de développement, certaines partie de ce README, ainsi que du reste du projet sont, soit, incomplète, soit en cours de rédaction, de concéption ou d'élaboration. Prenez donc garde aux mise-à-jour et à la stabilité du projet.
    
    !!! warning Attention
        Certaines parties ne concernent que la présentation du concepteur à  sa soutenance, ne les prenez donc surtout pas en compte !!!

## Présentation et description du projet d'IHM

Le Service Commun d'Électronique (Ci-après nommé SCE) du Laboratoire LAPLACE utilise dans de nombreux projet le kit de developpement PSoC-5LP avec un écran 4DSystems afin de réaliser des IHM. Or, afin de simplifier le developpement de ces projets, il a été proposé de developper une carte qui permette de réaliser plus rapidement et de façon standardisée une IHM.\
Ce projet est donc spécifiquement développé pour les µCU PSoC, mais pourrait être assez facilement adaptable à d'autres plateformes. Le cœur de celui-ci étant l'écran 4DSystems.

## Description mécanique

La carte fait une taille de **100 × 70 mm** et a été conçu pour pouvoir être intégré dans un boîtier faisant la taille de l'écran.\
Niveau mécanique, il peut être composé de :
1. Un écran µLCD 4DSystems pouvant être contrôlé :
    * En autonome par un µCU externe via une communication UART
    * Par un µCU externe et des fichiers inclus dans un mémoire propre au µLCD
    * En autonomie complète via le µCU **Diablo16** du µLCD
2. La carte principale peuplée ou non :
    * D'un µCU PSoC **Maître** ou **Métier**
    * D'un encodeur rotatif intégré ou déporté
    * D'un anneau de LED NeoPixel
    * D'un amplificateur Audio Mono 1,2 W avec sortie déportée
    * D'un connecteur pour bouton de **RESET** déporté
3. De cartes d'extension au format **50 × 70 mm** (format ${1 \over 2}$) communicant par :
    * I²C via un extendeur ou via un ADC/I²C
    * N'importe quel protocol de communication série ou parallèle via les broches du µCU déportées sur les différents connecteurs
4. Un rail de fixation
    * Préperçé en acier inoxydable de **12,7 mm²**
    * Ou via un model 3D d'impression par addition, imprimable par une imprimante de taille > 120 mm.

## Description Logicielle

Des librairies pour le µLCD et pour le µCU PSoC seront mis à disposition afin de simplifier le développement d'une mise en pratique de l'IHM, avec des exemple, une documentation et des fonctions facilitant grandement sa mise en œuvre.

[^1]: Here is the footnote.
[^longnote]: Here's one with multiple blocks.
Here is a footnote reference,[1] and another.[2]


*[SCE]: Service Commun d'Électronique du Laboratoire LAPLACE
*[LAPLACE]: LAboratoire PLAsma et Conversion d'Énergie
*[IHM]: Interface Homme-Machine
*[Diablo16]: µCU interne de l'écran 4DSystems
*[NeoPixel]: LEDs RGB avec un driver 3 × 8 bits intégré

___________

# Gestion du projet

## Gestion des numéros de version

Les numéros des version se compose comme suit:
v1.2-3.4:
1.  Numéro de la version de la carte (actuellement 0 mais en passe de devenir 1);
2. Numéro de release correspondant à la phase de développement et à la semaine en cours(actuellement la 1)
3. Numéro de Phase (actuellement phase 0 : Fixation du cahier des charges, va passé en phase 1 : Choix de la pré-BOM, puis en phase 3 dans la foulée avec le développement du PCB puis fabrication);
4. Numéro de semaine en cours;

## Phases de développement
- [x] 0. Reflexion sur les contraintes et ojectifs du PCB
- [x] 1. Choix d'une pré-BOM
- [x] 2. Développement du PCB
- [x] 3. Validation et fabrication du PCB
- [x] 4. Réflexion sur les contraintes et objectifs des librairies logiciels
- [ ] 5. Écritures des librairies d'utilisation du µLCD, des NeoPixels et de l'encodeur rotatif
- [ ] 6. Développement d'une carte d'extension
- [ ] 7. Écriture d'une librairie pour une carte d'extension via un extendeur I²C
- [ ] 8. Test
- [ ] 9. Écriture de la documentation technique et utilisateur
- [ ] 10. Rédaction du rapport de stage (+ préparation du Dossier Pro + Présentation)
- [ ] 11. Écriture de Librairies supplémentaires

### Objectifs temporels


| Semaine    | 1     | 2     | 3     | 4         | 5         | 6         | 7          | 8         |
| ---------- | ----- | ----- | ----- | --------- | --------- | --------- | ---------- | --------- |
| Objectif   | 0 + 1 | 1 + 2 | 2 + 3 | 4 + 5     | 5         | 6 + 7     | 8 + 9 + 10 | 10 + 11   |
| Phases     | 0 + 1 | 1 + 2 | 2 + 3 | 2 + 3 + 4 |           |           |            |           |
| Validation | [[√]] | [[√]] | [[√]] | [[X]]     | [[TBD]] | [[TBD]] | [[¿TBD?]]  | [[?TBD¿]] |

#### Remarques

!!! fail Calendrier invalidé
    Le developpement du PCB, via tous les aller-retours de propositions et de modifications a prit bien plus de temps que prévu, tant qu'il va falloir rattraper en accélerant le developpement des librairie et en rognant peut-être sur la phase 11.

## ToolChain

### Logiciels de CAO

#### Modèlisation 3D

* FreeCAD
* AutoDesk Inventor© (Logiciel privateur testé à des fins pédagogiques)

#### Conception du PCB (ECAD)

* KiCAD : Pour le gros du travail, la carte principale
* Altium Designer : À la demande du SCE pour les cartes d'extension afin qu'ils puissent retravailller les fichiers

### Logiciel de programmation / IDE

#### Programmation du PSoC

* PSoC Creator© (logiciel privateur propre au developpement sur les µCU PSoC) avec les librairies pour la famille 058/059-5LP
* Code-OSS (version libre de VS-Code)
* Vim

#### Programmation de l'écran 4DSystems

!!! note  _***À compléter***_

### Gestion de projet

#### Versionning

* Git avec un dépôt GitLab sur [Framagit](https://framagit.org/ihm_generic_4dsystems/Projet)
* Génération du projet via un script shell écrit par mes soins disponible sur [Framagit](https://framagit.org/Troupal/mkproject)

!!! warning **Attention**
    Le projet **mkproject** n'est qu'en version α et à besoin d'être révisé. Toute contribution est la bienvenue !

____________

# Logiciel

## Librairies

!!! info Cette partie n'est pas encore complétée
