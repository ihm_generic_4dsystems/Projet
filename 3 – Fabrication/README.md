# Dossier de Fabrication

------

**Ici se trouvent tous les éléments ayant à faire à la fabrication. Autrement dit, tous les fichiers qui vont servir aux différentes entreprises pour fabriquer les différentes parties de notre projets**

Par exemple, c'est ici que l'on trouvera les fichier gerber pour la fabrication du PCB ou les fichiers de perçage et d'impression 3D pour l'atelier mécanique.
