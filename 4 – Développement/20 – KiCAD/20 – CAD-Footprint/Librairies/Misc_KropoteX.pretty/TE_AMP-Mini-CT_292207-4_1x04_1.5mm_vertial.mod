PCBNEW-LibModule-V1  2019-09-24 17:10:19
# encoding utf-8
Units mm
$INDEX
TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial
$EndINDEX
$MODULE TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial
Po 0 0 0 15 5d8a3feb 00000000 ~~
Li TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial
Cd TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial"
DS -2 -3.1 -2 1.6 0.2 24
DS -2 1.6 6.5 1.6 0.2 24
DS 6.5 1.6 6.5 -3.1 0.2 24
DS 6.5 -3.1 -2 -3.1 0.2 24
DS -2 -3.1 -2 1.6 0.2 21
DS -2 1.6 6.5 1.6 0.2 21
DS 6.5 1.6 6.5 -3.1 0.2 21
DS 6.5 -3.1 -2 -3.1 0.2 21
$PAD
Po 0 0
Sh "1" C 1.5 1.5 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 1.5 -2
Sh "2" C 1.5 1.5 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3 0
Sh "3" C 1.5 1.5 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 4.5 -2
Sh "4" C 1.5 1.5 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -1.5 -1.5
Sh "5" C 1 1 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial
$EndLIBRARY
