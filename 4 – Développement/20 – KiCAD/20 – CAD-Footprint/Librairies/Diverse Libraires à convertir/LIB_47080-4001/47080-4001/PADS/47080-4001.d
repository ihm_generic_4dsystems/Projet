*PADS-LIBRARY-PCB-DECALS-V9*

47080-4001       M 2.687 -10.733 0 2 4 0 9 3 0
TIMESTAMP 2017.01.11.10.01.46
-0.498 8.151 0     0 0.381 0.0381 1 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-0.498 8.151 0     0 0.381 0.0381 1 32 35 "Regular <Romansim Stroke Font>"
PART-TYPE
CLOSED 5 0.2 27 -1
-8.77 11.56
8.77  11.56
8.77  0    
-8.77 0    
-8.77 11.56
OPEN   5 0.2 26 -1
-8.77 11.56
-8.77 0    
8.77  0    
8.77  11.56
4.593 11.56
OPEN   2 0.2 26 -1
-8.77 11.56
-4.611 11.56
CIRCLE 2 0.2 26 -1
4.482 12.353
4.66  12.353
T3.81  10.76 3.81  10.76 1
T2.54  10.76 2.54  10.76 2
T1.27  10.76 1.27  10.76 3
T0     10.76 0     10.76 4
T-1.27 10.76 -1.27 10.76 5
T-2.54 10.76 -2.54 10.76 6
T-3.81 10.76 -3.81 10.76 7
T-6.37 8.4   -6.37 8.4   8
T6.37  8.4   6.37  8.4   9
PAD 0 3 N 0    
-2 0.9 RF  0   90   2  0  
-1 0   R  
0  0   R  
PAD 8 3 P 1.55 
-2 2.35 R  
-1 2.35 R  
0  2.35 R  
PAD 9 3 P 1.55 
-2 2.35 R  
-1 2.35 R  
0  2.35 R
*END*
