PCBNEW-LibModule-V1  2019-09-24 23:28:30
# encoding utf-8
Units mm
$INDEX
Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal
$EndINDEX
$MODULE Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal
Po 0 0 0 15 5d8a988e 00000000 ~~
Li Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal
Cd Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 -0.498 -8.151 1.27 1.27 0 0.254 N V 21 N "J**"
T1 -0.498 -8.151 1.27 1.27 0 0.254 N I 21 N "Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal"
DS -8.77 -11.56 8.77 -11.56 0.2 24
DS 8.77 -11.56 8.77 0 0.2 24
DS 8.77 0 -8.77 0 0.2 24
DS -8.77 0 -8.77 -11.56 0.2 24
DS -8.77 -11.56 -8.77 0 0.2 21
DS -8.77 0 8.77 0 0.2 21
DS 8.77 0 8.77 -11.56 0.2 21
DS 8.77 -11.56 4.593 -11.56 0.2 21
DS -8.77 -11.56 -4.611 -11.56 0.2 21
DC 4.571 -12.353 4.482 -12.353 0.254 21
$PAD
Po 3.81 -10.76
Sh "1" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.54 -10.76
Sh "2" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.27 -10.76
Sh "3" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 -10.76
Sh "4" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.27 -10.76
Sh "5" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2.54 -10.76
Sh "6" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3.81 -10.76
Sh "7" R 0.9 2 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -6.37 -8.4
Sh "8" C 2.35 2.35 0 0 900
Dr 1.55 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 6.37 -8.4
Sh "9" C 2.35 2.35 0 0 900
Dr 1.55 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE Molex_SATA_47080-4001_1x07_1.27mm_SMD_Horizontal
$EndLIBRARY
