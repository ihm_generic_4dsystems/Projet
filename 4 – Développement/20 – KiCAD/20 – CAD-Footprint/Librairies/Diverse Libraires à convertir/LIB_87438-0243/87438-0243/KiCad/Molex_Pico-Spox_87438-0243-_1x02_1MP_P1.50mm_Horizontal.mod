PCBNEW-LibModule-V1  2019-09-25 01:07:50
# encoding utf-8
Units mm
$INDEX
Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal
$EndINDEX
$MODULE Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal
Po 0 0 0 15 5d8aafd6 00000000 ~~
Li Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal
Cd Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal
Kw Connector
Sc 0
At SMD
AR 
Op 0 0 0
T0 0.000 -0 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0.000 -0 1.27 1.27 0 0.254 N I 21 N "Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal"
DS -4.05 -2.15 1.7 -2.15 0.2 24
DS 1.7 -2.15 1.7 2.15 0.2 24
DS 1.7 2.15 -4.05 2.15 0.2 24
DS -4.05 2.15 -4.05 -2.15 0.2 24
DS -5.05 -3.15 5.05 -3.15 0.1 24
DS 5.05 -3.15 5.05 3.15 0.1 24
DS 5.05 3.15 -5.05 3.15 0.1 24
DS -5.05 3.15 -5.05 -3.15 0.1 24
DS 1.7 -2.15 -4.05 -2.15 0.1 21
DS -4.05 -2.15 -4.05 2.15 0.1 21
DS -4.05 2.15 1.4 2.15 0.1 21
DS 1.4 2.15 1.7 2.15 0.1 21
$PAD
Po 2.725 -0.75
Sh "1" R 0.850 2.650 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.175 -0.75
Sh "2" R 0.850 3.550 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.725 0.75
Sh "3" R 0.850 2.650 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.175 0.75
Sh "4" R 0.850 3.550 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal
$EndLIBRARY
