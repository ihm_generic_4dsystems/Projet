*PADS-LIBRARY-PART-TYPES-V9*

67800-5025 67800-5005_1 I CON 7 1 0 0 0
TIMESTAMP 2019.09.25.00.09.12
"RS Part Number" 8967447
"RS Price/Stock" http://uk.rs-online.com/web/p/products/8967447
"Manufacturer_Name" Molex
"Manufacturer_Part_Number" 67800-5025
"Allied_Number" 70747057
"Description" Serial SATA 1.27mm SMT PCB header, 7 way Molex 67800 Series, 1.27mm Pitch 7 Way 1 Row Vertical PCB Header, Solder Termination, 1.5A
"Datasheet Link" http://uk.rs-online.com/web/p/products/8967447
GATE 1 7 0
67800-5025
1 0 G 1
3 0 S 3
5 0 L 5
7 0 G 7
2 0 S 2
4 0 G 4
6 0 L 6

*END*
*REMARK* SamacSys ECAD Model
381256/175486/2.43/7/4/Connector
