(part "CY8C5888LTI-LP097"
    (packageRef "QFN40P800X800X100-69N")
    (interface
        (port "1" (symbPinId 1) (portName "(TRACEDATA[2],_GPIO)_P2[6]") (portType INOUT))
        (port "2" (symbPinId 2) (portName "(TRACEDATA[3],_GPIO)_P2[7]") (portType INOUT))
        (port "3" (symbPinId 3) (portName "(I2C0:_SCL,_SIO)_P12[4]") (portType INOUT))
        (port "4" (symbPinId 4) (portName "(I2C0:_SDA,_SIO)_P12[5]") (portType INOUT))
        (port "5" (symbPinId 5) (portName "VSSB") (portType GROUND))
        (port "6" (symbPinId 6) (portName "IND") (portType INPUT))
        (port "7" (symbPinId 7) (portName "VBOOST") (portType INOUT))
        (port "8" (symbPinId 8) (portName "VBAT") (portType INOUT))
        (port "9" (symbPinId 9) (portName "VSSD_1") (portType GROUND))
        (port "10" (symbPinId 10) (portName "%XRES%") (portType INOUT))
        (port "11" (symbPinId 11) (portName "(TMS,_SWDIO,_GPIO)_P1[0]") (portType INOUT))
        (port "12" (symbPinId 12) (portName "(TCK,_SWDCK,_GPIO)_P1[1]") (portType INOUT))
        (port "13" (symbPinId 13) (portName "(CONFIGURABLE_XRES,_GPIO)_P1[2]") (portType INOUT))
        (port "14" (symbPinId 14) (portName "(TDO,_SWV,_GPIO)_P1[3]") (portType INOUT))
        (port "15" (symbPinId 15) (portName "(TDI,_GPIO)_P1[4]") (portType INOUT))
        (port "16" (symbPinId 16) (portName "(NTRST,_GPIO)_P1[5]") (portType INOUT))
        (port "17" (symbPinId 17) (portName "VDDIO1") (portType POWER))
        (port "18" (symbPinId 18) (portName "(GPIO)_P1[6]") (portType INOUT))
        (port "19" (symbPinId 19) (portName "(GPIO)_P1[7]") (portType INOUT))
        (port "20" (symbPinId 20) (portName "(SIO)_P12[6]") (portType INOUT))
        (port "21" (symbPinId 21) (portName "(SIO)_P12[7]") (portType INOUT))
        (port "22" (symbPinId 22) (portName "(USBIO,_D+,_SWDIO)_P15[6]") (portType INOUT))
        (port "23" (symbPinId 23) (portName "(USBIO,_D-,_SWDCK)_P15[7]") (portType INOUT))
        (port "24" (symbPinId 24) (portName "VDDD_1") (portType POWER))
        (port "25" (symbPinId 25) (portName "VSSD_2") (portType GROUND))
        (port "26" (symbPinId 26) (portName "VCCD_1") (portType POWER))
        (port "27" (symbPinId 27) (portName "(MHZ_XTAL:_XO,_GPIO)_P15[0]") (portType INOUT))
        (port "28" (symbPinId 28) (portName "(MHZ_XTAL:_XI,_GPIO)_P15[1]") (portType INOUT))
        (port "29" (symbPinId 29) (portName "(IDAC1,_GPIO)_P3[0]") (portType INOUT))
        (port "30" (symbPinId 30) (portName "(IDAC3,_GPIO)_P3[1]") (portType INOUT))
        (port "31" (symbPinId 31) (portName "(OPAMP3-,_EXTREF1,_GPIO)_P3[2]") (portType INOUT))
        (port "32" (symbPinId 32) (portName "(OPAMP3+,_GPIO)_P3[3]") (portType INOUT))
        (port "33" (symbPinId 33) (portName "(OPAMP1-,_GPIO)_P3[4]") (portType INOUT))
        (port "34" (symbPinId 34) (portName "(OPAMP1+,_GPIO)_P3[5]") (portType INOUT))
        (port "35" (symbPinId 35) (portName "VDDIO3") (portType POWER))
        (port "36" (symbPinId 36) (portName "P3[6]_(GPIO,_OPAMP1OUT)") (portType INOUT))
        (port "37" (symbPinId 37) (portName "P3[7]_(GPIO,_OPAMP3OUT)") (portType INOUT))
        (port "38" (symbPinId 38) (portName "P12[0]_(SIO,_12C1:_SCL)") (portType INOUT))
        (port "39" (symbPinId 39) (portName "P12[1]_(SIO,_I2C1:_SDA)") (portType INOUT))
        (port "40" (symbPinId 40) (portName "P15[2]_(GPIO,_KHZ_XTAL:_XO)") (portType INOUT))
        (port "41" (symbPinId 41) (portName "P15[3]_(GPIO,_KHZ_XTAL:_XI)") (portType INOUT))
        (port "42" (symbPinId 42) (portName "VCCA") (portType POWER))
        (port "43" (symbPinId 43) (portName "VSSA") (portType GROUND))
        (port "44" (symbPinId 44) (portName "VDDA") (portType POWER))
        (port "45" (symbPinId 45) (portName "VSSD_3") (portType GROUND))
        (port "46" (symbPinId 46) (portName "P12[2]_(SIO)") (portType INOUT))
        (port "47" (symbPinId 47) (portName "P12[3]_(SIO") (portType INOUT))
        (port "48" (symbPinId 48) (portName "P0[0]_(GPIO,_OPAMP2OUT)") (portType INOUT))
        (port "49" (symbPinId 49) (portName "P0[1]_(GPIO,_OPAMP0OUT)") (portType INOUT))
        (port "50" (symbPinId 50) (portName "P0[2]_(GPIO,_OPAMP0+,_SAR1_EXTREF)") (portType INOUT))
        (port "51" (symbPinId 51) (portName "P0[3]_(GPIO,_OPAMP0-,_EXTREF0)") (portType INOUT))
        (port "52" (symbPinId 52) (portName "VDDIO0") (portType POWER))
        (port "53" (symbPinId 53) (portName "P0[4]_(GPIO,_OPAMP2+,_SAR0_EXTREF)") (portType INOUT))
        (port "54" (symbPinId 54) (portName "P0[5]_(GPIO,_OPAMP2-)") (portType INOUT))
        (port "55" (symbPinId 55) (portName "P0[6]_(GPIO,_IDAC0)") (portType INOUT))
        (port "56" (symbPinId 56) (portName "P0[7]_(GPIO,_IDAC2)") (portType INOUT))
        (port "57" (symbPinId 57) (portName "VCCD_2") (portType POWER))
        (port "58" (symbPinId 58) (portName "VSSD_4") (portType GROUND))
        (port "59" (symbPinId 59) (portName "VDDD_2") (portType POWER))
        (port "60" (symbPinId 60) (portName "P15[4]_(GPIO)") (portType INOUT))
        (port "61" (symbPinId 61) (portName "P15[5]_(GPOI)") (portType INOUT))
        (port "62" (symbPinId 62) (portName "P2[0]_(GPIO)") (portType INOUT))
        (port "63" (symbPinId 63) (portName "P2[1]_(GPIO)") (portType INOUT))
        (port "64" (symbPinId 64) (portName "P2[2]_(GPIO)") (portType INOUT))
        (port "65" (symbPinId 65) (portName "P2[3]_(GPIO,_TRACECLK)") (portType INOUT))
        (port "66" (symbPinId 66) (portName "P2[4]_(GPIO,_TRACEDATA[0])") (portType INOUT))
        (port "67" (symbPinId 67) (portName "VDDIO2") (portType POWER))
        (port "68" (symbPinId 68) (portName "P2[5]_(GPIO,_TRACEDATA[1])") (portType INOUT))
        (port "69" (symbPinId 69) (portName "EP_GND") (portType GROUND))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "101")
    (property "RS_Part_Number" "1242972P")
    (property "RS_Price/Stock" "http://uk.rs-online.com/web/p/products/1242972P")
    (property "Manufacturer_Name" "Cypress Semiconductor")
    (property "Manufacturer_Part_Number" "CY8C5888LTI-LP097")
    (property "Description" "ARM Microcontrollers - MCU 256KB Flash 64KBSRAM PSoC 5LP")
    (property "Datasheet_Link" "http://uk.rs-online.com/web/p/products/1242972P")
    (property "symbolName1" "CY8C5888LTI-LP097")
)
