*PADS-LIBRARY-PCB-DECALS-V9*

292207-4         M 0     0     0 2 2 0 5 2 0
TIMESTAMP 2015.12.02.15.15.29
0     0     0     0 1.27  0.127 1 32 35 "Regular <Romansim Stroke Font>"
PART-TYPE
0     0     0     0 1.27  0.127 1 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   5 0.2 27 -1
-2    3.1  
-2    -1.6 
6.5   -1.6 
6.5   3.1  
-2    3.1  
OPEN   5 0.2 26 -1
-2    3.1  
-2    -1.6 
6.5   -1.6 
6.5   3.1  
-2    3.1  
T0     0     0     0     1
T1.5   2     1.5   2     2
T3     0     3     0     3
T4.5   2     4.5   2     4
T-1.5  1.5   -1.5  1.5   5
PAD 0 3 P 1    
-2 1.5 R  
-1 1.5 R  
0  1.5 R  
PAD 5 3 N 1    
-2 1   R  
-1 1   R  
0  1   R
*END*
