(part "MINISMDC110F_16-2"
    (packageRef "FUSC4632X48N")
    (interface
        (port "1" (symbPinId 1) (portName "1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "2") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "RS_Part_Number" "")
    (property "RS_Price/Stock" "")
    (property "Manufacturer_Name" "TE Connectivity")
    (property "Manufacturer_Part_Number" "MINISMDC110F/16-2")
    (property "Allied_Number" "70059892")
    (property "Description" "Fuse SMD 1812 PolySwitch 1,1A TE Connectivity 1.1A Surface Mount Resettable Fuse, 16 V dc")
    (property "Datasheet_Link" "")
    (property "symbolName1" "MINISMDC110F_16-2")
)
