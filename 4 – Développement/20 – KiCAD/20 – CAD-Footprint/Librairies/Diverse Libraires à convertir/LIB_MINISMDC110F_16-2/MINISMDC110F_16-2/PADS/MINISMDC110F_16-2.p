*PADS-LIBRARY-PART-TYPES-V9*

MINISMDC110F_16-2 FUSC4632X48N I FUS 8 1 0 0 0
TIMESTAMP 2019.09.29.08.37.36
"RS Part Number" 
"RS Price/Stock" 
"Manufacturer_Name" TE Connectivity
"Manufacturer_Part_Number" MINISMDC110F/16-2
"Allied_Number" 70059892
"Description" Fuse SMD 1812 PolySwitch 1,1A TE Connectivity 1.1A Surface Mount Resettable Fuse, 16 V dc
"Datasheet Link" 
"Geometry.Height" 0.48mm
GATE 1 2 0
MINISMDC110F_16-2
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
330230/175486/2.43/2/4/Fuse
