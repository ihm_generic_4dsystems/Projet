# Librairies KiCAD 2D et 3D de composants non inclues de base

**Répertoire contenant un ensemble de librairies de composants qui ne sont pas inclues dans la librairie KiCAD de base, récupérés sur divers sites ou conçus par moi-même**

-----

## Liste de liens où trouver ces librairies

* https://rs.componentsearchengine.com/






-----

## Liste des composants

* CY8C5888LTI-LP097






-----

## Licence

**Voir le fichier LICENCE.md**

## Remerciement

**Je remercie RadioSpare pour son implication dans lle developpement et le partage de librairies de composants disponible sur le Logiciel libre KiCAD**
