EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
Text Label 5900 5100 2    50   ~ 0
GPIO12
Text Label 5900 5000 2    50   ~ 0
GPIO11
Text Label 5900 4900 2    50   ~ 0
GPIO10
Text Label 5900 4800 2    50   ~ 0
GPIO09
Text Label 5900 4700 2    50   ~ 0
GPIO08
Text Label 5900 4600 2    50   ~ 0
GPIO07
Text Label 5900 4500 2    50   ~ 0
GPIO06
Text Label 5900 4400 2    50   ~ 0
GPIO05
Text Label 5900 4300 2    50   ~ 0
GPIO04
Text Label 5900 4200 2    50   ~ 0
GPIO03
$Comp
L power:GND #PWR010
U 1 1 5DEC8254
P 7200 5600
F 0 "#PWR010" H 7200 5350 50  0001 C CNN
F 1 "GND" H 7205 5427 50  0000 C CNN
F 2 "" H 7200 5600 50  0001 C CNN
F 3 "" H 7200 5600 50  0001 C CNN
	1    7200 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4800 7400 4800
Wire Wire Line
	7400 4800 7400 5500
Text Label 7200 4600 0    50   ~ 0
GPIO13
Text Label 7200 4500 0    50   ~ 0
GPIO14
Text Label 7200 4400 0    50   ~ 0
GPIO15
Text Label 7200 4300 0    50   ~ 0
GPIO16
Text Label 7200 4200 0    50   ~ 0
GPIO17
Text HLabel 7150 5100 2    50   Input ~ 0
+5V
Text HLabel 7150 5000 2    50   Output ~ 0
TX0
Text HLabel 7150 4900 2    50   Input ~ 0
RX0
Text HLabel 7150 4700 2    50   Input ~ 0
RESET
Text HLabel 7150 4100 2    50   Output ~ 0
AUDIONB
Text HLabel 7150 4000 2    50   Output ~ 0
AUDIO-OUT
Wire Wire Line
	6950 5000 7150 5000
Wire Wire Line
	7150 4900 6950 4900
Wire Wire Line
	7150 4700 6950 4700
Wire Wire Line
	7150 4100 6950 4100
Wire Wire Line
	6950 4000 7150 4000
$Comp
L Connector_Generic:Conn_01x12 J5
U 1 1 5DB52F51
P 6750 4500
F 0 "J5" H 6700 5100 50  0000 L CNN
F 1 "4DS_R_prop2" V 6850 4250 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 6750 4500 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ec/0900766b816ec808.pdf" H 6750 4500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 6750 4500 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 6750 4500 50  0001 C CNN "Catégorie"
F 6 "267-7400" H 6750 4500 50  0001 C CNN "Code Fournisseur"
F 7 "5" H 6750 4500 50  0001 C CNN "Conditionnement"
F 8 "5 A" H 6750 4500 50  0001 C CNN "Courant"
F 9 "Connecteur Femelle, Traversant, 20 Contacts, 1 rangée, Droit, au pas de 2.54mm" H 6750 4500 50  0001 C CNN "Description"
F 10 "53,34×2,54×7.6mm" H 6750 4500 50  0001 C CNN "Height"
F 11 "Winslow" H 6750 4500 50  0001 C CNN "Manufacturer_Name"
F 12 " W35520TRC " H 6750 4500 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "2,54" H 6750 4500 50  0001 C CNN "Prix Conditionnement"
F 14 "0,508" H 6750 4500 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 6750 4500 50  0001 C CNN "Puissance"
F 16 "267-7400" H 6750 4500 50  0001 C CNN "RS Part Number"
F 17 "~" H 6750 4500 50  0001 C CNN "Tension"
F 18 "~" H 6750 4500 50  0001 C CNN "Tolérence"
F 19 "RS" H 6750 4500 50  0001 C CNN "Fournisseur"
	1    6750 4500
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x12 J4
U 1 1 5DB4D45A
P 6350 4500
F 0 "J4" H 6300 5100 50  0000 L CNN
F 1 "4DS_L_prop2" V 6450 4250 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 6350 4500 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ec/0900766b816ec808.pdf" H 6350 4500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 6350 4500 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 6350 4500 50  0001 C CNN "Catégorie"
F 6 "267-7400" H 6350 4500 50  0001 C CNN "Code Fournisseur"
F 7 "5" H 6350 4500 50  0001 C CNN "Conditionnement"
F 8 "5 A" H 6350 4500 50  0001 C CNN "Courant"
F 9 "Connecteur Femelle, Traversant, 20 Contacts, 1 rangée, Droit, au pas de 2.54mm" H 6350 4500 50  0001 C CNN "Description"
F 10 "53,34×2,54×7.6mm" H 6350 4500 50  0001 C CNN "Height"
F 11 "Winslow" H 6350 4500 50  0001 C CNN "Manufacturer_Name"
F 12 " W35520TRC " H 6350 4500 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "2,54" H 6350 4500 50  0001 C CNN "Prix Conditionnement"
F 14 "0,508" H 6350 4500 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 6350 4500 50  0001 C CNN "Puissance"
F 16 "267-7400" H 6350 4500 50  0001 C CNN "RS Part Number"
F 17 "~" H 6350 4500 50  0001 C CNN "Tension"
F 18 "~" H 6350 4500 50  0001 C CNN "Tolérence"
F 19 "RS" H 6350 4500 50  0001 C CNN "Fournisseur"
	1    6350 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EC63E73
P 7000 5300
AR Path="/5EC63E73" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5EC63E73" Ref="C?"  Part="1" 
AR Path="/5D7EDD13/5EC63E73" Ref="C14"  Part="1" 
F 0 "C14" H 7115 5346 50  0000 L CNN
F 1 "100n" H 7115 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7038 5150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 7000 5300 50  0001 C CNN
F 4 "698-3251" H 7000 5300 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 7000 5300 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 7000 5300 50  0001 C CNN "Description"
F 7 "RS" H 7000 5300 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 7000 5300 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 7000 5300 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 7000 5300 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 7000 5300 50  0001 C CNN "Tension"
F 12 "±10%" H 7000 5300 50  0001 C CNN "Tolérence"
F 13 "~" H 7000 5300 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 7000 5300 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 7000 5300 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 7000 5300 50  0001 C CNN "Height"
F 17 "AVX" H 7000 5300 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 7000 5300 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 7000 5300 50  0001 C CNN "RS Part Number"
	1    7000 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5450 7000 5500
Wire Wire Line
	7000 5500 7200 5500
Wire Wire Line
	7200 5500 7200 5600
Connection ~ 7200 5500
Wire Wire Line
	7200 5500 7400 5500
Wire Notes Line width 10
	3750 3400 8050 3400
Wire Notes Line width 10
	8050 3400 8050 5850
Wire Notes Line width 10
	8050 5850 3750 5850
Wire Notes Line width 10
	3750 5850 3750 3400
Text Notes 5000 3300 0    100  ~ 20
 Simple connecteur ×20
$Comp
L Connector_Generic:Conn_01x20 J3
U 1 1 5E7BDE9F
P 3900 4450
F 0 "J3" H 3900 5450 50  0000 C CNN
F 1 "4DS_ext_prop2" V 4000 4450 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Clasp_501190-2027_2x10_1MP_P1mm_Vertical" H 3900 4450 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/14f2/0900766b814f2c78.pdf" H 3900 4450 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/9048219/" H 3900 4450 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 3900 4450 50  0001 C CNN "Puissance"
F 6 "50 V c.c." H 3900 4450 50  0001 C CNN "Tension"
F 7 "~" H 3900 4450 50  0001 C CNN "Tolérence"
F 8 "RS" H 3900 4450 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 3900 4450 50  0001 C CNN "Catégorie"
F 10 "904-8219" H 3900 4450 50  0001 C CNN "Code Fournisseur"
F 11 "5" H 3900 4450 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 3900 4450 50  0001 C CNN "Courant"
F 13 "Embase pour CI Molex, Pico-Clasp, 20 pôles , 1mm, 2 rangées , 1A, Verticale" H 3900 4450 50  0001 C CNN "Description"
F 14 "1mm" H 3900 4450 50  0001 C CNN "Height"
F 15 "Molex" H 3900 4450 50  0001 C CNN "Manufacturer_Name"
F 16 "501190-2027" H 3900 4450 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "2,46" H 3900 4450 50  0001 C CNN "Prix Conditionnement"
F 18 "0,492" H 3900 4450 50  0001 C CNN "Prix Unitaire"
F 19 "904-8219" H 3900 4450 50  0001 C CNN "RS Part Number"
	1    3900 4450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4450 3550 4100 3550
Wire Wire Line
	4450 3650 4100 3650
Wire Wire Line
	4450 3750 4100 3750
Wire Wire Line
	4450 3850 4100 3850
Wire Wire Line
	4450 3950 4100 3950
Wire Wire Line
	4450 4050 4100 4050
Wire Wire Line
	4450 4150 4100 4150
Wire Wire Line
	4450 4350 4100 4350
Wire Wire Line
	4450 4450 4100 4450
Wire Wire Line
	4450 4650 4100 4650
Wire Wire Line
	4450 4750 4100 4750
Wire Wire Line
	4450 4950 4100 4950
Wire Wire Line
	4450 5150 4100 5150
Wire Wire Line
	4450 5250 4100 5250
Wire Wire Line
	4450 5350 4100 5350
Wire Wire Line
	4450 5450 4100 5450
Text Label 4450 3550 0    50   ~ 0
GPIO16
Text Label 4450 4050 0    50   ~ 0
GPIO15
Text Label 4450 3750 0    50   ~ 0
GPIO17
Text Label 4450 3650 0    50   ~ 0
GPIO14
Text Label 4450 3850 0    50   ~ 0
GPIO13
Text Label 4450 3950 0    50   ~ 0
GPIO01
Text Label 4450 4150 0    50   ~ 0
GPIO02
Text Label 4450 4250 0    50   ~ 0
GND
Text Label 4450 4350 0    50   ~ 0
GPIO03
Text Label 4450 5450 0    50   ~ 0
GPIO12
Text Label 4450 4550 0    50   ~ 0
GND
Text Label 4450 5250 0    50   ~ 0
GPIO11
Text Label 4450 4750 0    50   ~ 0
GPIO04
Text Label 4450 5050 0    50   ~ 0
GND
Text Label 4450 4950 0    50   ~ 0
GPIO05
Text Label 4450 4850 0    50   ~ 0
GPIO10
Text Label 4450 5150 0    50   ~ 0
GPIO06
Text Label 4450 4650 0    50   ~ 0
GPIO09
Text Label 4450 5350 0    50   ~ 0
GPIO07
Text Label 4450 4450 0    50   ~ 0
GPIO08
$Comp
L power:GND #PWR0101
U 1 1 5E823577
P 4800 5600
F 0 "#PWR0101" H 4800 5350 50  0001 C CNN
F 1 "GND" H 4805 5427 50  0000 C CNN
F 2 "" H 4800 5600 50  0001 C CNN
F 3 "" H 4800 5600 50  0001 C CNN
	1    4800 5600
	1    0    0    -1  
$EndComp
Text Label 7150 4800 0    50   ~ 0
GND
Text Label 7000 5100 0    50   ~ 0
VCC
Text Label 5900 4000 2    50   ~ 0
GPIO01
Text Label 5900 4100 2    50   ~ 0
GPIO02
Wire Wire Line
	6150 4000 5900 4000
Wire Wire Line
	6150 4100 5900 4100
Wire Wire Line
	6150 4200 5900 4200
Wire Wire Line
	6150 4300 5900 4300
Wire Wire Line
	6150 4400 5900 4400
Wire Wire Line
	6150 4500 5900 4500
Wire Wire Line
	6150 4600 5900 4600
Wire Wire Line
	6150 4700 5900 4700
Wire Wire Line
	6150 4800 5900 4800
Wire Wire Line
	6150 4900 5900 4900
Wire Wire Line
	6150 5000 5900 5000
Wire Wire Line
	6150 5100 5900 5100
Wire Wire Line
	7200 4200 6950 4200
Wire Wire Line
	7200 4300 6950 4300
Wire Wire Line
	7200 4400 6950 4400
Wire Wire Line
	7200 4500 6950 4500
Wire Wire Line
	7200 4600 6950 4600
Wire Wire Line
	6950 5100 7000 5100
Wire Wire Line
	7000 5150 7000 5100
Connection ~ 7000 5100
Wire Wire Line
	7000 5100 7150 5100
Text Notes 3800 1950 0    200  ~ 40
Connecteur µLCD 4DSystème
Wire Notes Line width 40 rgb(0, 185, 6)
	2700 2650 2700 1250
Text Notes 3400 2300 0    100  ~ 20
Avec déport des GPIOs sur connecteur 20 broches et système de reset
Wire Notes Line width 40 rgb(0, 185, 6)
	9350 2650 9350 1250
Wire Notes Line width 40 rgb(0, 185, 6)
	9350 2650 2700 2650
Wire Notes Line width 40 rgb(0, 185, 6)
	9350 1250 2700 1250
Wire Wire Line
	4100 4850 4450 4850
Wire Wire Line
	4800 4250 4800 4550
Wire Wire Line
	4100 4250 4800 4250
Wire Wire Line
	4100 4550 4800 4550
Connection ~ 4800 4550
Wire Wire Line
	4800 4550 4800 5050
Wire Wire Line
	4100 5050 4800 5050
Connection ~ 4800 5050
Wire Wire Line
	4800 5050 4800 5600
$EndSCHEMATC
