EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
Entry Wire Line
	3750 5800 3850 5700
Entry Wire Line
	3850 5800 3950 5700
Entry Wire Line
	3950 5800 4050 5700
Entry Wire Line
	3600 1550 3500 1450
Entry Wire Line
	3500 1550 3400 1450
Text Label 3400 1600 3    50   ~ 10
UART_Tx
Text Label 3300 1600 3    50   ~ 10
UART_Rx
Text Label 3100 1600 3    50   ~ 10
I2C0_SCL
Text Label 3350 5650 1    50   ~ 10
Prog_SWDCLK
Text Label 3250 5650 1    50   ~ 10
Prog_SWDIO
Text Label 3150 5650 1    50   ~ 10
I2C1_SCL
Text Label 3050 5650 1    50   ~ 10
I2C1_SDA
Entry Wire Line
	4000 1550 3900 1450
Text Label 4000 1600 3    50   ~ 0
XRES
Entry Wire Line
	2750 5800 2850 5700
Entry Wire Line
	2850 5800 2950 5700
Text Label 2950 5650 1    50   ~ 0
GPORST
Text Label 3800 1600 3    50   ~ 0
GPOAUDIO
Entry Wire Line
	3100 1450 3200 1550
Entry Wire Line
	3200 1450 3300 1550
Entry Wire Line
	3300 1450 3400 1550
Text Label 2850 5650 1    50   ~ 0
NP_DATA
Wire Wire Line
	1050 1850 1700 1850
Wire Wire Line
	1050 2000 1700 2000
Wire Wire Line
	1050 2150 1700 2150
Wire Wire Line
	1050 2300 1700 2300
Wire Wire Line
	1050 2450 1700 2450
Wire Wire Line
	1050 2600 1700 2600
Wire Wire Line
	1050 2750 1700 2750
Wire Wire Line
	1050 2900 1700 2900
Wire Wire Line
	1050 3050 1700 3050
Wire Wire Line
	1050 3200 1700 3200
Wire Wire Line
	1050 3350 1700 3350
Entry Wire Line
	950  5050 1050 5150
Entry Wire Line
	950  3250 1050 3350
Entry Wire Line
	950  3100 1050 3200
Entry Wire Line
	950  2950 1050 3050
Entry Wire Line
	950  2800 1050 2900
Entry Wire Line
	950  2650 1050 2750
Entry Wire Line
	950  2500 1050 2600
Entry Wire Line
	950  2350 1050 2450
Entry Wire Line
	950  2200 1050 2300
Entry Wire Line
	950  2050 1050 2150
Entry Wire Line
	950  1900 1050 2000
Entry Wire Line
	950  1750 1050 1850
Text HLabel 1700 1850 2    50   Output ~ 0
I2C0_SCL
Text HLabel 1700 2000 2    50   BiDi ~ 0
I2C0_SDA
Text HLabel 1700 2150 2    50   Input ~ 0
UART_Rx
Text HLabel 1700 2300 2    50   Output ~ 0
UART_Tx
Text HLabel 1700 2600 2    50   BiDi ~ 0
Prog_SWDIO
Text HLabel 1700 2450 2    50   Output ~ 0
Prog_SWDCLK
Text HLabel 1700 2750 2    50   Output ~ 0
I2C1_SCL
Text HLabel 1700 2900 2    50   BiDi ~ 0
I2C1_SDA
Text HLabel 1700 3050 2    50   Output ~ 0
P_TDI
Text HLabel 1700 3200 2    50   Input ~ 0
P_SWO
Text HLabel 1700 5000 2    50   Input ~ 0
VDD
Text Label 1100 1850 0    50   ~ 0
I2C0_SCL
Text Label 1100 2000 0    50   ~ 0
I2C0_SDA
Text Label 1100 2150 0    50   ~ 0
UART_Rx
Text Label 1100 2300 0    50   ~ 0
UART_Tx
Text Label 1100 2450 0    50   ~ 0
Prog_SWDCLK
Text Label 1100 2600 0    50   ~ 0
Prog_SWDIO
Text Label 1100 2750 0    50   ~ 0
I2C1_SCL
Text Label 1100 2900 0    50   ~ 0
I2C1_SDA
Text Label 1100 3050 0    50   ~ 0
P_TDI
Text Label 1100 3200 0    50   ~ 0
P_SWO
Text Label 1100 5150 0    50   ~ 0
GND
Text Label 1100 5000 0    50   ~ 0
VDD
$Comp
L power:GND #PWR011
U 1 1 5E0E0DE1
P 1700 5550
F 0 "#PWR011" H 1700 5300 50  0001 C CNN
F 1 "GND" H 1700 5350 50  0000 C CNN
F 2 "" H 1700 5550 50  0001 C CNN
F 3 "" H 1700 5550 50  0001 C CNN
	1    1700 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5150 1700 5150
Entry Wire Line
	950  3400 1050 3500
Wire Wire Line
	1050 3500 1700 3500
Text HLabel 1700 3350 2    50   Input ~ 0
XRES
Text Label 1100 3350 0    50   ~ 0
XRES
Text Label 1100 3650 0    50   ~ 0
GPOAUDIO
Text Label 1100 3800 0    50   ~ 0
GPORST
Entry Wire Line
	1050 3800 950  3700
Entry Wire Line
	1050 3650 950  3550
Wire Wire Line
	1050 3800 1700 3800
Wire Wire Line
	1050 3950 1700 3950
Text HLabel 1700 3650 2    50   Output ~ 0
AUDIO_out_PSoC
Text HLabel 1700 3800 2    50   Output ~ 0
Reset_PSoC
Entry Wire Line
	1050 3950 950  3850
Wire Wire Line
	1050 3650 1700 3650
Text HLabel 1700 3500 2    50   Output ~ 0
AUDIO_Enable_PSoC
Entry Wire Line
	950  4000 1050 4100
Entry Wire Line
	950  4150 1050 4250
Entry Wire Line
	950  4300 1050 4400
Entry Wire Line
	950  4450 1050 4550
Entry Wire Line
	950  4600 1050 4700
Entry Wire Line
	950  4750 1050 4850
Wire Wire Line
	1050 4100 1700 4100
Wire Wire Line
	1700 4250 1050 4250
Wire Wire Line
	1700 4400 1050 4400
Wire Wire Line
	1700 4550 1050 4550
Wire Wire Line
	1700 4850 1050 4850
Wire Wire Line
	1050 4700 1700 4700
Text Label 1100 3950 0    50   ~ 0
I2Cext2
Text Label 1100 3500 0    50   ~ 0
GPOAUDENA
Text Label 1100 4100 0    50   ~ 0
I2Cext1
Text Label 1100 4250 0    50   ~ 0
I2Cext0
Text Label 1100 4400 0    50   ~ 0
NP_DATA
Entry Wire Line
	3000 1450 3100 1550
Entry Wire Line
	3700 1550 3600 1450
Entry Wire Line
	3900 1550 3800 1450
Text Label 3600 1600 3    50   ~ 0
I2Cext1
Text Label 3700 1600 3    50   ~ 0
I2Cext0
Text Label 3500 1600 3    50   ~ 0
I2Cext2
Entry Wire Line
	3350 5800 3450 5700
Entry Wire Line
	3250 5800 3350 5700
Text Label 3900 1600 3    50   ~ 0
GPOAUDENA
Entry Wire Line
	2950 5800 3050 5700
Entry Wire Line
	3050 5800 3150 5700
Text Label 3450 5650 1    50   ~ 10
P_SWO
Text Label 3550 5650 1    50   ~ 10
P_TDI
Text Label 3950 5650 1    50   Italic 10
GND
Text Label 4050 5650 1    50   Italic 10
VDD
Entry Wire Line
	3550 5800 3650 5700
Entry Wire Line
	3450 5800 3550 5700
Entry Wire Line
	3650 5800 3750 5700
Entry Wire Line
	3150 5800 3250 5700
Wire Bus Line
	2550 5800 2550 1450
Wire Bus Line
	950  1450 2550 1450
Connection ~ 2550 1450
Text Label 3650 5650 1    50   ~ 0
ROTENSW
Text Label 3750 5650 1    50   ~ 0
ROTENA
Text Label 3850 5650 1    50   ~ 0
ROTENB
Text Label 1100 4550 0    50   ~ 0
ROTENSW
Text Label 1100 4700 0    50   ~ 0
ROTENA
Text Label 1100 4850 0    50   ~ 0
ROTENB
Entry Wire Line
	950  4900 1050 5000
Text HLabel 1700 3950 2    50   Output ~ 0
I2Cext2
Text HLabel 1700 4100 2    50   Output ~ 0
I2Cext1
Text HLabel 1700 4250 2    50   Output ~ 0
I2Cext0
Text HLabel 1700 4400 2    50   Output ~ 0
NP_DATA
Text HLabel 1700 4550 2    50   Input ~ 0
ROTENSW
Text HLabel 1700 4700 2    50   Input ~ 0
ROTENA
Text HLabel 1700 4850 2    50   Input ~ 0
ROTENB
$Comp
L Connector_Generic:Conn_01x26 J10
U 1 1 5D7FB7E5
P 7900 3550
F 0 "J10" H 7850 4850 50  0000 L CNN
F 1 "Conn_01x26_PSoC_R" V 8050 3300 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x26_P2.54mm_Vertical" H 7900 3550 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ec/0900766b816ec809.pdf" H 7900 3550 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 7900 3550 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 7900 3550 50  0001 C CNN "Puissance"
F 6 "~" H 7900 3550 50  0001 C CNN "Tension"
F 7 "~" H 7900 3550 50  0001 C CNN "Tolérence"
F 8 "Connecteur" H 7900 3550 50  0001 C CNN "Catégorie"
F 9 "267-7416" H 7900 3550 50  0001 C CNN "Code Fournisseur"
F 10 "5" H 7900 3550 50  0001 C CNN "Conditionnement"
F 11 "5 A" H 7900 3550 50  0001 C CNN "Courant"
F 12 "Connecteur Femelle, Traversant, 32 Contacts, 1 rangée, Droit, au pas de 2.54mm" H 7900 3550 50  0001 C CNN "Description"
F 13 "RS" H 7900 3550 50  0001 C CNN "Fournisseur"
F 14 "83,82×2,54×7.6mm" H 7900 3550 50  0001 C CNN "Height"
F 15 "Winslow" H 7900 3550 50  0001 C CNN "Manufacturer_Name"
F 16 "W35532TRC" H 7900 3550 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "6,00 " H 7900 3550 50  0001 C CNN "Prix Conditionnement"
F 18 "1,20" H 7900 3550 50  0001 C CNN "Prix Unitaire"
F 19 "267-7416" H 7900 3550 50  0001 C CNN "RS Part Number"
	1    7900 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x26 J6
U 1 1 5D7F4E1A
P 2950 3550
F 0 "J6" H 2950 4850 50  0000 C CNN
F 1 "Conn_01x26_PSoC_L" V 3100 3550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x26_P2.54mm_Vertical" H 2950 3550 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ec/0900766b816ec809.pdf" H 2950 3550 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 2950 3550 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 2950 3550 50  0001 C CNN "Puissance"
F 6 "~" H 2950 3550 50  0001 C CNN "Tension"
F 7 "~" H 2950 3550 50  0001 C CNN "Tolérence"
F 8 "Connecteur" H 2950 3550 50  0001 C CNN "Catégorie"
F 9 "267-7416" H 2950 3550 50  0001 C CNN "Code Fournisseur"
F 10 "5" H 2950 3550 50  0001 C CNN "Conditionnement"
F 11 "5 A" H 2950 3550 50  0001 C CNN "Courant"
F 12 "Connecteur Femelle, Traversant, 32 Contacts, 1 rangée, Droit, au pas de 2.54mm" H 2950 3550 50  0001 C CNN "Description"
F 13 "RS" H 2950 3550 50  0001 C CNN "Fournisseur"
F 14 "83,82×2,54×7.6mm" H 2950 3550 50  0001 C CNN "Height"
F 15 "Winslow" H 2950 3550 50  0001 C CNN "Manufacturer_Name"
F 16 "W35532TRC" H 2950 3550 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "6,00 " H 2950 3550 50  0001 C CNN "Prix Conditionnement"
F 18 "1,20" H 2950 3550 50  0001 C CNN "Prix Unitaire"
F 19 "267-7416" H 2950 3550 50  0001 C CNN "RS Part Number"
	1    2950 3550
	-1   0    0    -1  
$EndComp
Text Label 6950 2350 0    50   ~ 10
VDD
$Comp
L Device:C C?
U 1 1 60C4A8DC
P 8650 3750
AR Path="/60C4A8DC" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/60C4A8DC" Ref="C?"  Part="1" 
AR Path="/5D7EDCB3/60C4A8DC" Ref="C16"  Part="1" 
F 0 "C16" H 8765 3796 50  0000 L CNN
F 1 "100n" H 8765 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8688 3600 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 8650 3750 50  0001 C CNN
F 4 "698-3251" H 8650 3750 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 8650 3750 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 8650 3750 50  0001 C CNN "Description"
F 7 "RS" H 8650 3750 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 8650 3750 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 8650 3750 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 8650 3750 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 8650 3750 50  0001 C CNN "Tension"
F 12 "±10%" H 8650 3750 50  0001 C CNN "Tolérence"
F 13 "~" H 8650 3750 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 8650 3750 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 8650 3750 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 8650 3750 50  0001 C CNN "Height"
F 17 "AVX" H 8650 3750 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 8650 3750 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 8650 3750 50  0001 C CNN "RS Part Number"
	1    8650 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8650 3900 8650 3950
$Comp
L power:GND #PWR012
U 1 1 60C6E1AB
P 8650 3950
F 0 "#PWR012" H 8650 3700 50  0001 C CNN
F 1 "GND" H 8650 3750 50  0000 C CNN
F 2 "" H 8650 3950 50  0001 C CNN
F 3 "" H 8650 3950 50  0001 C CNN
	1    8650 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9650 3500 9650 3550
Wire Wire Line
	9850 3500 9850 3550
Wire Wire Line
	9950 3550 9950 3500
Wire Wire Line
	10250 3500 10250 3550
Text Label 10350 2500 1    50   ~ 10
I2C0_SCL
Text Label 3200 1600 3    50   ~ 10
I2C0_SDA
Text Label 10550 2500 1    50   ~ 10
I2C0_SDA
Text Label 10450 2500 1    50   ~ 10
UART_Rx
Text Label 9050 2500 1    50   ~ 0
I2Cext2
Text Label 9250 2500 1    50   ~ 0
I2Cext1
Text Label 9450 2500 1    50   ~ 0
I2Cext0
Text Label 9650 2500 1    50   ~ 0
GPOAUDIO
Text Label 8850 2500 1    50   ~ 0
GPOAUDENA
Text Label 9550 2500 1    50   ~ 10
I2C1_SDA
Text Label 9750 2500 1    50   ~ 10
I2C1_SCL
Text Label 9150 2500 1    50   ~ 10
Prog_SWDIO
Text Label 8950 2500 1    50   ~ 10
Prog_SWDCLK
Wire Wire Line
	9450 3500 9450 3550
Wire Wire Line
	10050 3500 10050 3550
Wire Wire Line
	10150 3550 10150 3500
Wire Wire Line
	9750 3500 9750 3550
Wire Wire Line
	8650 3550 8650 3600
Wire Wire Line
	10350 3500 10350 3550
Wire Wire Line
	10450 3500 10450 3550
Wire Wire Line
	10550 3500 10550 3550
Wire Wire Line
	10750 3500 10750 3550
Text Label 10650 2500 1    50   ~ 10
UART_Tx
Wire Wire Line
	1050 5000 1700 5000
Wire Wire Line
	1700 5150 1700 5550
Text Notes 700  7650 0    87   ~ 17
Le µCU PSOC voit toutes ses broches doublées via des Headers en parallèle\navec ses broches d'entrées sorties, ceux-ci permettant notament d'y relier\ndes connecteurs “Dupont” afin de faire facilement des tests ou des mesures.\n\nToutes ses broches sont également reliées à des connecteurs 20 broches\ndisponibles sur la face avant de la carte IHM permettant des contacts plus sûrs,\ndes plus grandes vitesses de transmissions et une connectique bien plus durable.\n\nSont également déportés tous les signaux “fixes” ou “localisés” du µCU CYC58LP\nvers des connecteurs dédiés afin d'en simplifier l'utilisation\n(les pins des autres signaux étant reconfigurables par logiciels).
Wire Wire Line
	9850 1750 9850 2500
$Comp
L Connector_Generic:Conn_01x20 J?
U 1 1 5FA224E5
P 9750 1550
AR Path="/5D7EDD13/5FA224E5" Ref="J?"  Part="1" 
AR Path="/5D7EDCB3/5FA224E5" Ref="J7"  Part="1" 
F 0 "J7" H 9750 2550 50  0000 C CNN
F 1 "Conn_01x20_PSoC_ext_1" V 9850 1550 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Clasp_501190-2027_2x10_1MP_P1mm_Vertical" H 9750 1550 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/14f2/0900766b814f2c78.pdf" H 9750 1550 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/9048219/" H 9750 1550 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9750 1550 50  0001 C CNN "Puissance"
F 6 "50 V c.c." H 9750 1550 50  0001 C CNN "Tension"
F 7 "~" H 9750 1550 50  0001 C CNN "Tolérence"
F 8 "RS" H 9750 1550 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 9750 1550 50  0001 C CNN "Catégorie"
F 10 "904-8219" H 9750 1550 50  0001 C CNN "Code Fournisseur"
F 11 "5" H 9750 1550 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 9750 1550 50  0001 C CNN "Courant"
F 13 "Embase pour CI Molex, Pico-Clasp, 20 pôles , 1mm, 2 rangées , 1A, Verticale" H 9750 1550 50  0001 C CNN "Description"
F 14 "1mm" H 9750 1550 50  0001 C CNN "Height"
F 15 "Molex" H 9750 1550 50  0001 C CNN "Manufacturer_Name"
F 16 "501190-2027" H 9750 1550 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "2,46" H 9750 1550 50  0001 C CNN "Prix Conditionnement"
F 18 "0,492" H 9750 1550 50  0001 C CNN "Prix Unitaire"
F 19 "904-8219" H 9750 1550 50  0001 C CNN "RS Part Number"
	1    9750 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10150 2500 10150 1750
$Comp
L power:GND #PWR016
U 1 1 6073DD01
P 8600 2250
F 0 "#PWR016" H 8600 2000 50  0001 C CNN
F 1 "GND" H 8600 2050 50  0000 C CNN
F 2 "" H 8600 2250 50  0001 C CNN
F 3 "" H 8600 2250 50  0001 C CNN
	1    8600 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8600 1850 8600 2250
Text Notes 4600 1400 0    200  ~ 40
µCU PSOC 5LP \n CY8CKIT_059
Wire Notes Line width 40 rgb(0, 185, 6)
	4550 1500 7000 1500
Wire Notes Line width 40 rgb(0, 185, 6)
	4550 1500 4550 600 
Wire Notes Line width 40 rgb(0, 185, 6)
	7000 1500 7000 600 
Text Label 3200 3650 0    50   ~ 0
GPORST
Text Label 3200 3550 0    50   ~ 0
NP_DATA
Text Label 3200 3750 0    50   ~ 10
I2C1_SDA
Text Label 3200 3850 0    50   ~ 10
I2C1_SCL
Text Label 3200 3950 0    50   ~ 10
Prog_SWDIO
Text Label 3200 4050 0    50   ~ 10
Prog_SWDCLK
Text Label 3200 4250 0    50   ~ 10
P_SWO
Text Label 3200 4350 0    50   ~ 10
P_TDI
Text Label 3200 4450 0    50   ~ 0
ROTENSW
Text Label 3200 4550 0    50   ~ 0
ROTENA
Text Label 3200 4650 0    50   ~ 0
ROTENB
Text Label 3200 4750 0    50   Italic 10
GND
Text Label 3200 4850 0    50   Italic 10
VDD
Text Label 3200 3450 0    50   ~ 10
I2C0_SCL
Text Label 3200 3350 0    50   ~ 10
I2C0_SDA
Text Label 3200 3250 0    50   ~ 10
UART_Rx
Text Label 3200 3150 0    50   ~ 10
UART_Tx
Text Label 3200 2950 0    50   ~ 0
I2Cext2
Text Label 3200 2850 0    50   ~ 0
I2Cext1
Text Label 3200 2750 0    50   ~ 0
I2Cext0
Text Label 3200 2650 0    50   ~ 0
GPOAUDIO
Text Label 3650 2350 0    50   ~ 0
GPOAUDENA
Text Label 6950 2550 0    50   ~ 0
XRES
Text Label 9550 3550 3    50   Italic 10
GND
Text Label 9450 3550 3    50   Italic 10
GND
Text Label 10050 3550 3    50   Italic 10
GND
Text Label 10150 3550 3    50   Italic 10
GND
Text Label 9850 2500 1    50   Italic 10
GND
Text Label 10150 2500 1    50   Italic 10
GND
Text Label 8650 3550 2    50   ~ 10
VDD
Text Label 4600 2350 0    50   ~ 0
P2[0]
Text Label 4600 2450 0    50   ~ 0
P2[1]
Text Label 4600 2550 0    50   ~ 0
P2[2]
Text Label 4600 2650 0    50   ~ 0
P2[3]
Text Label 4600 2750 0    50   ~ 0
P2[4]
Text Label 4600 2850 0    50   ~ 0
P2[5]
Text Label 4600 2950 0    50   ~ 0
P2[6]
Text Label 4600 3050 0    50   ~ 0
P2[7]
Text Label 4600 3150 0    50   ~ 0
P12[7]
Text Label 4600 3250 0    50   ~ 0
P12[6]
Text Label 4600 3350 0    50   ~ 0
P12[5]
Text Label 4600 3450 0    50   ~ 0
P12[4]
Text Label 4600 3550 0    50   ~ 0
P12[3]
Text Label 4600 3650 0    50   ~ 0
P12[2]
Text Label 4600 3750 0    50   ~ 0
P12[1]
Text Label 4600 3850 0    50   ~ 0
P12[0]
Text Label 4600 3950 0    50   ~ 0
P1[0]
Text Label 4600 4050 0    50   ~ 0
P1[1]
Text Label 4600 4150 0    50   ~ 0
P1[2]
Text Label 4600 4250 0    50   ~ 0
P1[3]
Text Label 4600 4350 0    50   ~ 0
P1[4]
Text Label 4600 4450 0    50   ~ 0
P1[5]
Text Label 4600 4550 0    50   ~ 0
P1[6]
Text Label 4600 4650 0    50   ~ 0
P1[7]
Text Label 4600 4750 0    50   ~ 0
GND
Text Label 4600 4850 0    50   ~ 0
VDDIO
Text Label 8850 1750 3    50   ~ 0
P2[0]
Text Label 10250 1750 3    50   ~ 0
P2[1]
Text Label 10050 1750 3    50   ~ 0
P2[2]
Text Label 9650 1750 3    50   ~ 0
P2[3]
Text Label 9450 1750 3    50   ~ 0
P2[4]
Text Label 9250 1750 3    50   ~ 0
P2[5]
Text Label 9050 1750 3    50   ~ 0
P2[6]
Text Label 10750 1750 3    50   ~ 0
P2[7]
Text Label 10650 1750 3    50   ~ 0
P12[7]
Text Label 10450 1750 3    50   ~ 0
P12[6]
Text Label 10550 1750 3    50   ~ 0
P12[5]
Text Label 10350 1750 3    50   ~ 0
P12[4]
Text Label 9550 1750 3    50   ~ 0
P12[1]
Text Label 9750 1750 3    50   ~ 0
P12[0]
Text Label 9150 1750 3    50   ~ 0
P1[0]
Text Label 8950 1750 3    50   ~ 0
P1[1]
Text Label 4550 4450 2    50   ~ 0
ROTENSW
Text Label 4550 4550 2    50   ~ 0
ROTENA
Text Label 4550 4650 2    50   ~ 0
ROTENB
Text Label 4500 3550 2    50   ~ 0
NP_DATA
Text Label 6950 4050 0    50   ~ 0
P3[7]
Text Label 6950 4150 0    50   ~ 0
P3[6]
Text Label 6950 4250 0    50   ~ 0
P3[5]
Text Label 6950 4350 0    50   ~ 0
P3[4]
Text Label 6950 4450 0    50   ~ 0
P3[3]
Text Label 6950 4550 0    50   ~ 0
P3[2]
Text Label 6950 4650 0    50   ~ 0
P3[1]
Text Label 6950 4750 0    50   ~ 0
P3[0]
Text Label 6950 4850 0    50   Italic 10
GND
Wire Wire Line
	6800 4350 7700 4350
Wire Wire Line
	6800 4050 7700 4050
Wire Wire Line
	6800 4650 7700 4650
Wire Wire Line
	6800 4150 7700 4150
Wire Wire Line
	6800 4550 7700 4550
Wire Wire Line
	6800 4850 7700 4850
Wire Wire Line
	6800 4250 7700 4250
Wire Wire Line
	6800 4450 7700 4450
Wire Wire Line
	6800 4750 7700 4750
Text Label 10250 5100 3    50   Italic 10
GND
Wire Wire Line
	8850 5100 8850 5700
Wire Wire Line
	9150 5100 9150 5700
Wire Wire Line
	9350 5100 9350 5700
Text Label 9850 5700 1    50   ~ 0
P3[0]
Text Label 10450 5700 1    50   ~ 0
P3[1]
Text Label 10650 5700 1    50   ~ 0
P3[2]
Text Label 10050 5700 1    50   ~ 0
P3[3]
Text Label 9950 5700 1    50   ~ 0
P3[4]
Text Label 10750 5700 1    50   ~ 0
P3[5]
Text Label 10550 5700 1    50   ~ 0
P3[6]
Text Label 10350 5700 1    50   ~ 0
P3[7]
Text Label 9050 5100 3    50   ~ 10
P_SWO
Text Label 9250 5100 3    50   ~ 0
NP_DATA
Text Label 9550 5100 3    50   ~ 0
ROTENB
Text Label 9650 5100 3    50   ~ 0
ROTENA
Text Label 9750 5100 3    50   ~ 0
ROTENSW
Text Label 8850 5100 3    50   ~ 10
P_TDI
Text Label 9550 5700 1    50   ~ 0
P1[7]
Text Label 9650 5700 1    50   ~ 0
P1[6]
Text Label 9750 5700 1    50   ~ 0
P1[5]
Text Label 8850 5700 1    50   ~ 0
P1[4]
Text Label 9050 5700 1    50   ~ 0
P1[3]
Text Label 8950 5700 1    50   ~ 0
P1[2]
Text Label 9950 1750 3    50   ~ 0
P12[2]
Text Label 9350 5100 3    50   Italic 10
VDD
Text Label 8650 5200 2    50   Italic 10
VDD
Text Label 10150 5100 3    50   Italic 10
GND
Text Label 9450 5100 3    50   Italic 10
GND
Text Label 9150 5100 3    50   Italic 10
GND
Wire Wire Line
	9450 5700 9450 5100
Wire Wire Line
	10150 5100 10150 5700
$Comp
L Connector_Generic:Conn_01x20 J?
U 1 1 5FAD7ABF
P 9850 5900
AR Path="/5D7EDD13/5FAD7ABF" Ref="J?"  Part="1" 
AR Path="/5D7EDCB3/5FAD7ABF" Ref="J8"  Part="1" 
F 0 "J8" H 9850 4800 50  0000 C CNN
F 1 "Conn_01x20_PSoC_ext_2" V 9950 5900 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Clasp_501190-2027_2x10_1MP_P1mm_Vertical" H 9850 5900 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/14f2/0900766b814f2c78.pdf" H 9850 5900 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/9048219/" H 9850 5900 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9850 5900 50  0001 C CNN "Puissance"
F 6 "50 V c.c." H 9850 5900 50  0001 C CNN "Tension"
F 7 "~" H 9850 5900 50  0001 C CNN "Tolérence"
F 8 "RS" H 9850 5900 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 9850 5900 50  0001 C CNN "Catégorie"
F 10 "904-8219" H 9850 5900 50  0001 C CNN "Code Fournisseur"
F 11 "5" H 9850 5900 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 9850 5900 50  0001 C CNN "Courant"
F 13 "Embase pour CI Molex, Pico-Clasp, 20 pôles , 1mm, 2 rangées , 1A, Verticale" H 9850 5900 50  0001 C CNN "Description"
F 14 "1mm" H 9850 5900 50  0001 C CNN "Height"
F 15 "Molex" H 9850 5900 50  0001 C CNN "Manufacturer_Name"
F 16 "501190-2027" H 9850 5900 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "2,46" H 9850 5900 50  0001 C CNN "Prix Conditionnement"
F 18 "0,492" H 9850 5900 50  0001 C CNN "Prix Unitaire"
F 19 "904-8219" H 9850 5900 50  0001 C CNN "RS Part Number"
	1    9850 5900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 606AE43D
P 8650 5400
AR Path="/606AE43D" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/606AE43D" Ref="C?"  Part="1" 
AR Path="/5D7EDCB3/606AE43D" Ref="C17"  Part="1" 
F 0 "C17" H 8765 5446 50  0000 L CNN
F 1 "100n" H 8765 5355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8688 5250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 8650 5400 50  0001 C CNN
F 4 "698-3251" H 8650 5400 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 8650 5400 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 8650 5400 50  0001 C CNN "Description"
F 7 "RS" H 8650 5400 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 8650 5400 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 8650 5400 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 8650 5400 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 8650 5400 50  0001 C CNN "Tension"
F 12 "±10%" H 8650 5400 50  0001 C CNN "Tolérence"
F 13 "~" H 8650 5400 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 8650 5400 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 8650 5400 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 8650 5400 50  0001 C CNN "Height"
F 17 "AVX" H 8650 5400 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 8650 5400 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 8650 5400 50  0001 C CNN "RS Part Number"
	1    8650 5400
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 606D25B8
P 8650 5600
F 0 "#PWR015" H 8650 5350 50  0001 C CNN
F 1 "GND" H 8650 5400 50  0000 C CNN
F 2 "" H 8650 5600 50  0001 C CNN
F 3 "" H 8650 5600 50  0001 C CNN
	1    8650 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8650 5550 8650 5600
Wire Wire Line
	8650 5200 8650 5250
Wire Wire Line
	10250 5100 10250 5700
Wire Wire Line
	9750 5100 9750 5700
Wire Wire Line
	9650 5700 9650 5100
Wire Wire Line
	9550 5100 9550 5700
Wire Wire Line
	9250 5100 9250 5700
Wire Wire Line
	9050 5100 9050 5700
Text Label 6950 3950 0    50   ~ 0
P15[0]
Text Label 6950 3750 0    50   ~ 0
P15[2]
Text Label 6950 3650 0    50   ~ 0
P15[3]
Text Label 6950 3550 0    50   ~ 0
P15[4]
Text Label 6950 3450 0    50   ~ 0
P15[5]
Text Label 6950 3350 0    50   ~ 0
P15[6]
Text Label 6950 3250 0    50   ~ 0
P0[1]
Text Label 6950 3150 0    50   ~ 0
P0[2]
Text Label 6950 3050 0    50   ~ 0
P0[3]
Text Label 6950 2950 0    50   ~ 0
P0[4]
Text Label 6950 2850 0    50   ~ 0
P0[5]
Text Label 6950 2750 0    50   ~ 0
P0[6]
Text Label 6950 2650 0    50   ~ 0
P0[7]
Text Label 6950 2450 0    50   ~ 0
GND
Text Label 10750 3550 3    50   ~ 0
P0[5]
Text Label 10500 3950 2    50   ~ 0
P15[2]
Text Label 10550 3550 3    50   ~ 0
P0[6]
Text Label 10450 3550 3    50   ~ 0
P15[1]
Text Label 10350 3550 3    50   ~ 0
P0[7]
Text Label 10250 3550 3    50   ~ 0
P15[0]
Text Label 9950 3550 3    50   ~ 0
XRES
Text Label 9850 3550 3    50   ~ 0
P0[1]
Text Label 9750 3550 3    50   ~ 0
P0[4]
Text Label 9650 3550 3    50   ~ 0
P15[4]
Text Label 9350 3550 3    50   ~ 0
P0[3]
Text Label 9250 3550 3    50   ~ 0
P15[5]
Text Label 9150 3550 3    50   ~ 0
P0[2]
Text Label 9050 3550 3    50   ~ 0
P15[6]
Text Label 8950 3550 3    50   ~ 10
VDD
Wire Wire Line
	6800 2450 7700 2450
Wire Wire Line
	6800 3550 7700 3550
Wire Wire Line
	6800 2950 7700 2950
Wire Wire Line
	6800 3250 7700 3250
Wire Wire Line
	6800 3950 7700 3950
Wire Wire Line
	6800 2650 7700 2650
Wire Wire Line
	6800 3850 7700 3850
Wire Wire Line
	6800 2750 7700 2750
Wire Wire Line
	6800 3750 7700 3750
Wire Wire Line
	6800 2850 7700 2850
Text Label 8600 1850 2    50   Italic 10
GND
Wire Wire Line
	6800 3650 7700 3650
Wire Wire Line
	6800 2350 7700 2350
Wire Wire Line
	6800 3350 7700 3350
Wire Wire Line
	6800 3150 7700 3150
Wire Wire Line
	6800 3450 7700 3450
Wire Wire Line
	6800 3050 7700 3050
Wire Wire Line
	8950 3500 8950 3550
Wire Wire Line
	9050 3550 9050 3500
Wire Wire Line
	9150 3500 9150 3550
Wire Wire Line
	9250 3550 9250 3500
Wire Wire Line
	9350 3500 9350 3550
$Comp
L Connector_Generic:Conn_01x20 J?
U 1 1 5FA2B437
P 9750 3300
AR Path="/5D7EDD13/5FA2B437" Ref="J?"  Part="1" 
AR Path="/5D7EDCB3/5FA2B437" Ref="J9"  Part="1" 
F 0 "J9" H 9750 4300 50  0000 C CNN
F 1 "Conn_01x20_PSoC_ext_3" V 9850 3300 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Clasp_501190-2027_2x10_1MP_P1mm_Vertical" H 9750 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/14f2/0900766b814f2c78.pdf" H 9750 3300 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/9048219/" H 9750 3300 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9750 3300 50  0001 C CNN "Puissance"
F 6 "50 V c.c." H 9750 3300 50  0001 C CNN "Tension"
F 7 "~" H 9750 3300 50  0001 C CNN "Tolérence"
F 8 "RS" H 9750 3300 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 9750 3300 50  0001 C CNN "Catégorie"
F 10 "904-8219" H 9750 3300 50  0001 C CNN "Code Fournisseur"
F 11 "5" H 9750 3300 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 9750 3300 50  0001 C CNN "Courant"
F 13 "Embase pour CI Molex, Pico-Clasp, 20 pôles , 1mm, 2 rangées , 1A, Verticale" H 9750 3300 50  0001 C CNN "Description"
F 14 "1mm" H 9750 3300 50  0001 C CNN "Height"
F 15 "Molex" H 9750 3300 50  0001 C CNN "Manufacturer_Name"
F 16 "501190-2027" H 9750 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "2,46" H 9750 3300 50  0001 C CNN "Prix Conditionnement"
F 18 "0,492" H 9750 3300 50  0001 C CNN "Prix Unitaire"
F 19 "904-8219" H 9750 3300 50  0001 C CNN "RS Part Number"
	1    9750 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9550 3500 9550 3550
Text Label 6950 3850 0    50   ~ 0
P15[1]
Wire Wire Line
	6800 2550 7700 2550
Wire Wire Line
	4000 1550 4000 1600
$Comp
L Misc_KropoteX:SolderJumper_3_Open_Bi JP5
U 1 1 5E12DD6D
P 10650 3950
F 0 "JP5" V 10850 3750 50  0000 L CNN
F 1 "SolderJumper_3_Open_Bi" V 10750 3200 39  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 10650 3950 50  0001 C CNN
F 3 "" H 10650 3950 50  0001 C CNN
	1    10650 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	10650 3500 10650 3750
Wire Wire Line
	8850 3700 8850 3500
$Comp
L Misc_KropoteX:SolderJumper_3_Open_Bi JP4
U 1 1 5E12B350
P 8850 3900
F 0 "JP4" V 9100 3700 50  0000 L CNN
F 1 "SolderJumper_3_Open_Bi" V 8950 3150 39  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 8850 3900 50  0001 C CNN
F 3 "" H 8850 3900 50  0001 C CNN
	1    8850 3900
	0    -1   1    0   
$EndComp
Wire Wire Line
	10650 4150 10650 4250
Text Label 9000 3900 0    50   ~ 0
P15[3]
Wire Wire Line
	8850 4100 8850 4250
$Comp
L Device:Crystal_GND3 Y1
U 1 1 5D9AC333
P 9750 4250
F 0 "Y1" H 9600 4300 50  0000 C CNN
F 1 "Crystal" H 10000 4300 50  0000 C CNN
F 2 "Crystal:Crystal_C26-LF_D2.1mm_L6.5mm_Horizontal_1EP_style2" H 9750 4250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/15dc/0900766b815dc7d7.pdf" H 9750 4250 50  0001 C CNN
F 4 "Passif" H 9750 4250 50  0001 C CNN "Catégorie"
F 5 " 144-2308" H 9750 4250 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 9750 4250 50  0001 C CNN "Conditionnement"
F 7 "Quartz Cylindrique RS PRO, 32.768kHz 6pF, 2 broches, ±20ppm" H 9750 4250 50  0001 C CNN "Description"
F 8 "RS" H 9750 4250 50  0001 C CNN "Fournisseur"
F 9 "2.1 x 6.2mm" H 9750 4250 50  0001 C CNN "Height"
F 10 "RS PRO" H 9750 4250 50  0001 C CNN "Manufacturer_Name"
F 11 "144-2308" H 9750 4250 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "10,05" H 9750 4250 50  0001 C CNN "Prix Conditionnement"
F 13 "0,201" H 9750 4250 50  0001 C CNN "Prix Unitaire"
F 14 "144-2308" H 9750 4250 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/quartz-electroniques/1442308/" H 9750 4250 50  0001 C CNN "RS Price/Stock"
F 16 "±20ppm" H 9750 4250 50  0001 C CNN "Tolérence"
	1    9750 4250
	-1   0    0    -1  
$EndComp
Text Label 8850 4250 0    50   ~ 0
XTAL_IN
Text Label 10650 4250 2    50   ~ 0
XTAL_OUT
Wire Wire Line
	8850 4250 9600 4250
Wire Wire Line
	9900 4250 10650 4250
Wire Wire Line
	9050 1750 9050 2500
Wire Wire Line
	3150 2550 4950 2550
Wire Wire Line
	3150 2450 4950 2450
$Comp
L IHM-Générique-rescue:5LP_PSoC_CY8CKIT_059-Misc_KropoteX IC?
U 1 1 5D7EC124
P 5150 1900
AR Path="/5D7EC124" Ref="IC?"  Part="1" 
AR Path="/5D7EDCB3/5D7EC124" Ref="IC2"  Part="1" 
F 0 "IC2" H 5661 2067 50  0000 C CNB
F 1 "5LP_PSoC_CY8CKIT_059" H 5661 1976 50  0000 C CNB
F 2 "Misc_KropoteX:5LP_PSoC_CY8CKIT_059_Solo" H 5100 2550 50  0001 L CNN
F 3 "https://www.cypress.com/file/157976/download" H 4550 2750 50  0001 L CNN
F 4 "Kit pour prototype 5LP PSoC CY8CKIT-059 avec un programmateur/débogueur à \"clipser\"" H 4550 2650 50  0001 L CNN "Description"
F 5 "98,89 × 24,13 mm" H 4550 2550 50  0001 L CNN "Height"
F 6 "124-4192" H 4550 2450 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/1244192" H 4550 2350 50  0001 L CNN "RS Price/Stock"
F 8 "Cypress Semiconductor" H 4550 2250 50  0001 L CNN "Manufacturer_Name"
F 9 "CY8CKIT-059" H 4550 2150 50  0001 L CNN "Manufacturer_Part_Number"
F 10 "CY8C5888LTI-LP097" H 4550 2050 50  0001 L CNN "MCU_Part_Number"
F 11 "https://uk.rs-online.com/web/p/products/1242972P" H 4550 1950 50  0001 L CNN "MCU_Documentation"
F 12 "Circuit intégré" H 5150 1900 50  0001 C CNN "Catégorie"
F 13 "RS" H 5150 1900 50  0001 C CNN "Fournisseur"
F 14 "5" H 5150 1900 50  0001 C CNN "Tension"
F 15 "~" H 5150 1900 50  0001 C CNN "Tolérence"
F 16 "~" H 5150 1900 50  0001 C CNN "Puissance"
F 17 "124-4192" H 5150 1900 50  0001 C CNN "Code Fournisseur"
F 18 "2" H 5150 1900 50  0001 C CNN "Conditionnement"
F 19 "200 mA" H 5150 1900 50  0001 C CNN "Courant"
F 20 "12,05" H 5150 1900 50  0001 C CNN "Prix Conditionnement"
F 21 "12,6" H 5150 1900 50  0001 C CNN "Prix Unitaire"
	1    5150 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 1750 8850 2500
Wire Wire Line
	8950 2500 8950 1750
Wire Wire Line
	9150 1750 9150 2500
Wire Wire Line
	9250 2500 9250 1750
Wire Wire Line
	10650 1750 10650 2500
Wire Wire Line
	10550 1750 10550 2500
Wire Wire Line
	10450 1750 10450 2500
Wire Wire Line
	9950 1750 9950 2500
Wire Wire Line
	9750 1750 9750 2500
Wire Wire Line
	9650 1750 9650 2500
Wire Wire Line
	9550 1750 9550 2500
Wire Wire Line
	9350 1750 9350 2500
Text Label 9350 2500 1    50   Italic 10
GND
Wire Wire Line
	9450 1750 9450 2500
Wire Wire Line
	10350 1750 10350 2500
Text Notes 8650 1100 0    200  ~ 40
Header PSoC\n
Wire Notes Line width 40 rgb(0, 185, 6)
	8500 750  10750 750 
Wire Notes Line width 40 rgb(0, 185, 6)
	8300 6250 8300 1350
Wire Notes Line width 40 rgb(0, 185, 6)
	8500 1200 8500 750 
Wire Notes Line width 40 rgb(0, 185, 6)
	10750 1200 10750 750 
Wire Notes Line width 40 rgb(0, 185, 6)
	10900 6250 10900 1350
Wire Notes Line width 40 rgb(0, 185, 6)
	10900 1350 8300 1350
Wire Notes Line width 40 rgb(0, 185, 6)
	8300 6250 10900 6250
Wire Notes Line width 40 rgb(0, 185, 6)
	8500 1200 10750 1200
$Comp
L power:GND #PWR0102
U 1 1 5DA5477C
P 9750 4450
F 0 "#PWR0102" H 9750 4200 50  0001 C CNN
F 1 "GND" H 9750 4300 50  0000 C CNN
F 2 "" H 9750 4450 50  0001 C CNN
F 3 "" H 9750 4450 50  0001 C CNN
	1    9750 4450
	-1   0    0    -1  
$EndComp
Text Label 9250 5700 1    50   ~ 0
P12[3]
Text Label 9950 2500 1    50   ~ 0
GPORST
Wire Wire Line
	3150 2350 4950 2350
Wire Wire Line
	3900 1600 3900 1550
Wire Wire Line
	3150 2650 4950 2650
Wire Wire Line
	3150 2750 4950 2750
Wire Wire Line
	3150 2850 4950 2850
Wire Wire Line
	3150 2950 4950 2950
Wire Wire Line
	3150 3050 4950 3050
Wire Wire Line
	3150 3150 4950 3150
Wire Wire Line
	3150 3250 4950 3250
Wire Wire Line
	3150 3350 4950 3350
Wire Wire Line
	3150 3450 4950 3450
Entry Wire Line
	3800 1550 3700 1450
Wire Wire Line
	3800 1600 3800 1550
Wire Wire Line
	3700 1600 3700 1550
Wire Wire Line
	3600 1550 3600 1600
Wire Wire Line
	3500 1600 3500 1550
Wire Wire Line
	3400 1550 3400 1600
Wire Wire Line
	3300 1600 3300 1550
Wire Wire Line
	3200 1550 3200 1600
Wire Wire Line
	3100 1600 3100 1550
Wire Wire Line
	3150 3550 4950 3550
Wire Wire Line
	3150 3650 4950 3650
Wire Wire Line
	3150 4550 4950 4550
Wire Wire Line
	3150 4650 4950 4650
Wire Wire Line
	3150 4750 4950 4750
Wire Wire Line
	3150 4850 4950 4850
Wire Wire Line
	3150 4450 4950 4450
Wire Wire Line
	3150 4350 4950 4350
Wire Wire Line
	3150 4250 4950 4250
Wire Wire Line
	3150 4150 4950 4150
Wire Wire Line
	3150 4050 4950 4050
Wire Wire Line
	3150 3950 4950 3950
Wire Wire Line
	3150 3850 4950 3850
Wire Wire Line
	3150 3750 4950 3750
Wire Wire Line
	2850 5700 2850 5650
Wire Wire Line
	2950 5650 2950 5700
Wire Wire Line
	3050 5700 3050 5650
Wire Wire Line
	3150 5650 3150 5700
Wire Wire Line
	3250 5700 3250 5650
Wire Wire Line
	3350 5650 3350 5700
Wire Wire Line
	3450 5700 3450 5650
Wire Wire Line
	3550 5650 3550 5700
Wire Wire Line
	3650 5700 3650 5650
Wire Wire Line
	3750 5650 3750 5700
Wire Wire Line
	3850 5700 3850 5650
Wire Wire Line
	3950 5650 3950 5700
Wire Wire Line
	4050 5700 4050 5650
Text Notes 900  1050 0    197  ~ 39
Bus signaux fixés
Wire Notes Line width 40 rgb(0, 185, 6)
	850  700  3650 700 
Wire Notes Line width 40 rgb(0, 185, 6)
	850  1200 850  700 
Wire Notes Line width 40 rgb(0, 185, 6)
	3650 1200 3650 700 
Wire Notes Line width 40 rgb(0, 185, 6)
	850  1200 3650 1200
Wire Notes Line width 40 rgb(0, 185, 6)
	4100 2100 2600 2100
Wire Notes Line width 40 rgb(0, 185, 6)
	4100 2100 4100 1350
Wire Notes Line width 40 rgb(0, 185, 6)
	4150 5050 2600 5050
Wire Notes Line width 40 rgb(0, 185, 6)
	4150 5900 4150 5050
Wire Notes Line width 40 rgb(0, 185, 6)
	2600 2100 2600 5050
Wire Notes Line width 40 rgb(0, 185, 6)
	800  1350 800  5900
Wire Notes Line width 40 rgb(0, 185, 6)
	800  5900 4150 5900
Wire Notes Line width 40 rgb(0, 185, 6)
	4550 600  7000 600 
Wire Notes Line width 40 rgb(0, 185, 6)
	4100 1350 800  1350
Wire Bus Line
	2550 1450 3900 1450
Wire Bus Line
	2550 5800 3950 5800
Wire Bus Line
	950  1450 950  5050
$EndSCHEMATC
