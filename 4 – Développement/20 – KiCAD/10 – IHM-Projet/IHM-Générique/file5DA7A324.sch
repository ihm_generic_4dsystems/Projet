EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Rotary_Encoder_Switch SW?
U 1 1 5DA84A8E
P 6150 3650
AR Path="/5DA84A8E" Ref="SW?"  Part="1" 
AR Path="/5DA7A325/5DA84A8E" Ref="SW2"  Part="1" 
F 0 "SW2" H 6150 3300 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 6150 3400 50  0000 C CNN
F 2 "Misc_KropoteX:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 6000 3810 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0f4c/0900766b80f4c3d5.pdf" H 6150 3910 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 1727621 " H 6150 3650 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 6150 3650 50  0001 C CNN "Puissance"
F 6 "~" H 6150 3650 50  0001 C CNN "Tension"
F 7 "~" H 6150 3650 50  0001 C CNN "Tolérence"
F 8 "Passif" H 6150 3650 50  0001 C CNN "Catégorie"
F 9 "172-7621" H 6150 3650 50  0001 C CNN "Code Fournisseur"
F 10 "95" H 6150 3650 50  0001 C CNN "Conditionnement"
F 11 "0,5 mA" H 6150 3650 50  0001 C CNN "Courant"
F 12 "Encodeur rotatif mécanique, , Incrémental, 24 impulsions par tour, axe de 6 mm, Traversant" H 6150 3650 50  0001 C CNN "Description"
F 13 "RS" H 6150 3650 50  0001 C CNN "Fournisseur"
F 14 "1 mm, 2.1 x 2 mm" H 6150 3650 50  0001 C CNN "Height"
F 15 "Alps Alpine" H 6150 3650 50  0001 C CNN "Manufacturer_Name"
F 16 " 172-7621" H 6150 3650 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "45,03" H 6150 3650 50  0001 C CNN "Prix Conditionnement"
F 18 "0,474" H 6150 3650 50  0001 C CNN "Prix Unitaire"
F 19 " 172-7621 " H 6150 3650 50  0001 C CNN "RS Part Number"
	1    6150 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DA84AA4
P 4450 3300
AR Path="/5DA84AA4" Ref="R?"  Part="1" 
AR Path="/5DA7A325/5DA84AA4" Ref="R11"  Part="1" 
F 0 "R11" H 4520 3346 50  0000 L CNN
F 1 "10k" H 4520 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4380 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 4450 3300 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 4450 3300 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4450 3300 50  0001 C CNN "Puissance"
F 6 "~" H 4450 3300 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4450 3300 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4450 3300 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 4450 3300 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4450 3300 50  0001 C CNN "Conditionnement"
F 11 "~" H 4450 3300 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4450 3300 50  0001 C CNN "Description"
F 13 "RS" H 4450 3300 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4450 3300 50  0001 C CNN "Height"
F 15 "RS PRO" H 4450 3300 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 4450 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 4450 3300 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 4450 3300 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 4450 3300 50  0001 C CNN "RS Part Number"
	1    4450 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DA84ABA
P 4850 3300
AR Path="/5DA84ABA" Ref="R?"  Part="1" 
AR Path="/5DA7A325/5DA84ABA" Ref="R14"  Part="1" 
F 0 "R14" H 4920 3346 50  0000 L CNN
F 1 "10k" H 4920 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4780 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 4850 3300 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 4850 3300 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4850 3300 50  0001 C CNN "Puissance"
F 6 "~" H 4850 3300 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4850 3300 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4850 3300 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 4850 3300 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4850 3300 50  0001 C CNN "Conditionnement"
F 11 "~" H 4850 3300 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4850 3300 50  0001 C CNN "Description"
F 13 "RS" H 4850 3300 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4850 3300 50  0001 C CNN "Height"
F 15 "RS PRO" H 4850 3300 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 4850 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 4850 3300 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 4850 3300 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 4850 3300 50  0001 C CNN "RS Part Number"
	1    4850 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3450 4450 3550
Connection ~ 4450 3550
Wire Wire Line
	5850 3650 5800 3650
Connection ~ 4850 3750
Wire Wire Line
	4850 3750 4850 3450
Wire Wire Line
	4450 3150 4450 3050
Wire Wire Line
	4450 3050 4850 3050
Wire Wire Line
	4850 3050 4850 3150
Wire Wire Line
	4650 4450 4850 4450
Connection ~ 4650 4450
Wire Wire Line
	4850 4450 4850 4400
Wire Wire Line
	4450 4450 4650 4450
Wire Wire Line
	4450 4400 4450 4450
$Comp
L Device:C C?
U 1 1 5DA84ADE
P 4850 4250
AR Path="/5DA84ADE" Ref="C?"  Part="1" 
AR Path="/5DA7A325/5DA84ADE" Ref="C30"  Part="1" 
F 0 "C30" H 4965 4296 50  0000 L CNN
F 1 "10n" H 4965 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4888 4100 50  0001 C CNN
F 3 "~" H 4850 4250 50  0001 C CNN
F 4 "Passif" H 4850 4250 50  0001 C CNN "Catégorie"
F 5 "798-6743" H 4850 4250 50  0001 C CNN "Code Fournisseur"
F 6 "500" H 4850 4250 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 10V cc, boitier 0201 (0603M)" H 4850 4250 50  0001 C CNN "Description"
F 8 "RS" H 4850 4250 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 4850 4250 50  0001 C CNN "Height"
F 10 "Murata" H 4850 4250 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033B11A103KA01D" H 4850 4250 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "3" H 4850 4250 50  0001 C CNN "Prix Conditionnement"
F 13 "0,006" H 4850 4250 50  0001 C CNN "Prix Unitaire"
F 14 " 798-6743" H 4850 4250 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/7986743/" H 4850 4250 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 4850 4250 50  0001 C CNN "Tension"
F 17 "±10%" H 4850 4250 50  0001 C CNN "Tolérence"
F 18 "~" H 4850 4250 50  0001 C CNN "Puissance"
	1    4850 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DA84AF3
P 4450 4250
AR Path="/5DA84AF3" Ref="C?"  Part="1" 
AR Path="/5DA7A325/5DA84AF3" Ref="C29"  Part="1" 
F 0 "C29" H 4565 4296 50  0000 L CNN
F 1 "10n" H 4565 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4488 4100 50  0001 C CNN
F 3 "~" H 4450 4250 50  0001 C CNN
F 4 "Passif" H 4450 4250 50  0001 C CNN "Catégorie"
F 5 "798-6743" H 4450 4250 50  0001 C CNN "Code Fournisseur"
F 6 "500" H 4450 4250 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 10V cc, boitier 0201 (0603M)" H 4450 4250 50  0001 C CNN "Description"
F 8 "RS" H 4450 4250 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 4450 4250 50  0001 C CNN "Height"
F 10 "Murata" H 4450 4250 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033B11A103KA01D" H 4450 4250 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "3" H 4450 4250 50  0001 C CNN "Prix Conditionnement"
F 13 "0,006" H 4450 4250 50  0001 C CNN "Prix Unitaire"
F 14 " 798-6743" H 4450 4250 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/7986743/" H 4450 4250 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 4450 4250 50  0001 C CNN "Tension"
F 17 "±10%" H 4450 4250 50  0001 C CNN "Tolérence"
F 18 "~" H 4450 4250 50  0001 C CNN "Puissance"
	1    4450 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DA84AF9
P 4650 4450
AR Path="/5DA84AF9" Ref="#PWR?"  Part="1" 
AR Path="/5DA7A325/5DA84AF9" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 4650 4200 50  0001 C CNN
F 1 "GND" H 4655 4277 50  0000 C CNN
F 2 "" H 4650 4450 50  0001 C CNN
F 3 "" H 4650 4450 50  0001 C CNN
	1    4650 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DA84B0F
P 7300 3300
AR Path="/5DA84B0F" Ref="R?"  Part="1" 
AR Path="/5DA7A325/5DA84B0F" Ref="R16"  Part="1" 
F 0 "R16" H 7370 3346 50  0000 L CNN
F 1 "10k" H 7370 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7230 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 7300 3300 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 7300 3300 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 7300 3300 50  0001 C CNN "Puissance"
F 6 "~" H 7300 3300 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 7300 3300 50  0001 C CNN "Tolérence"
F 8 "Passif" H 7300 3300 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 7300 3300 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 7300 3300 50  0001 C CNN "Conditionnement"
F 11 "~" H 7300 3300 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 7300 3300 50  0001 C CNN "Description"
F 13 "RS" H 7300 3300 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 7300 3300 50  0001 C CNN "Height"
F 15 "RS PRO" H 7300 3300 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 7300 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 7300 3300 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 7300 3300 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 7300 3300 50  0001 C CNN "RS Part Number"
	1    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DA84B24
P 7300 4250
AR Path="/5DA84B24" Ref="C?"  Part="1" 
AR Path="/5DA7A325/5DA84B24" Ref="C31"  Part="1" 
F 0 "C31" H 7415 4296 50  0000 L CNN
F 1 "10n" H 7415 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7338 4100 50  0001 C CNN
F 3 "~" H 7300 4250 50  0001 C CNN
F 4 "Passif" H 7300 4250 50  0001 C CNN "Catégorie"
F 5 "798-6743" H 7300 4250 50  0001 C CNN "Code Fournisseur"
F 6 "500" H 7300 4250 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 10V cc, boitier 0201 (0603M)" H 7300 4250 50  0001 C CNN "Description"
F 8 "RS" H 7300 4250 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 7300 4250 50  0001 C CNN "Height"
F 10 "Murata" H 7300 4250 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033B11A103KA01D" H 7300 4250 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "3" H 7300 4250 50  0001 C CNN "Prix Conditionnement"
F 13 "0,006" H 7300 4250 50  0001 C CNN "Prix Unitaire"
F 14 " 798-6743" H 7300 4250 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/7986743/" H 7300 4250 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 7300 4250 50  0001 C CNN "Tension"
F 17 "±10%" H 7300 4250 50  0001 C CNN "Tolérence"
F 18 "~" H 7300 4250 50  0001 C CNN "Puissance"
	1    7300 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DA84B2A
P 7300 4450
AR Path="/5DA84B2A" Ref="#PWR?"  Part="1" 
AR Path="/5DA7A325/5DA84B2A" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 7300 4200 50  0001 C CNN
F 1 "GND" H 7305 4277 50  0000 C CNN
F 2 "" H 7300 4450 50  0001 C CNN
F 3 "" H 7300 4450 50  0001 C CNN
	1    7300 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3650 5800 3400
Wire Wire Line
	7300 3450 7300 3750
Wire Wire Line
	7300 4400 7300 4450
Connection ~ 7300 3750
Wire Wire Line
	6450 3550 6500 3550
Wire Wire Line
	6500 3550 6500 3400
$Comp
L power:GND #PWR?
U 1 1 5DA84B37
P 6650 3450
AR Path="/5DA84B37" Ref="#PWR?"  Part="1" 
AR Path="/5DA7A325/5DA84B37" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 6650 3200 50  0001 C CNN
F 1 "GND" H 6655 3277 50  0000 C CNN
F 2 "" H 6650 3450 50  0001 C CNN
F 3 "" H 6650 3450 50  0001 C CNN
	1    6650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3400 6500 3400
Wire Wire Line
	6500 3400 6650 3400
Wire Wire Line
	6650 3400 6650 3450
Connection ~ 6500 3400
Wire Wire Line
	7300 3150 7300 3050
Connection ~ 4850 3050
Wire Wire Line
	7050 4050 7300 4050
Connection ~ 7300 4050
Wire Wire Line
	7300 4050 7300 4100
Text Label 6300 2950 0    50   ~ 0
VDD
Wire Wire Line
	4850 3050 6100 3050
Wire Wire Line
	6100 3050 6100 2950
Wire Wire Line
	6100 2950 6300 2950
Connection ~ 6100 3050
Wire Wire Line
	6100 3050 7300 3050
Wire Notes Line
	7600 2750 7600 5000
Wire Notes Line
	7600 5000 3900 5000
Wire Notes Line
	3900 5000 3900 2750
Wire Notes Line
	3900 2750 7600 2750
Text Notes 5500 2700 2    50   ~ 0
Encodeur Rotatif
Wire Wire Line
	7300 3750 7300 4050
Text Label 7050 4050 2    50   ~ 0
ROTENSW
Wire Wire Line
	4850 3750 4850 4050
Wire Wire Line
	4850 4100 4850 4050
Connection ~ 4850 4050
Wire Wire Line
	4250 4050 4850 4050
Text Label 4250 4050 2    50   ~ 0
ROTENB
Text Label 4250 3900 2    50   ~ 0
ROTENA
Wire Wire Line
	4450 3550 4450 3900
Wire Wire Line
	4450 3900 4450 4100
Connection ~ 4450 3900
Wire Wire Line
	4250 3900 4450 3900
$Comp
L Connector_Generic:Conn_01x07 J?
U 1 1 5DAA6DA0
P 3200 3950
AR Path="/5DAA6DA0" Ref="J?"  Part="1" 
AR Path="/5D7EDD70/5DAA6DA0" Ref="J?"  Part="1" 
AR Path="/5DA7A325/5DAA6DA0" Ref="J20"  Part="1" 
F 0 "J20" H 3280 3992 50  0000 L CNN
F 1 "I2C_extention" H 3280 3901 50  0000 L CNN
F 2 "Connector_JST:JST_GH_SM07B-GHS-TB_1x07-1MP_P1.25mm_Horizontal" H 3200 3950 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654ef2.pdf" H 3200 3950 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 3200 3950 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 3200 3950 50  0001 C CNN "Puissance"
F 6 "~" H 3200 3950 50  0001 C CNN "Tension"
F 7 "~" H 3200 3950 50  0001 C CNN "Tolérence"
F 8 "RS" H 3200 3950 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 3200 3950 50  0001 C CNN "Catégorie"
F 10 "175-5543" H 3200 3950 50  0001 C CNN "Code Fournisseur"
F 11 "10" H 3200 3950 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 3200 3950 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, GH, 7 pôles , 1.25mm 1 rangée, 1A, Droit" H 3200 3950 50  0001 C CNN "Description"
F 14 "1.25mm" H 3200 3950 50  0001 C CNN "Height"
F 15 "JST" H 3200 3950 50  0001 C CNN "Manufacturer_Name"
F 16 "BM07B-GHS-TBT (LF)(SN)" H 3200 3950 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "3,5" H 3200 3950 50  0001 C CNN "Prix Conditionnement"
F 18 "0,35" H 3200 3950 50  0001 C CNN "Prix Unitaire"
F 19 "175-5543" H 3200 3950 50  0001 C CNN "RS Part Number"
	1    3200 3950
	-1   0    0    -1  
$EndComp
Text Label 3550 3650 0    50   ~ 0
VDD
Wire Wire Line
	3400 3650 3550 3650
$Comp
L power:GND #PWR?
U 1 1 5DAA9035
P 3500 4400
AR Path="/5DAA9035" Ref="#PWR?"  Part="1" 
AR Path="/5DA7A325/5DAA9035" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 3500 4150 50  0001 C CNN
F 1 "GND" H 3505 4227 50  0000 C CNN
F 2 "" H 3500 4400 50  0001 C CNN
F 3 "" H 3500 4400 50  0001 C CNN
	1    3500 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4150 3500 4400
Text Label 3550 3850 0    50   ~ 0
ROTENA
Text Label 3550 4050 0    50   ~ 0
ROTENB
Text Label 3550 4250 0    50   ~ 0
ROTENSW
Wire Wire Line
	3400 4150 3500 4150
Wire Wire Line
	3400 4050 3550 4050
Wire Wire Line
	3400 3850 3550 3850
Wire Wire Line
	3400 3750 3500 3750
Wire Wire Line
	3500 3750 3500 3950
Wire Wire Line
	3500 3950 3400 3950
Wire Wire Line
	3500 3950 3500 4150
Connection ~ 3500 3950
Connection ~ 3500 4150
Wire Wire Line
	3400 4250 3550 4250
Wire Wire Line
	6450 3750 7300 3750
Wire Wire Line
	4850 3750 5850 3750
Wire Wire Line
	4450 3550 5850 3550
Wire Notes Line
	4250 7350 4250 6500
Wire Notes Line
	6550 7350 4250 7350
Wire Notes Line
	6550 6500 6550 7350
Wire Notes Line
	4250 6500 6550 6500
Text Notes 5900 6400 2    50   ~ 10
Trous de fixation du PCB
$Comp
L power:GND #PWR?
U 1 1 5DAD4B50
P 5400 7050
AR Path="/5DAD4B50" Ref="#PWR?"  Part="1" 
AR Path="/5DA7A325/5DAD4B50" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 5400 6800 50  0001 C CNN
F 1 "GND" H 5405 6877 50  0000 C CNN
F 2 "" H 5400 7050 50  0001 C CNN
F 3 "" H 5400 7050 50  0001 C CNN
	1    5400 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 6900 5150 6900
Connection ~ 5400 6900
Wire Wire Line
	5400 6900 5400 7050
Wire Wire Line
	5150 6900 5150 7100
Connection ~ 5150 6900
Wire Wire Line
	5650 6900 5650 6750
Connection ~ 5650 6900
Wire Wire Line
	5650 6900 5400 6900
Wire Wire Line
	5650 7100 5650 6900
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5DAD4B60
P 5150 7200
AR Path="/5DAD4B60" Ref="H?"  Part="1" 
AR Path="/5DA7A325/5DAD4B60" Ref="H8"  Part="1" 
F 0 "H8" H 5350 7150 50  0000 R CNN
F 1 "MountingHole_Pad" H 5950 7250 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5150 7200 50  0001 C CNN
F 3 "~" H 5150 7200 50  0001 C CNN
	1    5150 7200
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5DAD4B66
P 5150 6650
AR Path="/5DAD4B66" Ref="H?"  Part="1" 
AR Path="/5DA7A325/5DAD4B66" Ref="H7"  Part="1" 
F 0 "H7" H 5350 6700 50  0000 R CNN
F 1 "MountingHole_Pad" H 5900 6600 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5150 6650 50  0001 C CNN
F 3 "~" H 5150 6650 50  0001 C CNN
	1    5150 6650
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5DAD4B6C
P 5650 7200
AR Path="/5DAD4B6C" Ref="H?"  Part="1" 
AR Path="/5DA7A325/5DAD4B6C" Ref="H10"  Part="1" 
F 0 "H10" H 5550 7157 50  0000 R CNN
F 1 "MountingHole_Pad" H 5550 7248 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5650 7200 50  0001 C CNN
F 3 "~" H 5650 7200 50  0001 C CNN
	1    5650 7200
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5DAD4B72
P 5650 6650
AR Path="/5DAD4B72" Ref="H?"  Part="1" 
AR Path="/5DA7A325/5DAD4B72" Ref="H9"  Part="1" 
F 0 "H9" H 5750 6699 50  0000 L CNN
F 1 "MountingHole_Pad" H 5750 6608 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5650 6650 50  0001 C CNN
F 3 "~" H 5650 6650 50  0001 C CNN
	1    5650 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 6750 5150 6900
$EndSCHEMATC
