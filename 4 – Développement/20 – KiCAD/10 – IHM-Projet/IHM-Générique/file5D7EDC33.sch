EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:NCP1117-5.0_TO252 U1
U 1 1 5DE3B1B5
P 3850 3200
AR Path="/5D7EDC34/5DE3B1B5" Ref="U1"  Part="1" 
AR Path="/5DE3B1B5" Ref="U?"  Part="1" 
F 0 "U1" H 3850 3442 50  0000 C CNN
F 1 "NCP1117-5.0_TO252" H 3850 3351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 3850 3425 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/NCP1117-D.PDF" H 3850 3200 50  0001 C CNN
F 4 "RS" H 3850 3200 50  0001 C CNN "Fournisseur"
F 5 "802-2124" H 3850 3200 50  0001 C CNN "Code Fournisseur"
F 6 "Circuit Intégré" H 3850 3200 50  0001 C CNN "Catégorie"
F 7 "Régulateur LDO Fixe; 5 V, 2.2A, DPAK 3 broches Positif" H 3850 3200 50  0001 C CNN "Description"
F 8 "25" H 3850 3200 50  0001 C CNN "Conditionnement"
F 9 "8,90" H 3850 3200 50  0001 C CNN "Prix Conditionnement"
F 10 "0,356" H 3850 3200 50  0001 C CNN "Prix Unitaire"
F 11 "11W" H 3850 3200 50  0001 C CNN "Puissance"
F 12 "5V" H 3850 3200 50  0001 C CNN "Tension"
F 13 "2,2A" H 3850 3200 50  0001 C CNN "Courant"
F 14 "±1%" H 3850 3200 50  0001 C CNN "Tolérence"
F 15 "https://fr.rs-online.com/web/p/regulateurs-ldo/8022124/" H 3850 3200 50  0001 C CNN "RS Price/Stock"
F 16 "6.73 x 6.22 x 2.38mm" H 3850 3200 50  0001 C CNN "Height"
F 17 "ON Semiconductor" H 3850 3200 50  0001 C CNN "Manufacturer_Name"
F 18 "NCP1117DT50RKG" H 3850 3200 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "802-2124" H 3850 3200 50  0001 C CNN "RS Part Number"
	1    3850 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3200 4350 3200
Wire Wire Line
	3850 3500 3850 3600
Wire Wire Line
	3400 3200 3400 3250
Wire Wire Line
	3400 3200 3550 3200
Wire Wire Line
	3400 3550 3400 3600
Wire Wire Line
	3400 3600 3850 3600
Wire Wire Line
	3850 3600 4350 3600
Wire Wire Line
	4350 3600 4350 3550
Connection ~ 3850 3600
Wire Wire Line
	4350 3250 4350 3200
Connection ~ 4350 3200
Text HLabel 4650 3200 2    50   Output ~ 10
VDD
Wire Wire Line
	3850 3600 3850 3750
$Comp
L power:GND #PWR024
U 1 1 5DE3B1CF
P 3850 3750
AR Path="/5D7EDC34/5DE3B1CF" Ref="#PWR024"  Part="1" 
AR Path="/5DE3B1CF" Ref="#PWR?"  Part="1" 
F 0 "#PWR024" H 3850 3500 50  0001 C CNN
F 1 "GND" H 3855 3577 50  0000 C CNN
F 2 "" H 3850 3750 50  0001 C CNN
F 3 "" H 3850 3750 50  0001 C CNN
	1    3850 3750
	1    0    0    -1  
$EndComp
Wire Notes Line
	4950 2850 4950 4100
$Comp
L Misc_KropoteX:NLAST4599DTT1G IC?
U 1 1 5DE3B1E8
P 7350 3050
AR Path="/5DE3B1E8" Ref="IC?"  Part="1" 
AR Path="/5D7EDC34/5DE3B1E8" Ref="IC3"  Part="1" 
F 0 "IC3" H 7950 3315 50  0000 C CNN
F 1 "NLAST4599DTT1G" H 7950 3224 50  0000 C CNN
F 2 "Misc_KropoteX:SOT95P275X110-6N" H 8400 3150 50  0001 L CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1575/0900766b81575e26.pdf" H 8400 3050 50  0001 L CNN
F 4 "Low Voltage Single Supply SPDT Analog Switch" H 8400 2950 50  0001 L CNN "Description"
F 5 "3.1 x 1.7 x 1mm" H 8400 2850 50  0001 L CNN "Height"
F 6 "8021673P" H 8400 2750 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/8021673P" H 8400 2650 50  0001 L CNN "RS Price/Stock"
F 8 "ON Semiconductor" H 8400 2550 50  0001 L CNN "Manufacturer_Name"
F 9 "NLAST4599DTT1G" H 8400 2450 50  0001 L CNN "Manufacturer_Part_Number"
F 10 "70467418" H 8400 2350 50  0001 L CNN "Allied_Number"
F 11 "Circuit intégré" H 7350 3050 50  0001 C CNN "Catégorie"
F 12 "RS" H 7350 3050 50  0001 C CNN "Fournisseur"
F 13 "2 - 5,5 V c.c." H 7350 3050 50  0001 C CNN "Tension"
F 14 "~" H 7350 3050 50  0001 C CNN "Tolérence"
F 15 "200 mW" H 7350 3050 50  0001 C CNN "Puissance"
F 16 "802-1673" H 7350 3050 50  0001 C CNN "Code Fournisseur"
F 17 "50" H 7350 3050 50  0001 C CNN "Conditionnement"
F 18 "3,55" H 7350 3050 50  0001 C CNN "Prix Conditionnement"
F 19 "0,071" H 7350 3050 50  0001 C CNN "Prix Unitaire"
	1    7350 3050
	1    0    0    -1  
$EndComp
Text Label 6700 2800 0    50   ~ 0
GPORST
$Comp
L power:GND #PWR?
U 1 1 5DE3B1EF
P 7950 4050
AR Path="/5DE3B1EF" Ref="#PWR?"  Part="1" 
AR Path="/5D7EDC34/5DE3B1EF" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 7950 3800 50  0001 C CNN
F 1 "GND" H 7955 3877 50  0000 C CNN
F 2 "" H 7950 4050 50  0001 C CNN
F 3 "" H 7950 4050 50  0001 C CNN
	1    7950 4050
	1    0    0    -1  
$EndComp
Text Label 7300 3350 2    50   ~ 0
VDD
Wire Wire Line
	8750 3150 8550 3150
Wire Wire Line
	7350 3250 7350 3450
Text Label 8000 2700 0    50   ~ 0
VDD
Wire Wire Line
	8550 3050 8600 3050
Text Label 9100 3250 2    50   ~ 0
XRES
Connection ~ 8750 3150
$Comp
L Device:R R?
U 1 1 5DE3B1FC
P 8750 2950
AR Path="/5DE3B1FC" Ref="R?"  Part="1" 
AR Path="/5D7EDC34/5DE3B1FC" Ref="R12"  Part="1" 
F 0 "R12" H 8820 2996 50  0000 L CNN
F 1 "1k" H 8820 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8680 2950 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 8750 2950 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 8750 2950 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8750 2950 50  0001 C CNN "Puissance"
F 6 "~" H 8750 2950 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8750 2950 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8750 2950 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 8750 2950 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8750 2950 50  0001 C CNN "Conditionnement"
F 11 "~" H 8750 2950 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8750 2950 50  0001 C CNN "Description"
F 13 "RS" H 8750 2950 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8750 2950 50  0001 C CNN "Height"
F 15 "RS PRO" H 8750 2950 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 8750 2950 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 8750 2950 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 8750 2950 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 8750 2950 50  0001 C CNN "RS Part Number"
	1    8750 2950
	1    0    0    -1  
$EndComp
Text Label 9100 3150 2    50   ~ 0
RESET
Wire Wire Line
	7150 3050 7350 3050
Wire Wire Line
	7000 3250 7000 3450
Wire Wire Line
	7000 3450 7350 3450
Connection ~ 7350 3450
Wire Wire Line
	8600 3050 8600 3450
Connection ~ 7950 3450
Wire Wire Line
	7950 3450 8600 3450
Wire Wire Line
	7950 3450 7950 4000
Connection ~ 8600 3450
Wire Wire Line
	8550 3250 9100 3250
Wire Notes Line
	9150 2600 9150 4300
Wire Notes Line
	9150 4300 6700 4300
Wire Notes Line
	6700 4300 6700 2600
Wire Notes Line
	6700 2600 9150 2600
Text Notes 6550 2250 0    100  ~ 20
Analog Switch pour contrôle du reset
Wire Wire Line
	8750 3150 9300 3150
Wire Wire Line
	9100 3250 9100 3450
Wire Wire Line
	8750 2700 8750 2800
Wire Wire Line
	7300 3150 7300 3600
Wire Wire Line
	7300 2700 8750 2700
Wire Wire Line
	7300 2700 7300 3150
Connection ~ 7300 3150
Wire Wire Line
	7300 3150 7350 3150
Wire Wire Line
	8750 3150 8750 3100
Wire Wire Line
	6650 2800 7000 2800
Wire Wire Line
	7000 2800 7000 2850
Text HLabel 9300 3150 2    50   Output ~ 0
RESET
Text HLabel 7150 3600 0    50   Input ~ 0
VDD
Text HLabel 6650 2800 0    50   Input ~ 0
RESET_PSoC
Wire Wire Line
	9100 3450 9300 3450
Connection ~ 9100 3450
Text HLabel 9300 3450 2    50   Output ~ 0
XRES
Wire Wire Line
	7300 3600 7150 3600
Wire Wire Line
	4350 3200 4650 3200
Wire Wire Line
	3150 3200 3250 3200
Connection ~ 3400 3200
$Comp
L Device:CP C?
U 1 1 5F851DF6
P 4350 3400
AR Path="/5F851DF6" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5F851DF6" Ref="C26"  Part="1" 
F 0 "C26" H 4468 3446 50  0000 L CNN
F 1 "10µ" H 4468 3355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.2" H 4388 3250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1084/0900766b81084d63.pdf" H 4350 3400 50  0001 C CNN
F 4 " 756-8137" H 4350 3400 50  0001 C CNN "Code Fournisseur"
F 5 "10" H 4350 3400 50  0001 C CNN "Conditionnement"
F 6 "Condensateur électrolytique aluminium Nippon Chemi-Con, 10μF, 50V c.c., série MVE" H 4350 3400 50  0001 C CNN "Description"
F 7 "RS" H 4350 3400 50  0001 C CNN "Fournisseur"
F 8 "0,42" H 4350 3400 50  0001 C CNN "Prix Conditionnement"
F 9 "0,042" H 4350 3400 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 4350 3400 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 4350 3400 50  0001 C CNN "Tension"
F 12 "±20%" H 4350 3400 50  0001 C CNN "Tolérence"
F 13 "~" H 4350 3400 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 4350 3400 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-aluminium/7568137/" H 4350 3400 50  0001 C CNN "RS Price/Stock"
F 16 "6.3 x 5.2mm" H 4350 3400 50  0001 C CNN "Height"
F 17 "Nippon Chemi-Con" H 4350 3400 50  0001 C CNN "Manufacturer_Name"
F 18 "EMVE500ADA100MF55G" H 4350 3400 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "756-8137" H 4350 3400 50  0001 C CNN "RS Part Number"
	1    4350 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5F85329F
P 3400 3400
AR Path="/5F85329F" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5F85329F" Ref="C25"  Part="1" 
F 0 "C25" H 3518 3446 50  0000 L CNN
F 1 "10µ" H 3518 3355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.2" H 3438 3250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1084/0900766b81084d63.pdf" H 3400 3400 50  0001 C CNN
F 4 " 756-8137" H 3400 3400 50  0001 C CNN "Code Fournisseur"
F 5 "10" H 3400 3400 50  0001 C CNN "Conditionnement"
F 6 "Condensateur électrolytique aluminium Nippon Chemi-Con, 10μF, 50V c.c., série MVE" H 3400 3400 50  0001 C CNN "Description"
F 7 "RS" H 3400 3400 50  0001 C CNN "Fournisseur"
F 8 "0,42" H 3400 3400 50  0001 C CNN "Prix Conditionnement"
F 9 "0,042" H 3400 3400 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 3400 3400 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 3400 3400 50  0001 C CNN "Tension"
F 12 "±20%" H 3400 3400 50  0001 C CNN "Tolérence"
F 13 "~" H 3400 3400 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 3400 3400 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-aluminium/7568137/" H 3400 3400 50  0001 C CNN "RS Price/Stock"
F 16 "6.3 x 5.2mm" H 3400 3400 50  0001 C CNN "Height"
F 17 "Nippon Chemi-Con" H 3400 3400 50  0001 C CNN "Manufacturer_Name"
F 18 "EMVE500ADA100MF55G" H 3400 3400 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "756-8137" H 3400 3400 50  0001 C CNN "RS Part Number"
	1    3400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3450 8800 3450
Wire Wire Line
	8900 3450 9100 3450
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 5DE3B203
P 7000 3050
AR Path="/5DE3B203" Ref="JP?"  Part="1" 
AR Path="/5D7EDC34/5DE3B203" Ref="JP3"  Part="1" 
F 0 "JP3" H 7000 3250 50  0000 C CNN
F 1 "SolderJumper_3_Open" H 7350 3150 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 7000 3050 50  0001 C CNN
F 3 "" H 7000 3050 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 8048921" H 7000 3050 50  0001 C CNN "RS Price/Stock"
F 5 "" H 7000 3050 50  0001 C CNN "Puissance"
F 6 "~" H 7000 3050 50  0001 C CNN "Tension"
F 7 "~" H 7000 3050 50  0001 C CNN "Tolérence"
	1    7000 3050
	0    -1   1    0   
$EndComp
Text Notes 3350 2550 0    100  ~ 20
Réguateur 5V 2A
Wire Notes Line
	4950 4100 2950 4100
Wire Notes Line
	2950 2850 4950 2850
Wire Notes Line
	2950 4100 2950 2850
Text Label 3150 3200 2    50   ~ 0
Vcc
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5DE3B19E
P 2000 6900
AR Path="/5DE3B19E" Ref="#FLG?"  Part="1" 
AR Path="/5D7EDC34/5DE3B19E" Ref="#FLG02"  Part="1" 
F 0 "#FLG02" H 2000 6975 50  0001 C CNN
F 1 "PWR_FLAG" H 2000 7073 50  0000 C CNN
F 2 "" H 2000 6900 50  0001 C CNN
F 3 "~" H 2000 6900 50  0001 C CNN
	1    2000 6900
	-1   0    0    1   
$EndComp
Text Notes 1400 5850 0    50   ~ 10
Découplage Entrée Alimentation 
Wire Notes Line
	1000 5900 1000 7400
Wire Notes Line
	3100 7400 1000 7400
Wire Notes Line
	3100 5900 3100 7400
Wire Notes Line
	1000 5900 3100 5900
$Comp
L power:+5V #PWR?
U 1 1 5DE3B187
P 2450 6300
AR Path="/5DE3B187" Ref="#PWR?"  Part="1" 
AR Path="/5D7EDC34/5DE3B187" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 2450 6150 50  0001 C CNN
F 1 "+5V" H 2465 6473 50  0000 C CNN
F 2 "" H 2450 6300 50  0001 C CNN
F 3 "" H 2450 6300 50  0001 C CNN
	1    2450 6300
	1    0    0    -1  
$EndComp
Connection ~ 2000 6850
Wire Wire Line
	2000 6900 2000 6850
Wire Wire Line
	2200 6750 2200 6850
Wire Wire Line
	2200 6850 2450 6850
Wire Wire Line
	2200 6850 2000 6850
Connection ~ 2200 6850
Wire Wire Line
	2000 6350 2000 6300
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5DE3B193
P 2000 6300
AR Path="/5DE3B193" Ref="#FLG?"  Part="1" 
AR Path="/5D7EDC34/5DE3B193" Ref="#FLG01"  Part="1" 
F 0 "#FLG01" H 2000 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 2000 6473 50  0000 C CNN
F 2 "" H 2000 6300 50  0001 C CNN
F 3 "~" H 2000 6300 50  0001 C CNN
	1    2000 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DE3B18D
P 2450 6900
AR Path="/5DE3B18D" Ref="#PWR?"  Part="1" 
AR Path="/5D7EDC34/5DE3B18D" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 2450 6650 50  0001 C CNN
F 1 "GND" H 2455 6727 50  0000 C CNN
F 2 "" H 2450 6900 50  0001 C CNN
F 3 "" H 2450 6900 50  0001 C CNN
	1    2450 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 6850 2450 6900
Wire Wire Line
	2450 6350 2600 6350
Connection ~ 2450 6350
Wire Wire Line
	2450 6350 2450 6300
Wire Wire Line
	1650 6850 1800 6850
Wire Wire Line
	1800 6850 2000 6850
Connection ~ 1800 6850
Wire Wire Line
	1800 6750 1800 6850
Wire Wire Line
	2200 6350 2450 6350
Wire Wire Line
	2200 6350 2000 6350
Connection ~ 2200 6350
Wire Wire Line
	2200 6350 2200 6450
Connection ~ 2000 6350
Wire Wire Line
	1800 6350 2000 6350
Wire Wire Line
	1800 6350 1800 6450
$Comp
L Device:C C?
U 1 1 5DE3B177
P 2200 6600
AR Path="/5DE3B177" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5DE3B177" Ref="C24"  Part="1" 
F 0 "C24" H 2315 6646 50  0000 L CNN
F 1 "100n" H 2315 6555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2238 6450 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 2200 6600 50  0001 C CNN
F 4 "698-3251" H 2200 6600 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 2200 6600 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 2200 6600 50  0001 C CNN "Description"
F 7 "RS" H 2200 6600 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 2200 6600 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 2200 6600 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 2200 6600 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 2200 6600 50  0001 C CNN "Tension"
F 12 "±10%" H 2200 6600 50  0001 C CNN "Tolérence"
F 13 "~" H 2200 6600 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 2200 6600 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 2200 6600 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 2200 6600 50  0001 C CNN "Height"
F 17 "AVX" H 2200 6600 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 2200 6600 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 2200 6600 50  0001 C CNN "RS Part Number"
	1    2200 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 5DE3B167
P 1800 6600
AR Path="/5DE3B167" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5DE3B167" Ref="C23"  Part="1" 
F 0 "C23" H 1918 6646 50  0000 L CNN
F 1 "10µ" H 1918 6555 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.2" H 1838 6450 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1084/0900766b81084d63.pdf" H 1800 6600 50  0001 C CNN
F 4 " 756-8137" H 1800 6600 50  0001 C CNN "Code Fournisseur"
F 5 "10" H 1800 6600 50  0001 C CNN "Conditionnement"
F 6 "Condensateur électrolytique aluminium Nippon Chemi-Con, 10μF, 50V c.c., série MVE" H 1800 6600 50  0001 C CNN "Description"
F 7 "RS" H 1800 6600 50  0001 C CNN "Fournisseur"
F 8 "0,42" H 1800 6600 50  0001 C CNN "Prix Conditionnement"
F 9 "0,042" H 1800 6600 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 1800 6600 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 1800 6600 50  0001 C CNN "Tension"
F 12 "±20%" H 1800 6600 50  0001 C CNN "Tolérence"
F 13 "~" H 1800 6600 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 1800 6600 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-aluminium/7568137/" H 1800 6600 50  0001 C CNN "RS Price/Stock"
F 16 "6.3 x 5.2mm" H 1800 6600 50  0001 C CNN "Height"
F 17 "Nippon Chemi-Con" H 1800 6600 50  0001 C CNN "Manufacturer_Name"
F 18 "EMVE500ADA100MF55G" H 1800 6600 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "756-8137" H 1800 6600 50  0001 C CNN "RS Part Number"
	1    1800 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6650 1650 6850
Wire Wire Line
	1550 6650 1650 6650
Connection ~ 1800 6350
Wire Wire Line
	1650 6350 1800 6350
Wire Wire Line
	1650 6550 1650 6350
Wire Wire Line
	1550 6550 1650 6550
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5DE3B151
P 1350 6550
AR Path="/5DE3B151" Ref="J?"  Part="1" 
AR Path="/5D7EDC34/5DE3B151" Ref="J16"  Part="1" 
F 0 "J16" H 1350 6700 50  0000 C CNN
F 1 "Conn_01x02_Alimentation" V 1500 6450 50  0000 C CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MCV_1,5_2-G-3.5_1x02_P3.50mm_Vertical" H 1350 6550 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1714/0900766b817142ff.pdf" H 1350 6550 50  0001 C CNN
F 4 "220-4822" H 1350 6550 50  0001 C CNN "Code Fournisseur"
F 5 "5" H 1350 6550 50  0001 C CNN "Conditionnement"
F 6 "Embase pour circuit imprimé Phoenix Contact, MCV 1.5/ 2-G-3.81, 2 pôles , 3.81mm 1 rangée, 8A, Droit " H 1350 6550 50  0001 C CNN "Description"
F 7 "RS" H 1350 6550 50  0001 C CNN "Fournisseur"
F 8 "3,58" H 1350 6550 50  0001 C CNN "Prix Conditionnement"
F 9 "0,716" H 1350 6550 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 1350 6550 50  0001 C CNN "Puissance"
F 11 "300 V" H 1350 6550 50  0001 C CNN "Tension"
F 12 "~" H 1350 6550 50  0001 C CNN "Tolérence"
F 13 "~" H 1350 6550 50  0001 C CNN "Documentation Interne"
F 14 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/2204822/" H 1350 6550 50  0001 C CNN "RS Price/Stock"
F 15 "Connecteur" H 1350 6550 50  0001 C CNN "Catégorie"
F 16 "8 A" H 1350 6550 50  0001 C CNN "Courant"
F 17 "7,25 mm x 9,01 mm x 12,6 mm" H 1350 6550 50  0001 C CNN "Height"
F 18 "Phoenix Contact" H 1350 6550 50  0001 C CNN "Manufacturer_Name"
F 19 "1803426" H 1350 6550 50  0001 C CNN "Manufacturer_Part_Number"
F 20 "220-4822" H 1350 6550 50  0001 C CNN "RS Part Number"
	1    1350 6550
	-1   0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 616565D5
P 2750 6350
F 0 "F1" V 2553 6350 50  0000 C CNN
F 1 "Fuse" V 2644 6350 50  0000 C CNN
F 2 "Misc_KropoteX:FUSC4632X48N" V 2680 6350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/142b/0900766b8142b8c6.pdf" H 2750 6350 50  0001 C CNN
F 4 "Passif" H 2750 6350 50  0001 C CNN "Catégorie"
F 5 "867-5273" H 2750 6350 50  0001 C CNN "Code Fournisseur"
F 6 "10" H 2750 6350 50  0001 C CNN "Conditionnement"
F 7 "2,2 A" H 2750 6350 50  0001 C CNN "Courant"
F 8 "Fusible CMS réarmable, Littelfuse, 2.2A, 1.1A 1812 16V c.c." H 2750 6350 50  0001 C CNN "Description"
F 9 "RS" H 2750 6350 50  0001 C CNN "Fournisseur"
F 10 "3.41 x 0.48 x 4.37mm" H 2750 6350 50  0001 C CNN "Height"
F 11 "Littelfuse" H 2750 6350 50  0001 C CNN "Manufacturer_Name"
F 12 "MINISMDC110F/16-" H 2750 6350 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "3,74" H 2750 6350 50  0001 C CNN "Prix Conditionnement"
F 14 "0,374" H 2750 6350 50  0001 C CNN "Prix Unitaire"
F 15 "10,8 W" H 2750 6350 50  0001 C CNN "Puissance"
F 16 "867-5273" H 2750 6350 50  0001 C CNN "RS Part Number"
F 17 "https://fr.rs-online.com/web/p/fusibles-cms-rearmables/8675273/" H 2750 6350 50  0001 C CNN "RS Price/Stock"
F 18 "16V c.c." H 2750 6350 50  0001 C CNN "Tension"
F 19 "~" H 2750 6350 50  0001 C CNN "Tolérence"
	1    2750 6350
	0    1    1    0   
$EndComp
Text Label 2900 6350 0    50   ~ 0
Vcc
$Comp
L power:PWR_FLAG #FLG?
U 1 1 61663C86
P 3250 3200
AR Path="/61663C86" Ref="#FLG?"  Part="1" 
AR Path="/5D7EDC34/61663C86" Ref="#FLG03"  Part="1" 
F 0 "#FLG03" H 3250 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 3373 50  0000 C CNN
F 2 "" H 3250 3200 50  0001 C CNN
F 3 "~" H 3250 3200 50  0001 C CNN
	1    3250 3200
	1    0    0    -1  
$EndComp
Connection ~ 3250 3200
Wire Wire Line
	3250 3200 3400 3200
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5D987BBB
P 8800 3650
AR Path="/5D7EDDD9/5D987BBB" Ref="J?"  Part="1" 
AR Path="/5D987BBB" Ref="J?"  Part="1" 
AR Path="/5D7EDC34/5D987BBB" Ref="J17"  Part="1" 
F 0 "J17" V 8800 3750 50  0000 L CNN
F 1 "Conn_BTN_Reset_prop2" V 8900 3450 50  0000 L CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal" H 8800 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/144c/0900766b8144c299.pdf" H 8800 3650 50  0001 C CNN
F 4 "Connecteur" H 8800 3650 50  0001 C CNN "Catégorie"
F 5 " 896-7500" H 8800 3650 50  0001 C CNN "Code Fournisseur"
F 6 "10" H 8800 3650 50  0001 C CNN "Conditionnement"
F 7 "2,5 A" H 8800 3650 50  0001 C CNN "Courant"
F 8 "Embase pour CI Molex, Pico-SPOX, 2 pôles , 1.5mm 1 rangée, 2.5A, Verticale" H 8800 3650 50  0001 C CNN "Description"
F 9 "RS" H 8800 3650 50  0001 C CNN "Fournisseur"
F 10 "1,5mm" H 8800 3650 50  0001 C CNN "Height"
F 11 "Molex" H 8800 3650 50  0001 C CNN "Manufacturer_Name"
F 12 "87437-0243" H 8800 3650 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "4,51" H 8800 3650 50  0001 C CNN "Prix Conditionnement"
F 14 "0,451" H 8800 3650 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 8800 3650 50  0001 C CNN "Puissance"
F 16 "896-7500" H 8800 3650 50  0001 C CNN "RS Part Number"
F 17 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/8967500/" H 8800 3650 50  0001 C CNN "RS Price/Stock"
F 18 "250 V(cc/ca)" H 8800 3650 50  0001 C CNN "Tension"
F 19 "~" H 8800 3650 50  0001 C CNN "Tolérence"
	1    8800 3650
	0    -1   1    0   
$EndComp
Wire Wire Line
	7350 3450 7950 3450
$Comp
L Device:C C?
U 1 1 5DA38B00
P 7600 3800
AR Path="/5DA38B00" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5DA38B00" Ref="C28"  Part="1" 
F 0 "C28" H 7715 3846 50  0000 L CNN
F 1 "100n" H 7715 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7638 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 7600 3800 50  0001 C CNN
F 4 "698-3251" H 7600 3800 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 7600 3800 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 7600 3800 50  0001 C CNN "Description"
F 7 "RS" H 7600 3800 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 7600 3800 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 7600 3800 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 7600 3800 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 7600 3800 50  0001 C CNN "Tension"
F 12 "±10%" H 7600 3800 50  0001 C CNN "Tolérence"
F 13 "~" H 7600 3800 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 7600 3800 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 7600 3800 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 7600 3800 50  0001 C CNN "Height"
F 17 "AVX" H 7600 3800 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 7600 3800 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 7600 3800 50  0001 C CNN "RS Part Number"
	1    7600 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7300 3600 7600 3600
Wire Wire Line
	7600 3600 7600 3650
Connection ~ 7300 3600
Wire Wire Line
	7600 3950 7600 4000
Wire Wire Line
	7600 4000 7950 4000
Connection ~ 7950 4000
Wire Wire Line
	7950 4000 7950 4050
Text Notes 3200 2800 0    50   ~ 0
À l'usage principal de la partie puissance\n      (NEOPIXEL, AUDIO, µLCD, etc.)
Text Notes 1300 5350 0    100  ~ 20
La régulation permet de connecter \nune alimentation non-stabilisée de 5,5 à 14 V\n\nL'alimentation transforme jusqu'à 2,2  Ampères, \nmais un fusible auto-réarmable protège \ntout le circuit au-delà d'un courant de 2 A.
Text Notes 6700 6150 0    100  ~ 20
Un SolderJumper permet de choisir si une broche\ndu µCU est dédiée au reset du µLCD.\n\nSi tel est le cas, un switch analogique permet de \nreseter le µCU et le µLCD en même temps, \nvia un bouton poussoir déporté\n(à l'arrière du boîtier par exemple), \net de ne reseter que le µLCD \nvia un niveau haut sur le P12[3] du PSoC.
Text Notes 1800 1400 0    200  ~ 40
Gestion de l'énergie, alimentation, sécurité et reset
Wire Notes Line width 40 rgb(0, 185, 6)
	10400 800  1400 800 
Wire Notes Line width 40 rgb(0, 185, 6)
	10400 1800 1400 1800
Wire Notes Line width 40 rgb(0, 185, 6)
	1400 800  1400 1800
Wire Notes Line width 40 rgb(0, 185, 6)
	10400 800  10400 1800
Text Notes 6750 2550 0    66   ~ 0
  Au repos (quand SELECT est à l'état bas),\nla connexion est entre les broches NC et COM
$EndSCHEMATC
