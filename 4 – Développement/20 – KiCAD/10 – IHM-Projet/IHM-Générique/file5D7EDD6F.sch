EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
Wire Wire Line
	3400 3450 3200 3450
Wire Wire Line
	3100 3550 3400 3550
Wire Wire Line
	3100 3650 3400 3650
Wire Wire Line
	3100 3750 3400 3750
Wire Wire Line
	3100 3850 3400 3850
Wire Wire Line
	3100 3950 3400 3950
Wire Wire Line
	2650 4050 3400 4050
Text HLabel 3100 3450 0    50   Input ~ 0
VDD
Text HLabel 6150 3700 2    50   BiDi ~ 0
I2C1_SDA
Text HLabel 6150 3800 2    50   Input ~ 0
I2C1_SCL
Text HLabel 3100 3850 0    50   Input ~ 0
I2Cext1
Text HLabel 3100 3950 0    50   Input ~ 0
I2Cext0
Text HLabel 3100 3750 0    50   Input ~ 0
I2Cext2
$Comp
L power:GND #PWR018
U 1 1 5E53CD9B
P 2650 4150
F 0 "#PWR018" H 2650 3900 50  0001 C CNN
F 1 "GND" H 2655 3977 50  0000 C CNN
F 2 "" H 2650 4150 50  0001 C CNN
F 3 "" H 2650 4150 50  0001 C CNN
	1    2650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3600 5900 3600
Wire Wire Line
	6150 3700 5650 3700
Wire Wire Line
	6150 3800 5650 3800
Text HLabel 6150 3600 2    50   Input ~ 0
VDD
Text HLabel 3100 3550 0    50   BiDi ~ 0
I2C0_SDA
Text HLabel 3100 3650 0    50   Input ~ 0
I2C0_SCL
$Comp
L power:GND #PWR019
U 1 1 5E53F216
P 6600 4000
F 0 "#PWR019" H 6600 3750 50  0001 C CNN
F 1 "GND" H 6605 3827 50  0000 C CNN
F 2 "" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5650 3900 6600 3900
Wire Wire Line
	8350 3600 8600 3600
Wire Wire Line
	10350 3600 10150 3600
Wire Wire Line
	10350 3700 9850 3700
Wire Wire Line
	9850 3800 10350 3800
Text HLabel 8350 3600 0    50   Input ~ 0
VDD
Text HLabel 10350 3600 2    50   Input ~ 0
VDD
$Comp
L power:GND #PWR020
U 1 1 5E55CBC7
P 8750 4000
F 0 "#PWR020" H 8750 3750 50  0001 C CNN
F 1 "GND" H 8755 3827 50  0000 C CNN
F 2 "" H 8750 4000 50  0001 C CNN
F 3 "" H 8750 4000 50  0001 C CNN
	1    8750 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5E55DF2A
P 10700 4000
F 0 "#PWR021" H 10700 3750 50  0001 C CNN
F 1 "GND" H 10705 3827 50  0000 C CNN
F 2 "" H 10700 4000 50  0001 C CNN
F 3 "" H 10700 4000 50  0001 C CNN
	1    10700 4000
	-1   0    0    -1  
$EndComp
Text HLabel 8350 3700 0    50   Input ~ 0
Prog_SWDCLK
Text HLabel 8350 3800 0    50   BiDi ~ 0
Prog_SWDIO
Text HLabel 10350 3700 2    50   Output ~ 0
P_SWO
Text HLabel 10350 3800 2    50   Input ~ 0
P_TDI
Wire Wire Line
	9850 3900 10700 3900
Wire Wire Line
	8750 3900 8850 3900
Connection ~ 3200 3450
Wire Wire Line
	3200 3450 3100 3450
Wire Wire Line
	3200 3150 2650 3150
Wire Wire Line
	2650 3150 2650 4050
Connection ~ 2650 4050
Wire Wire Line
	2650 4050 2650 4150
Wire Wire Line
	5900 3300 5900 3250
Wire Wire Line
	5900 3250 6600 3250
Wire Wire Line
	6600 3250 6600 3900
Wire Wire Line
	6600 4000 6600 3900
Connection ~ 6600 3900
Connection ~ 8600 3600
Wire Wire Line
	8600 3600 8850 3600
Wire Wire Line
	8750 4000 8750 3900
Connection ~ 10150 3600
Wire Wire Line
	10150 3600 9850 3600
Wire Wire Line
	10150 3300 10150 3250
Wire Wire Line
	10150 3250 10700 3250
Wire Wire Line
	10700 3250 10700 3900
Wire Wire Line
	10700 3900 10700 4000
Connection ~ 10700 3900
Connection ~ 5900 3600
Wire Wire Line
	5900 3600 6150 3600
Wire Wire Line
	8750 3900 7750 3900
Wire Wire Line
	7750 3900 7750 3300
Wire Wire Line
	7750 3300 8600 3300
Connection ~ 8750 3900
Wire Wire Line
	8350 3700 8850 3700
Wire Wire Line
	8850 3800 8350 3800
Wire Wire Line
	1850 3400 1650 3400
Wire Wire Line
	1550 3500 1850 3500
Wire Wire Line
	1550 3600 1850 3600
Wire Wire Line
	1550 3700 1850 3700
Wire Wire Line
	1550 3800 1850 3800
Wire Wire Line
	1550 3900 1850 3900
Wire Wire Line
	1100 4000 1850 4000
Text HLabel 1550 3400 0    50   Input ~ 0
VDD
Text HLabel 1550 3800 0    50   Input ~ 0
I2Cext1
Text HLabel 1550 3900 0    50   Input ~ 0
I2Cext0
Text HLabel 1550 3700 0    50   Input ~ 0
I2Cext2
$Comp
L power:GND #PWR017
U 1 1 616B0E49
P 1100 4100
F 0 "#PWR017" H 1100 3850 50  0001 C CNN
F 1 "GND" H 1105 3927 50  0000 C CNN
F 2 "" H 1100 4100 50  0001 C CNN
F 3 "" H 1100 4100 50  0001 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
Connection ~ 1650 3400
Wire Wire Line
	1650 3400 1550 3400
Wire Wire Line
	1650 3100 1100 3100
Wire Wire Line
	1100 3100 1100 4000
Connection ~ 1100 4000
Wire Wire Line
	1100 4000 1100 4100
$Comp
L Device:C C?
U 1 1 616B0E5B
P 1650 3250
AR Path="/616B0E5B" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/616B0E5B" Ref="C?"  Part="1" 
AR Path="/5D7EDD70/616B0E5B" Ref="C18"  Part="1" 
F 0 "C18" H 1765 3296 50  0000 L CNN
F 1 "100n" H 1765 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1688 3100 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 1650 3250 50  0001 C CNN
F 4 "698-3251" H 1650 3250 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 1650 3250 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 1650 3250 50  0001 C CNN "Description"
F 7 "RS" H 1650 3250 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 1650 3250 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 1650 3250 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 1650 3250 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 1650 3250 50  0001 C CNN "Tension"
F 12 "±10%" H 1650 3250 50  0001 C CNN "Tolérence"
F 13 "~" H 1650 3250 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 1650 3250 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 1650 3250 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 1650 3250 50  0001 C CNN "Height"
F 17 "AVX" H 1650 3250 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 1650 3250 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 1650 3250 50  0001 C CNN "RS Part Number"
	1    1650 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J?
U 1 1 616B0E36
P 2050 3700
AR Path="/616B0E36" Ref="J?"  Part="1" 
AR Path="/5D7EDD70/616B0E36" Ref="J11"  Part="1" 
F 0 "J11" H 2130 3742 50  0000 L CNN
F 1 "I2C_extention" H 2130 3651 50  0000 L CNN
F 2 "Connector_JST:JST_GH_BM07B-GHS-TBT_1x07-1MP_P1.25mm_Vertical" H 2050 3700 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654ef2.pdf" H 2050 3700 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 2050 3700 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 2050 3700 50  0001 C CNN "Puissance"
F 6 "~" H 2050 3700 50  0001 C CNN "Tension"
F 7 "~" H 2050 3700 50  0001 C CNN "Tolérence"
F 8 "RS" H 2050 3700 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 2050 3700 50  0001 C CNN "Catégorie"
F 10 "175-5543" H 2050 3700 50  0001 C CNN "Code Fournisseur"
F 11 "10" H 2050 3700 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 2050 3700 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, GH, 7 pôles , 1.25mm 1 rangée, 1A, Droit" H 2050 3700 50  0001 C CNN "Description"
F 14 "1.25mm" H 2050 3700 50  0001 C CNN "Height"
F 15 "JST" H 2050 3700 50  0001 C CNN "Manufacturer_Name"
F 16 "BM07B-GHS-TBT (LF)(SN)" H 2050 3700 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "3,5" H 2050 3700 50  0001 C CNN "Prix Conditionnement"
F 18 "0,35" H 2050 3700 50  0001 C CNN "Prix Unitaire"
F 19 "175-5543" H 2050 3700 50  0001 C CNN "RS Part Number"
	1    2050 3700
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5EB58AC0
P 5900 3450
AR Path="/5EB58AC0" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5EB58AC0" Ref="C?"  Part="1" 
AR Path="/5D7EDD70/5EB58AC0" Ref="C20"  Part="1" 
F 0 "C20" H 6015 3496 50  0000 L CNN
F 1 "100n" H 6015 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5938 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 5900 3450 50  0001 C CNN
F 4 "698-3251" H 5900 3450 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 5900 3450 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 5900 3450 50  0001 C CNN "Description"
F 7 "RS" H 5900 3450 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 5900 3450 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 5900 3450 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 5900 3450 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 5900 3450 50  0001 C CNN "Tension"
F 12 "±10%" H 5900 3450 50  0001 C CNN "Tolérence"
F 13 "~" H 5900 3450 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 5900 3450 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 5900 3450 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 5900 3450 50  0001 C CNN "Height"
F 17 "AVX" H 5900 3450 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 5900 3450 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 5900 3450 50  0001 C CNN "RS Part Number"
	1    5900 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EB14E16
P 10150 3450
AR Path="/5EB14E16" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5EB14E16" Ref="C?"  Part="1" 
AR Path="/5D7EDD70/5EB14E16" Ref="C22"  Part="1" 
F 0 "C22" H 10265 3496 50  0000 L CNN
F 1 "100n" H 10265 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10188 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 10150 3450 50  0001 C CNN
F 4 "698-3251" H 10150 3450 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 10150 3450 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 10150 3450 50  0001 C CNN "Description"
F 7 "RS" H 10150 3450 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 10150 3450 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 10150 3450 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 10150 3450 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 10150 3450 50  0001 C CNN "Tension"
F 12 "±10%" H 10150 3450 50  0001 C CNN "Tolérence"
F 13 "~" H 10150 3450 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 10150 3450 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 10150 3450 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 10150 3450 50  0001 C CNN "Height"
F 17 "AVX" H 10150 3450 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 10150 3450 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 10150 3450 50  0001 C CNN "RS Part Number"
	1    10150 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EB148C7
P 8600 3450
AR Path="/5EB148C7" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5EB148C7" Ref="C?"  Part="1" 
AR Path="/5D7EDD70/5EB148C7" Ref="C21"  Part="1" 
F 0 "C21" H 8715 3496 50  0000 L CNN
F 1 "100n" H 8715 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8638 3300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 8600 3450 50  0001 C CNN
F 4 "698-3251" H 8600 3450 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 8600 3450 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 8600 3450 50  0001 C CNN "Description"
F 7 "RS" H 8600 3450 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 8600 3450 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 8600 3450 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 8600 3450 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 8600 3450 50  0001 C CNN "Tension"
F 12 "±10%" H 8600 3450 50  0001 C CNN "Tolérence"
F 13 "~" H 8600 3450 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 8600 3450 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 8600 3450 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 8600 3450 50  0001 C CNN "Height"
F 17 "AVX" H 8600 3450 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 8600 3450 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 8600 3450 50  0001 C CNN "RS Part Number"
	1    8600 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EB12CBE
P 3200 3300
AR Path="/5EB12CBE" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5EB12CBE" Ref="C?"  Part="1" 
AR Path="/5D7EDD70/5EB12CBE" Ref="C19"  Part="1" 
F 0 "C19" H 3315 3346 50  0000 L CNN
F 1 "100n" H 3315 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3238 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 3200 3300 50  0001 C CNN
F 4 "698-3251" H 3200 3300 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 3200 3300 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 3200 3300 50  0001 C CNN "Description"
F 7 "RS" H 3200 3300 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 3200 3300 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 3200 3300 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 3200 3300 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 3200 3300 50  0001 C CNN "Tension"
F 12 "±10%" H 3200 3300 50  0001 C CNN "Tolérence"
F 13 "~" H 3200 3300 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 3200 3300 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 3200 3300 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 3200 3300 50  0001 C CNN "Height"
F 17 "AVX" H 3200 3300 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 3200 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 3200 3300 50  0001 C CNN "RS Part Number"
	1    3200 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J14
U 1 1 5E54F8A8
P 9050 3800
F 0 "J14" H 9050 3400 50  0000 C CNN
F 1 "Prog" H 9050 3500 50  0000 C CNN
F 2 "Misc_KropoteX:TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial" H 9050 3800 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1357/0900766b81357f2f.pdf" H 9050 3800 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1724913" H 9050 3800 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9050 3800 50  0001 C CNN "Puissance"
F 6 "250 V c.a./c.c." H 9050 3800 50  0001 C CNN "Tension"
F 7 "~" H 9050 3800 50  0001 C CNN "Tolérence"
F 8 "RS" H 9050 3800 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 9050 3800 50  0001 C CNN "Catégorie"
F 10 "172-4913" H 9050 3800 50  0001 C CNN "Code Fournisseur"
F 11 "500" H 9050 3800 50  0001 C CNN "Conditionnement"
F 12 "3 A" H 9050 3800 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, XH, 4 pôles , 2.5mm 1 rangée, 3A, Droit " H 9050 3800 50  0001 C CNN "Description"
F 14 "2.5mm" H 9050 3800 50  0001 C CNN "Height"
F 15 "JST" H 9050 3800 50  0001 C CNN "Manufacturer_Name"
F 16 "49" H 9050 3800 50  0001 C CNN "Prix Conditionnement"
F 17 "0,098" H 9050 3800 50  0001 C CNN "Prix Unitaire"
F 18 " 172-4913" H 9050 3800 50  0001 C CNN "RS Part Number"
F 19 "B4B-XH-A(LF)(SN) " H 9050 3800 50  0001 C CNN "Manufacturer_Part_Number"
	1    9050 3800
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J15
U 1 1 5E54E6C1
P 9650 3700
F 0 "J15" H 9650 4000 50  0000 C CNN
F 1 "JTAG" H 9650 3900 50  0000 C CNN
F 2 "Misc_KropoteX:TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial" H 9650 3700 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1357/0900766b81357f2f.pdf" H 9650 3700 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1724913" H 9650 3700 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9650 3700 50  0001 C CNN "Puissance"
F 6 "250 V c.a./c.c." H 9650 3700 50  0001 C CNN "Tension"
F 7 "~" H 9650 3700 50  0001 C CNN "Tolérence"
F 8 "RS" H 9650 3700 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 9650 3700 50  0001 C CNN "Catégorie"
F 10 "172-4913" H 9650 3700 50  0001 C CNN "Code Fournisseur"
F 11 "500" H 9650 3700 50  0001 C CNN "Conditionnement"
F 12 "3 A" H 9650 3700 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, XH, 4 pôles , 2.5mm 1 rangée, 3A, Droit " H 9650 3700 50  0001 C CNN "Description"
F 14 "2.5mm" H 9650 3700 50  0001 C CNN "Height"
F 15 "JST" H 9650 3700 50  0001 C CNN "Manufacturer_Name"
F 16 "49" H 9650 3700 50  0001 C CNN "Prix Conditionnement"
F 17 "0,098" H 9650 3700 50  0001 C CNN "Prix Unitaire"
F 18 " 172-4913" H 9650 3700 50  0001 C CNN "RS Part Number"
F 19 "B4B-XH-A(LF)(SN) " H 9650 3700 50  0001 C CNN "Manufacturer_Part_Number"
	1    9650 3700
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J13
U 1 1 5E53D2E6
P 5450 3700
F 0 "J13" H 5368 4017 50  0000 C CNN
F 1 "I2C0" H 5368 3926 50  0000 C CNN
F 2 "Misc_KropoteX:TE_AMP-Mini-CT_292207-4_1x04_1.5mm_vertial" H 5450 3700 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1357/0900766b81357f2f.pdf" H 5450 3700 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1724913" H 5450 3700 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 5450 3700 50  0001 C CNN "Puissance"
F 6 "250 V c.a./c.c." H 5450 3700 50  0001 C CNN "Tension"
F 7 "~" H 5450 3700 50  0001 C CNN "Tolérence"
F 8 "RS" H 5450 3700 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 5450 3700 50  0001 C CNN "Catégorie"
F 10 "172-4913" H 5450 3700 50  0001 C CNN "Code Fournisseur"
F 11 "500" H 5450 3700 50  0001 C CNN "Conditionnement"
F 12 "3 A" H 5450 3700 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, XH, 4 pôles , 2.5mm 1 rangée, 3A, Droit " H 5450 3700 50  0001 C CNN "Description"
F 14 "2.5mm" H 5450 3700 50  0001 C CNN "Height"
F 15 "JST" H 5450 3700 50  0001 C CNN "Manufacturer_Name"
F 16 "49" H 5450 3700 50  0001 C CNN "Prix Conditionnement"
F 17 "0,098" H 5450 3700 50  0001 C CNN "Prix Unitaire"
F 18 " 172-4913" H 5450 3700 50  0001 C CNN "RS Part Number"
F 19 "B4B-XH-A(LF)(SN) " H 5450 3700 50  0001 C CNN "Manufacturer_Part_Number"
	1    5450 3700
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J?
U 1 1 5E53C393
P 3600 3750
AR Path="/5E53C393" Ref="J?"  Part="1" 
AR Path="/5D7EDD70/5E53C393" Ref="J12"  Part="1" 
F 0 "J12" H 3680 3792 50  0000 L CNN
F 1 "I2C_extention" H 3680 3701 50  0000 L CNN
F 2 "Connector_JST:JST_GH_BM07B-GHS-TBT_1x07-1MP_P1.25mm_Vertical" H 3600 3750 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654ef2.pdf" H 3600 3750 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products" H 3600 3750 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 3600 3750 50  0001 C CNN "Puissance"
F 6 "~" H 3600 3750 50  0001 C CNN "Tension"
F 7 "~" H 3600 3750 50  0001 C CNN "Tolérence"
F 8 "RS" H 3600 3750 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 3600 3750 50  0001 C CNN "Catégorie"
F 10 "175-5543" H 3600 3750 50  0001 C CNN "Code Fournisseur"
F 11 "10" H 3600 3750 50  0001 C CNN "Conditionnement"
F 12 "1 A" H 3600 3750 50  0001 C CNN "Courant"
F 13 "Embase pour CI JST, GH, 7 pôles , 1.25mm 1 rangée, 1A, Droit" H 3600 3750 50  0001 C CNN "Description"
F 14 "1.25mm" H 3600 3750 50  0001 C CNN "Height"
F 15 "JST" H 3600 3750 50  0001 C CNN "Manufacturer_Name"
F 16 "BM07B-GHS-TBT (LF)(SN)" H 3600 3750 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "3,5" H 3600 3750 50  0001 C CNN "Prix Conditionnement"
F 18 "0,35" H 3600 3750 50  0001 C CNN "Prix Unitaire"
F 19 "175-5543" H 3600 3750 50  0001 C CNN "RS Part Number"
	1    3600 3750
	1    0    0    1   
$EndComp
Text HLabel 1550 3500 0    50   BiDi ~ 0
I2C0_SDA
Text HLabel 1550 3600 0    50   Input ~ 0
I2C0_SCL
Wire Notes Line
	900  2950 4250 2950
Wire Notes Line
	4250 2950 4250 4500
Wire Notes Line
	4250 4500 900  4500
Wire Notes Line
	900  4500 900  2950
Text Notes 1550 2700 0    100  ~ 20
Connecteurs d'extension I²C
Text Notes 1900 2850 0    50   ~ 0
Vers cartes filles (cartes d'extension)
Wire Notes Line
	5200 2950 6750 2950
Wire Notes Line
	6750 2950 6750 4500
Wire Notes Line
	6750 4500 5200 4500
Wire Notes Line
	5200 4500 5200 2950
Wire Notes Line
	7550 2950 7550 4500
Wire Notes Line
	7550 4500 10850 4500
Wire Notes Line
	10850 4500 10850 2950
Wire Notes Line
	10850 2950 7550 2950
Text Notes 5400 2850 0    100  ~ 20
Connecteur I²C
Text Notes 7900 2850 0    100  ~ 20
Connecteurs JTAG/Programmation
Text Notes 2450 1650 0    200  ~ 40
Périphériques de communnication extérieur
Wire Notes Line width 40 rgb(0, 185, 6)
	2000 1000 2000 2000
Wire Notes Line width 40 rgb(0, 185, 6)
	9750 1000 9750 2000
Wire Notes Line width 40 rgb(0, 185, 6)
	2000 2000 9750 2000
Wire Notes Line width 40 rgb(0, 185, 6)
	2000 1000 9750 1000
Text Notes 650  5750 0    87   ~ 17
Les cartes filles d'extension viendront se brancher ici,\net permettrons, via un extendeur ou un ADC I²C de\ntransformer des signaux analogiques en signaux numériques\net de comuniquer avec l'un des µCU via le protocole série.\n\nLes broches I2Cext2 à I2Cext0 permettent d'y relier des signaux \nd'interruption, de configuration ou encore d'activation.\nVous n'avez nul besoin de vous en servir pour toutes les utilisations.
Text Notes 5250 5000 0    87   ~ 17
Second port I²C qui\npet être utilisé pour\nn'importe quel usage
Text Notes 7550 4850 0    87   ~ 17
Ces connecteurs correspondent aux périphériques\n“fixes” ou “localisés” du µCU CYC58LP
$EndSCHEMATC
