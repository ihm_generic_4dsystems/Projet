EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
$Comp
L Misc_KropoteX:TPA6205A1DGN IC1
U 1 1 5DAD6975
P 7600 2750
F 0 "IC1" H 7975 2915 50  0000 C CNN
F 1 "TPA6205A1DGN" H 7975 2824 50  0000 C CNN
F 2 "Misc_KropoteX:SOP65P490X110-9N" H 9725 2645 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tpa6205a1.pdf" H 7600 2700 50  0001 C CNN
F 4 "Texas Instruments,TPA6205A1DGN,IC,Audio Texas Instruments TPA6205A1DGN Audio Amplifier IC, Class-AB Mono -70dB, 8-Pin MSOP" H 11605 2550 50  0001 C CNN "Description"
F 5 "1.1mm" H 9105 2450 50  0001 C CNN "Height"
F 6 "8124268P" H 9235 2345 50  0001 C CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/8124268P" H 10065 2250 50  0001 C CNN "RS Price/Stock"
F 8 "Texas Instruments" H 9390 2150 50  0001 C CNN "Manufacturer_Name"
F 9 "TPA6205A1DGN" H 9330 2050 50  0001 C CNN "Allied_Number"
F 10 "Circuit Intégré" H 7600 2750 50  0001 C CNN "Catégorie"
F 11 "Farnell" H 7600 2750 50  0001 C CNN "Fournisseur"
F 12 "5,5" H 7600 2750 50  0001 C CNN "Tension"
F 13 "~" H 7600 2750 50  0001 C CNN "Tolérence"
F 14 "1,25 W" H 7600 2750 50  0001 C CNN "Puissance"
F 15 "3116960" H 7600 2750 50  0001 C CNN "Code Fournisseur"
F 16 "1" H 7600 2750 50  0001 C CNN "Conditionnement"
F 17 "TPA6205A1DGN" H 7600 2750 50  0001 C CNN "Manufacturer_Part_Number"
F 18 "0,973" H 7600 2750 50  0001 C CNN "Prix Conditionnement"
F 19 "0,973" H 7600 2750 50  0001 C CNN "Prix Unitaire"
	1    7600 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5DAE0A48
P 6650 3000
F 0 "C11" V 6398 3000 50  0000 C CNN
F 1 "220n" V 6489 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6688 2850 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 6650 3000 50  0001 C CNN
F 4 "Passif" H 6650 3000 50  0001 C CNN "Catégorie"
F 5 "391-135" H 6650 3000 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 6650 3000 50  0001 C CNN "Conditionnement"
F 7 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6650 3000 50  0001 C CNN "Description"
F 8 "RS" H 6650 3000 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 6650 3000 50  0001 C CNN "Height"
F 10 "AVX" H 6650 3000 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 6650 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,15" H 6650 3000 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 6650 3000 50  0001 C CNN "Prix Unitaire"
F 14 "391-135" H 6650 3000 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/0391135/" H 6650 3000 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 6650 3000 50  0001 C CNN "Tension"
F 17 "±10%" H 6650 3000 50  0001 C CNN "Tolérence"
F 18 "~" H 6650 3000 50  0001 C CNN "Puissance"
	1    6650 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5DAE4A78
P 7000 3000
F 0 "R8" V 7207 3000 50  0000 C CNN
F 1 "10k" V 7116 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6930 3000 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 7000 3000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 7000 3000 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 7000 3000 50  0001 C CNN "Puissance"
F 6 "~" H 7000 3000 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 7000 3000 50  0001 C CNN "Tolérence"
F 8 "Passif" H 7000 3000 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 7000 3000 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 7000 3000 50  0001 C CNN "Conditionnement"
F 11 "~" H 7000 3000 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 7000 3000 50  0001 C CNN "Description"
F 13 "RS" H 7000 3000 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 7000 3000 50  0001 C CNN "Height"
F 15 "RS PRO" H 7000 3000 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 7000 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 7000 3000 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 7000 3000 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 7000 3000 50  0001 C CNN "RS Part Number"
	1    7000 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 5DAE51BD
P 7000 3150
F 0 "R9" V 6800 3150 50  0000 C CNN
F 1 "10k" V 6900 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6930 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 7000 3150 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 7000 3150 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 7000 3150 50  0001 C CNN "Puissance"
F 6 "~" H 7000 3150 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 7000 3150 50  0001 C CNN "Tolérence"
F 8 "Passif" H 7000 3150 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 7000 3150 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 7000 3150 50  0001 C CNN "Conditionnement"
F 11 "~" H 7000 3150 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 7000 3150 50  0001 C CNN "Description"
F 13 "RS" H 7000 3150 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 7000 3150 50  0001 C CNN "Height"
F 15 "RS PRO" H 7000 3150 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 7000 3150 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 7000 3150 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 7000 3150 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 7000 3150 50  0001 C CNN "RS Part Number"
	1    7000 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5DAE994B
P 6100 3850
F 0 "#PWR07" H 6100 3600 50  0001 C CNN
F 1 "GND" H 6105 3677 50  0000 C CNN
F 2 "" H 6100 3850 50  0001 C CNN
F 3 "" H 6100 3850 50  0001 C CNN
	1    6100 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5DAE9D81
P 8500 3650
F 0 "#PWR08" H 8500 3400 50  0001 C CNN
F 1 "GND" H 8505 3477 50  0000 C CNN
F 2 "" H 8500 3650 50  0001 C CNN
F 3 "" H 8500 3650 50  0001 C CNN
	1    8500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3600 8500 3600
Wire Wire Line
	8500 3600 8500 3650
Wire Wire Line
	8500 3600 8500 3300
Connection ~ 8500 3600
Wire Wire Line
	8450 3300 8500 3300
Wire Wire Line
	8450 2850 8550 2850
Wire Wire Line
	7500 3150 7450 3150
Wire Wire Line
	6850 3150 6650 3150
Wire Wire Line
	6650 3150 6650 3450
Wire Wire Line
	6650 3750 6650 3800
Wire Wire Line
	6100 3800 6100 3850
Connection ~ 6100 3800
Wire Wire Line
	6900 3450 6900 3400
Wire Wire Line
	7450 3150 7450 3900
Wire Wire Line
	7450 3900 7850 3900
Wire Wire Line
	8550 3150 8450 3150
Wire Wire Line
	7450 2450 7450 3000
Wire Wire Line
	7450 3000 7500 3000
Wire Wire Line
	6900 3800 6900 3750
$Comp
L Device:C C12
U 1 1 5DB09D72
P 6650 3600
F 0 "C12" H 6900 3550 50  0000 R CNN
F 1 "220n" H 7000 3650 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6688 3450 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 6650 3600 50  0001 C CNN
F 4 "Passif" H 6650 3600 50  0001 C CNN "Catégorie"
F 5 "391-135" H 6650 3600 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 6650 3600 50  0001 C CNN "Conditionnement"
F 7 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6650 3600 50  0001 C CNN "Description"
F 8 "RS" H 6650 3600 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 6650 3600 50  0001 C CNN "Height"
F 10 "AVX" H 6650 3600 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 6650 3600 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,15" H 6650 3600 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 6650 3600 50  0001 C CNN "Prix Unitaire"
F 14 "391-135" H 6650 3600 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/0391135/" H 6650 3600 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 6650 3600 50  0001 C CNN "Tension"
F 17 "±10%" H 6650 3600 50  0001 C CNN "Tolérence"
F 18 "~" H 6650 3600 50  0001 C CNN "Puissance"
	1    6650 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C13
U 1 1 5DB0A3C1
P 6900 3600
F 0 "C13" H 6785 3554 50  0000 R CNN
F 1 "220n" H 6785 3645 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6938 3450 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 6900 3600 50  0001 C CNN
F 4 "Passif" H 6900 3600 50  0001 C CNN "Catégorie"
F 5 "391-135" H 6900 3600 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 6900 3600 50  0001 C CNN "Conditionnement"
F 7 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6900 3600 50  0001 C CNN "Description"
F 8 "RS" H 6900 3600 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 6900 3600 50  0001 C CNN "Height"
F 10 "AVX" H 6900 3600 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 6900 3600 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,15" H 6900 3600 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 6900 3600 50  0001 C CNN "Prix Unitaire"
F 14 "391-135" H 6900 3600 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/0391135/" H 6900 3600 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 6900 3600 50  0001 C CNN "Tension"
F 17 "±10%" H 6900 3600 50  0001 C CNN "Tolérence"
F 18 "~" H 6900 3600 50  0001 C CNN "Puissance"
	1    6900 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Variable R15
U 1 1 5DB20780
P 8400 2450
F 0 "R15" V 8193 2450 50  0000 C CNN
F 1 "100k" V 8284 2450 50  0000 C CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3314J_Vertical" V 8330 2450 50  0001 C CNN
F 3 "~" H 8400 2450 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8049000" H 8400 2450 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8400 2450 50  0001 C CNN "Puissance"
F 6 "~" H 8400 2450 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8400 2450 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8400 2450 50  0001 C CNN "Catégorie"
F 9 "804-9000" H 8400 2450 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8400 2450 50  0001 C CNN "Conditionnement"
F 11 "~" H 8400 2450 50  0001 C CNN "Courant"
F 12 "Résistance CMS 100kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8400 2450 50  0001 C CNN "Description"
F 13 "RS" H 8400 2450 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8400 2450 50  0001 C CNN "Height"
F 15 "RS PRO" H 8400 2450 50  0001 C CNN "Manufacturer_Name"
F 16 "804-9000" H 8400 2450 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,47" H 8400 2450 50  0001 C CNN "Prix Conditionnement"
F 18 "7,47" H 8400 2450 50  0001 C CNN "Prix Unitaire"
F 19 "804-9000" H 8400 2450 50  0001 C CNN "RS Part Number"
	1    8400 2450
	0    1    -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5DB216F9
P 5700 3000
F 0 "R7" V 5907 3000 50  0000 C CNN
F 1 "1k" V 5816 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5630 3000 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 5700 3000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 5700 3000 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 5700 3000 50  0001 C CNN "Puissance"
F 6 "~" H 5700 3000 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 5700 3000 50  0001 C CNN "Tolérence"
F 8 "Passif" H 5700 3000 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 5700 3000 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 5700 3000 50  0001 C CNN "Conditionnement"
F 11 "~" H 5700 3000 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 5700 3000 50  0001 C CNN "Description"
F 13 "RS" H 5700 3000 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 5700 3000 50  0001 C CNN "Height"
F 15 "RS PRO" H 5700 3000 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 5700 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 5700 3000 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 5700 3000 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 5700 3000 50  0001 C CNN "RS Part Number"
	1    5700 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C10
U 1 1 5DB21DA9
P 5950 3300
F 0 "C10" H 6200 3250 50  0000 R CNN
F 1 "6.8n" H 6300 3350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5988 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/135b/0900766b8135b0f3.pdf" H 5950 3300 50  0001 C CNN
F 4 "Passif" H 5950 3300 50  0001 C CNN "Catégorie"
F 5 "839-2956" H 5950 3300 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 5950 3300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 5950 3300 50  0001 C CNN "Description"
F 8 "RS" H 5950 3300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 5950 3300 50  0001 C CNN "Height"
F 10 "Wurth Elektronik" H 5950 3300 50  0001 C CNN "Manufacturer_Name"
F 11 "885012206088" H 5950 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "1,20" H 5950 3300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,012" H 5950 3300 50  0001 C CNN "Prix Unitaire"
F 14 "839-2956" H 5950 3300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/8392956" H 5950 3300 50  0001 C CNN "RS Price/Stock"
F 16 "50V cc" H 5950 3300 50  0001 C CNN "Tension"
F 17 "±10%" H 5950 3300 50  0001 C CNN "Tolérence"
F 18 "~" H 5950 3300 50  0001 C CNN "Puissance"
	1    5950 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 3000 5950 3000
Wire Wire Line
	5950 3000 5950 3150
Connection ~ 5950 3000
$Comp
L Device:R R6
U 1 1 5DB28211
P 5150 3000
F 0 "R6" V 5357 3000 50  0000 C CNN
F 1 "1k" V 5266 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5080 3000 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 5150 3000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 5150 3000 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 5150 3000 50  0001 C CNN "Puissance"
F 6 "~" H 5150 3000 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 5150 3000 50  0001 C CNN "Tolérence"
F 8 "Passif" H 5150 3000 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 5150 3000 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 5150 3000 50  0001 C CNN "Conditionnement"
F 11 "~" H 5150 3000 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 5150 3000 50  0001 C CNN "Description"
F 13 "RS" H 5150 3000 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 5150 3000 50  0001 C CNN "Height"
F 15 "RS PRO" H 5150 3000 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 5150 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 5150 3000 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 5150 3000 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 5150 3000 50  0001 C CNN "RS Part Number"
	1    5150 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C9
U 1 1 5DB28446
P 5450 3300
F 0 "C9" H 5700 3250 50  0000 R CNN
F 1 "6.8n" H 5800 3350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5488 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/135b/0900766b8135b0f3.pdf" H 5450 3300 50  0001 C CNN
F 4 "Passif" H 5450 3300 50  0001 C CNN "Catégorie"
F 5 "839-2956" H 5450 3300 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 5450 3300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 5450 3300 50  0001 C CNN "Description"
F 8 "RS" H 5450 3300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 5450 3300 50  0001 C CNN "Height"
F 10 "Wurth Elektronik" H 5450 3300 50  0001 C CNN "Manufacturer_Name"
F 11 "885012206088" H 5450 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "1,20" H 5450 3300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,012" H 5450 3300 50  0001 C CNN "Prix Unitaire"
F 14 "839-2956" H 5450 3300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/8392956" H 5450 3300 50  0001 C CNN "RS Price/Stock"
F 16 "50V cc" H 5450 3300 50  0001 C CNN "Tension"
F 17 "±10%" H 5450 3300 50  0001 C CNN "Tolérence"
F 18 "~" H 5450 3300 50  0001 C CNN "Puissance"
	1    5450 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 3000 5450 3000
Wire Wire Line
	5450 3000 5450 3150
Connection ~ 5450 3000
Wire Wire Line
	5450 3000 5550 3000
Wire Notes Line
	3850 2700 3850 3550
Wire Notes Line
	3850 3550 6100 3550
Wire Notes Line
	6100 3550 6100 2700
Wire Notes Line
	6100 2700 3850 2700
Text Notes 4650 2650 0    50   ~ 0
Filtre passe-bas\n  23,405 kHz
Connection ~ 7450 3000
Wire Wire Line
	7150 3150 7450 3150
Connection ~ 7450 3150
Wire Wire Line
	6900 3400 7500 3400
Wire Wire Line
	7350 3550 7500 3550
Wire Wire Line
	5450 3450 5450 3600
Wire Wire Line
	5450 3600 5700 3600
Wire Wire Line
	5950 3600 5950 3450
Connection ~ 5700 3600
Wire Wire Line
	5700 3600 5950 3600
Connection ~ 6650 3800
Wire Wire Line
	6650 3800 6900 3800
Wire Wire Line
	5950 3000 6500 3000
Wire Wire Line
	6100 3800 6650 3800
Wire Notes Line
	6300 2700 6300 3850
Wire Notes Line
	6300 3850 7250 3850
Wire Notes Line
	7250 3850 7250 2700
Wire Notes Line
	7250 2700 6300 2700
Text Notes 6500 2650 0    50   ~ 0
Filtre passe-haut\n   72,343 Hz
$Comp
L Jumper:SolderJumper_3_Open JP1
U 1 1 5D8EED84
P 3650 3000
F 0 "JP1" V 3696 3067 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3605 3067 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3650 3000 50  0001 C CNN
F 3 "~" H 3650 3000 50  0001 C CNN
	1    3650 3000
	0    -1   -1   0   
$EndComp
Text HLabel 3300 2650 0    50   Input ~ 0
AUDIO_out_Diablo
Text HLabel 3250 3750 0    50   Input ~ 0
AUDIO_enable_Diablo
Text HLabel 1750 6000 0    50   Input ~ 0
VDD
Text HLabel 3250 3400 0    50   Input ~ 0
AUDIO_out_PSoC
Text HLabel 3250 4450 0    50   Input ~ 0
AUDIO_Enable_PSoC
Wire Wire Line
	3300 2650 3650 2650
Wire Wire Line
	3650 2650 3650 2800
Wire Wire Line
	3650 3200 3650 3400
Wire Wire Line
	3650 3400 3250 3400
$Comp
L Misc_KropoteX:SolderJumper_3_Open_Bi JP2
U 1 1 5D8FA147
P 3650 4100
F 0 "JP2" V 3696 4167 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3605 4167 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3650 4100 50  0001 C CNN
F 3 "~" H 3650 4100 50  0001 C CNN
	1    3650 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 3750 3650 3750
Wire Wire Line
	3650 3750 3650 3900
Wire Wire Line
	3650 4300 3650 4450
Wire Wire Line
	3650 4450 3250 4450
$Comp
L Device:R R5
U 1 1 5D90B392
P 4650 3000
F 0 "R5" V 4857 3000 50  0000 C CNN
F 1 "1k" V 4766 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 3000 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 4650 3000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 4650 3000 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4650 3000 50  0001 C CNN "Puissance"
F 6 "~" H 4650 3000 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4650 3000 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4650 3000 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 4650 3000 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4650 3000 50  0001 C CNN "Conditionnement"
F 11 "~" H 4650 3000 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4650 3000 50  0001 C CNN "Description"
F 13 "RS" H 4650 3000 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4650 3000 50  0001 C CNN "Height"
F 15 "RS PRO" H 4650 3000 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 4650 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 4650 3000 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 4650 3000 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 4650 3000 50  0001 C CNN "RS Part Number"
	1    4650 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C8
U 1 1 5D90B3A7
P 4900 3300
F 0 "C8" H 5150 3250 50  0000 R CNN
F 1 "6.8n" H 5250 3350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4938 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/135b/0900766b8135b0f3.pdf" H 4900 3300 50  0001 C CNN
F 4 "Passif" H 4900 3300 50  0001 C CNN "Catégorie"
F 5 "839-2956" H 4900 3300 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 4900 3300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 4900 3300 50  0001 C CNN "Description"
F 8 "RS" H 4900 3300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 4900 3300 50  0001 C CNN "Height"
F 10 "Wurth Elektronik" H 4900 3300 50  0001 C CNN "Manufacturer_Name"
F 11 "885012206088" H 4900 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "1,20" H 4900 3300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,012" H 4900 3300 50  0001 C CNN "Prix Unitaire"
F 14 "839-2956" H 4900 3300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/8392956" H 4900 3300 50  0001 C CNN "RS Price/Stock"
F 16 "50V cc" H 4900 3300 50  0001 C CNN "Tension"
F 17 "±10%" H 4900 3300 50  0001 C CNN "Tolérence"
F 18 "~" H 4900 3300 50  0001 C CNN "Puissance"
	1    4900 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 3000 4900 3000
Wire Wire Line
	4900 3000 4900 3150
Connection ~ 4900 3000
$Comp
L Device:R R4
U 1 1 5D90B3C0
P 4100 3000
F 0 "R4" V 4307 3000 50  0000 C CNN
F 1 "1k" V 4216 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4030 3000 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 4100 3000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 4100 3000 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4100 3000 50  0001 C CNN "Puissance"
F 6 "~" H 4100 3000 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4100 3000 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4100 3000 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 4100 3000 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4100 3000 50  0001 C CNN "Conditionnement"
F 11 "~" H 4100 3000 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4100 3000 50  0001 C CNN "Description"
F 13 "RS" H 4100 3000 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4100 3000 50  0001 C CNN "Height"
F 15 "RS PRO" H 4100 3000 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 4100 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 4100 3000 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 4100 3000 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 4100 3000 50  0001 C CNN "RS Part Number"
	1    4100 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 5D90B3D5
P 4400 3300
F 0 "C7" H 4650 3250 50  0000 R CNN
F 1 "6.8n" H 4750 3350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4438 3150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/135b/0900766b8135b0f3.pdf" H 4400 3300 50  0001 C CNN
F 4 "Passif" H 4400 3300 50  0001 C CNN "Catégorie"
F 5 "839-2956" H 4400 3300 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 4400 3300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 4400 3300 50  0001 C CNN "Description"
F 8 "RS" H 4400 3300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 4400 3300 50  0001 C CNN "Height"
F 10 "Wurth Elektronik" H 4400 3300 50  0001 C CNN "Manufacturer_Name"
F 11 "885012206088" H 4400 3300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "1,20" H 4400 3300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,012" H 4400 3300 50  0001 C CNN "Prix Unitaire"
F 14 "839-2956" H 4400 3300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/8392956" H 4400 3300 50  0001 C CNN "RS Price/Stock"
F 16 "50V cc" H 4400 3300 50  0001 C CNN "Tension"
F 17 "±10%" H 4400 3300 50  0001 C CNN "Tolérence"
F 18 "~" H 4400 3300 50  0001 C CNN "Puissance"
	1    4400 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 3000 4400 3000
Wire Wire Line
	4400 3000 4400 3150
Connection ~ 4400 3000
Wire Wire Line
	4400 3000 4500 3000
Wire Wire Line
	4400 3450 4400 3600
Wire Wire Line
	4900 3600 4900 3450
Wire Wire Line
	4900 3000 5000 3000
Wire Wire Line
	3800 3000 3950 3000
Wire Wire Line
	4400 3600 4650 3600
Wire Wire Line
	4650 3600 4650 3800
Wire Wire Line
	4650 3800 5700 3800
Connection ~ 4650 3600
Wire Wire Line
	4650 3600 4900 3600
Wire Wire Line
	5700 3600 5700 3800
Connection ~ 5700 3800
Wire Wire Line
	5700 3800 6100 3800
Wire Wire Line
	3800 4100 7350 4100
Wire Wire Line
	7350 3550 7350 4100
$Comp
L Device:C C5
U 1 1 5D9920C7
P 1900 6350
F 0 "C5" H 2015 6396 50  0000 L CNN
F 1 "1µ" H 2015 6305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.4" H 1938 6200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13bc/0900766b813bccee.pdf" H 1900 6350 50  0001 C CNN
F 4 "Passif" H 1900 6350 50  0001 C CNN "Catégorie"
F 5 "844-0774" H 1900 6350 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 1900 6350 50  0001 C CNN "Conditionnement"
F 7 "Condensateur électrolytique aluminium Nichicon, 1μF, 50V c.c., série WX" H 1900 6350 50  0001 C CNN "Description"
F 8 "RS" H 1900 6350 50  0001 C CNN "Fournisseur"
F 9 "3 (Dia.) x 5.4mm" H 1900 6350 50  0001 C CNN "Height"
F 10 "Nichicon" H 1900 6350 50  0001 C CNN "Manufacturer_Name"
F 11 "UWX1H010MCL2GB" H 1900 6350 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "4,85" H 1900 6350 50  0001 C CNN "Prix Conditionnement"
F 13 "0,097" H 1900 6350 50  0001 C CNN "Prix Unitaire"
F 14 "~" H 1900 6350 50  0001 C CNN "Puissance"
F 15 "844-0774" H 1900 6350 50  0001 C CNN "RS Part Number"
F 16 "https://fr.rs-online.com/web/products/8440774/" H 1900 6350 50  0001 C CNN "RS Price/Stock"
F 17 "50V c.c." H 1900 6350 50  0001 C CNN "Tension"
F 18 "±20%" H 1900 6350 50  0001 C CNN "Tolérence"
	1    1900 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D9C0993
P 2400 6350
AR Path="/5D9C0993" Ref="C?"  Part="1" 
AR Path="/5D7EDDD9/5D9C0993" Ref="C6"  Part="1" 
F 0 "C6" H 2515 6396 50  0000 L CNN
F 1 "10n" H 2515 6305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2438 6200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 2400 6350 50  0001 C CNN
F 4 "Passif" H 2400 6350 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 2400 6350 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 2400 6350 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 2400 6350 50  0001 C CNN "Description"
F 8 "RS" H 2400 6350 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 2400 6350 50  0001 C CNN "Height"
F 10 "Murata" H 2400 6350 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 2400 6350 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 2400 6350 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 2400 6350 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 2400 6350 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 2400 6350 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 2400 6350 50  0001 C CNN "Tension"
F 17 "±10%" H 2400 6350 50  0001 C CNN "Tolérence"
F 18 "~" H 2400 6350 50  0001 C CNN "Puissance"
	1    2400 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6200 1900 6050
Wire Wire Line
	1900 6050 2150 6050
Wire Wire Line
	2400 6050 2400 6200
Wire Wire Line
	2400 6500 2400 6700
Wire Wire Line
	2400 6700 2150 6700
Wire Wire Line
	1900 6700 1900 6500
Wire Wire Line
	2150 6700 2150 6900
Connection ~ 2150 6700
Wire Wire Line
	2150 6700 1900 6700
Wire Wire Line
	1750 6000 2150 6000
Wire Wire Line
	2150 6000 2150 6050
Connection ~ 2150 6050
Wire Wire Line
	2150 6050 2400 6050
Wire Wire Line
	2150 6000 2550 6000
Connection ~ 2150 6000
$Comp
L power:GND #PWR06
U 1 1 5D9CC38F
P 2150 6900
F 0 "#PWR06" H 2150 6650 50  0001 C CNN
F 1 "GND" H 2155 6727 50  0000 C CNN
F 2 "" H 2150 6900 50  0001 C CNN
F 3 "" H 2150 6900 50  0001 C CNN
	1    2150 6900
	1    0    0    -1  
$EndComp
Text Label 2550 6000 0    50   ~ 0
VDD
Text Label 8550 2850 0    50   ~ 0
VDD
Wire Wire Line
	8450 3000 8750 3000
Wire Wire Line
	8800 3100 8750 3100
Wire Wire Line
	8550 3100 8550 3150
Wire Wire Line
	8750 2450 8750 3000
Wire Wire Line
	8550 2450 8750 2450
Connection ~ 8750 3000
Wire Wire Line
	8750 3000 8800 3000
Wire Wire Line
	8750 3900 8750 3100
Connection ~ 8750 3100
Wire Wire Line
	8750 3100 8550 3100
Wire Wire Line
	7150 3000 7450 3000
Wire Wire Line
	6850 3000 6800 3000
$Comp
L Device:R R10
U 1 1 5DA3DF44
P 7800 2450
F 0 "R10" V 8007 2450 50  0000 C CNN
F 1 "1k" V 7916 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7730 2450 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 7800 2450 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 7800 2450 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 7800 2450 50  0001 C CNN "Puissance"
F 6 "~" H 7800 2450 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 7800 2450 50  0001 C CNN "Tolérence"
F 8 "Passif" H 7800 2450 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 7800 2450 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 7800 2450 50  0001 C CNN "Conditionnement"
F 11 "~" H 7800 2450 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 7800 2450 50  0001 C CNN "Description"
F 13 "RS" H 7800 2450 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 7800 2450 50  0001 C CNN "Height"
F 15 "RS PRO" H 7800 2450 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 7800 2450 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 7800 2450 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 7800 2450 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 7800 2450 50  0001 C CNN "RS Part Number"
	1    7800 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7650 2450 7450 2450
Wire Wire Line
	7950 2450 8250 2450
$Comp
L Device:R R13
U 1 1 5DA781E7
P 8000 3900
F 0 "R13" V 7800 3900 50  0000 C CNN
F 1 "10k" V 7900 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7930 3900 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 8000 3900 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 8000 3900 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8000 3900 50  0001 C CNN "Puissance"
F 6 "~" H 8000 3900 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8000 3900 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8000 3900 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 8000 3900 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8000 3900 50  0001 C CNN "Conditionnement"
F 11 "~" H 8000 3900 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8000 3900 50  0001 C CNN "Description"
F 13 "RS" H 8000 3900 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8000 3900 50  0001 C CNN "Height"
F 15 "RS PRO" H 8000 3900 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 8000 3900 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 8000 3900 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 8000 3900 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 8000 3900 50  0001 C CNN "RS Part Number"
	1    8000 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	8150 3900 8750 3900
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5D992FDA
P 9000 3000
F 0 "J2" H 9080 2992 50  0000 L CNN
F 1 "Conn_01x02" H 9080 2901 50  0000 L CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal" H 9000 3000 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/144c/0900766b8144c299.pdf" H 9000 3000 50  0001 C CNN
F 4 "Connecteur" H 9000 3000 50  0001 C CNN "Catégorie"
F 5 " 896-7500" H 9000 3000 50  0001 C CNN "Code Fournisseur"
F 6 "10" H 9000 3000 50  0001 C CNN "Conditionnement"
F 7 "2,5 A" H 9000 3000 50  0001 C CNN "Courant"
F 8 "Embase pour CI Molex, Pico-SPOX, 2 pôles , 1.5mm 1 rangée, 2.5A, Verticale" H 9000 3000 50  0001 C CNN "Description"
F 9 "RS" H 9000 3000 50  0001 C CNN "Fournisseur"
F 10 "1,5mm" H 9000 3000 50  0001 C CNN "Height"
F 11 "Molex" H 9000 3000 50  0001 C CNN "Manufacturer_Name"
F 12 "87437-0243" H 9000 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "4,51" H 9000 3000 50  0001 C CNN "Prix Conditionnement"
F 14 "0,451" H 9000 3000 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 9000 3000 50  0001 C CNN "Puissance"
F 16 "896-7500" H 9000 3000 50  0001 C CNN "RS Part Number"
F 17 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/8967500/" H 9000 3000 50  0001 C CNN "RS Price/Stock"
F 18 "250 V(cc/ca)" H 9000 3000 50  0001 C CNN "Tension"
F 19 "~" H 9000 3000 50  0001 C CNN "Tolérence"
	1    9000 3000
	1    0    0    -1  
$EndComp
Text Notes 2750 1350 0    200  ~ 40
Amplificteur Audio 1,25 W de Classe AB
Text Notes 3850 1750 0    100  ~ 20
Avec système de  sélection d'entrée et d'activation
Wire Notes Line width 40 rgb(0, 185, 6)
	9550 800  2050 800 
Wire Notes Line width 40 rgb(0, 185, 6)
	9550 2050 2050 2050
Wire Notes Line width 40 rgb(0, 185, 6)
	2050 800  2050 2050
Wire Notes Line width 40 rgb(0, 185, 6)
	9550 800  9550 2050
Text Notes 3950 6000 0    100  ~ 20
Le SolderJumper1 permet de sélectionner l'entrée\nde l'amplificateur (Diablo ou PSoC). Cette entrée\nétant filtrée en premier lieux par un passe-bas de 23kHz,\npuis par un passe-haut de 72 Hz\nce qui améliore nettement la qualité du son en sortie.\n\nLe SolderJumper2 permet de sélectionner le périphérique\nqui activera l'amplificateur audio (Diablo ou PSoC).
Wire Notes Line
	1350 5750 2900 5750
Wire Notes Line
	2900 5750 2900 7300
Wire Notes Line
	2900 7300 1350 7300
Wire Notes Line
	1350 7300 1350 5750
Text Notes 1550 5650 0    50   ~ 0
Découplage de l'alimentation \n   de l'amplificateur audio
$EndSCHEMATC
