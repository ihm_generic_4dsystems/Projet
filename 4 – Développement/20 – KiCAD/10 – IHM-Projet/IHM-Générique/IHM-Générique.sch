EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-30"
Rev "1.0β-4.3"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
$Sheet
S 8600 1800 1000 1500
U 5D7EDDD9
F0 "Ampli-Audio" 50
F1 "file5D7EDDD8.sch" 50
F2 "AUDIO_out_PSoC" I L 8600 3100 50 
F3 "AUDIO_out_Diablo" I L 8600 2750 50 
F4 "AUDIO_Enable_PSoC" I L 8600 3200 50 
F5 "AUDIO_enable_Diablo" I L 8600 2850 50 
F6 "VDD" I L 8600 2300 50 
$EndSheet
Wire Wire Line
	6100 2000 6800 2000
Wire Wire Line
	7100 2250 6100 2250
Wire Wire Line
	6100 2150 7100 2150
Wire Wire Line
	7000 1900 7100 1900
Wire Wire Line
	6900 1700 6900 2400
Wire Wire Line
	6100 1900 6700 1900
Wire Wire Line
	8100 2750 8600 2750
Wire Wire Line
	8600 2850 8100 2850
Wire Wire Line
	6100 2500 7050 2500
Wire Wire Line
	7050 2500 7050 3400
Wire Wire Line
	7050 3400 8200 3400
Wire Wire Line
	8200 3400 8200 3100
Wire Wire Line
	8200 3100 8600 3100
Wire Wire Line
	6100 2600 6950 2600
Wire Wire Line
	6950 2600 6950 3500
Wire Wire Line
	6950 3500 8300 3500
Wire Wire Line
	8300 3500 8300 3200
Wire Wire Line
	8300 3200 8600 3200
Wire Wire Line
	8200 2300 8600 2300
Wire Wire Line
	7000 1400 7000 1900
$Sheet
S 7100 1800 1000 1500
U 5D7EDD13
F0 "4DSystem" 50
F1 "file5D7EDD12.sch" 50
F2 "AUDIO-OUT" O R 8100 2750 50 
F3 "AUDIONB" O R 8100 2850 50 
F4 "RESET" I L 7100 1900 50 
F5 "RX0" I L 7100 2150 50 
F6 "TX0" O L 7100 2250 50 
F7 "+5V" I L 7100 2400 50 
$EndSheet
Wire Wire Line
	6800 1500 6800 2000
Wire Wire Line
	6700 1900 6700 1600
Wire Wire Line
	6900 1700 8200 1700
Wire Wire Line
	8200 1700 8200 2300
Connection ~ 6900 1700
Wire Wire Line
	6900 2400 7100 2400
$Sheet
S 4600 1800 1500 1500
U 5D7EDCB3
F0 "PSoC" 50
F1 "file5D7EDCB2.sch" 50
F2 "VDD" I L 4600 1900 50 
F3 "I2C0_SCL" O L 4600 2150 50 
F4 "I2C0_SDA" B L 4600 2050 50 
F5 "UART_Rx" I R 6100 2250 50 
F6 "UART_Tx" O R 6100 2150 50 
F7 "Prog_SWDIO" B L 4600 3000 50 
F8 "Prog_SWDCLK" O L 4600 2900 50 
F9 "I2C1_SCL" O L 4600 2400 50 
F10 "I2C1_SDA" B L 4600 2300 50 
F11 "P_TDI" O L 4600 3250 50 
F12 "P_SWO" I L 4600 3150 50 
F13 "XRES" I R 6100 2000 50 
F14 "AUDIO_out_PSoC" O R 6100 2500 50 
F15 "Reset_PSoC" O R 6100 1900 50 
F16 "AUDIO_Enable_PSoC" O R 6100 2600 50 
F17 "I2Cext2" O L 4600 2550 50 
F18 "I2Cext1" O L 4600 2650 50 
F19 "I2Cext0" O L 4600 2750 50 
F20 "ROTENSW" I R 6100 3000 50 
F21 "ROTENA" I R 6100 2800 50 
F22 "NP_DATA" O R 6100 3250 50 
F23 "ROTENB" I R 6100 2900 50 
$EndSheet
Wire Wire Line
	6250 2900 6100 2900
Wire Wire Line
	6100 3000 6250 3000
Text Label 6250 2800 0    50   ~ 0
ROTENA
Text Label 6250 2900 0    50   ~ 0
ROTENB
Text Label 6250 3000 0    50   ~ 0
ROTENSW
Wire Wire Line
	6100 2800 6250 2800
Wire Wire Line
	6100 3250 6400 3250
Text Label 8200 1700 0    50   ~ 0
VDD
Wire Wire Line
	4550 5250 3100 5250
Wire Wire Line
	4550 3250 4550 5250
Wire Wire Line
	4600 3250 4550 3250
Wire Wire Line
	4450 3150 4600 3150
Wire Wire Line
	4450 5150 4450 3150
Wire Wire Line
	3100 5150 4450 5150
Wire Wire Line
	4350 5000 3100 5000
Wire Wire Line
	4350 3000 4350 5000
Wire Wire Line
	4600 3000 4350 3000
Wire Wire Line
	4250 2900 4600 2900
Wire Wire Line
	4250 4900 4250 2900
Wire Wire Line
	3100 4900 4250 4900
Wire Wire Line
	4150 4750 3100 4750
Wire Wire Line
	4150 2750 4150 4750
Wire Wire Line
	4600 2750 4150 2750
Wire Wire Line
	4050 2650 4600 2650
Wire Wire Line
	4050 4650 4050 2650
Wire Wire Line
	3100 4650 4050 4650
Wire Wire Line
	3950 2550 4600 2550
Wire Wire Line
	3950 4550 3950 2550
Wire Wire Line
	3100 4550 3950 4550
Wire Wire Line
	3850 2400 4600 2400
Wire Wire Line
	3850 4400 3850 2400
Wire Wire Line
	3100 4400 3850 4400
Wire Wire Line
	3750 2300 4600 2300
Wire Wire Line
	3750 4300 3750 2300
Wire Wire Line
	3100 4300 3750 4300
Wire Wire Line
	3650 2150 4600 2150
Wire Wire Line
	3650 4150 3650 2150
Wire Wire Line
	3100 4150 3650 4150
Wire Wire Line
	3550 2050 4600 2050
Wire Wire Line
	3550 4050 3550 2050
Wire Wire Line
	3100 4050 3550 4050
Connection ~ 3450 2200
Wire Wire Line
	3450 3900 3450 2200
Wire Wire Line
	3100 3900 3450 3900
$Sheet
S 2100 3800 1000 1500
U 5D7EDD70
F0 "I²C" 50
F1 "file5D7EDD6F.sch" 50
F2 "VDD" I R 3100 3900 50 
F3 "I2C1_SDA" B R 3100 4300 50 
F4 "I2C1_SCL" I R 3100 4400 50 
F5 "I2Cext1" I R 3100 4650 50 
F6 "I2Cext0" I R 3100 4750 50 
F7 "I2Cext2" I R 3100 4550 50 
F8 "I2C0_SDA" B R 3100 4050 50 
F9 "I2C0_SCL" I R 3100 4150 50 
F10 "Prog_SWDCLK" I R 3100 4900 50 
F11 "Prog_SWDIO" B R 3100 5000 50 
F12 "P_SWO" O R 3100 5150 50 
F13 "P_TDI" I R 3100 5250 50 
$EndSheet
Wire Wire Line
	3450 1900 3450 2200
Connection ~ 3450 1900
Wire Wire Line
	3450 1900 4600 1900
Wire Wire Line
	3100 2200 3450 2200
Wire Wire Line
	3350 2100 3100 2100
Wire Wire Line
	3350 1600 3350 2100
Wire Wire Line
	6700 1600 3350 1600
Wire Wire Line
	3250 1500 6800 1500
Wire Wire Line
	3250 2000 3250 1500
Wire Wire Line
	3100 2000 3250 2000
Wire Wire Line
	3150 1400 7000 1400
Wire Wire Line
	3100 1900 3150 1900
Wire Wire Line
	3450 1700 3450 1900
Wire Wire Line
	3450 1700 6900 1700
Wire Notes Line
	5000 6050 5000 5200
Wire Notes Line
	7300 6050 5000 6050
Wire Notes Line
	7300 5200 7300 6050
Wire Notes Line
	5000 5200 7300 5200
Text Notes 6650 5100 2    50   ~ 10
Trous de fixation du PCB
$Comp
L power:GND #PWR01
U 1 1 614C846C
P 6150 5800
F 0 "#PWR01" H 6150 5550 50  0001 C CNN
F 1 "GND" H 6155 5627 50  0000 C CNN
F 2 "" H 6150 5800 50  0001 C CNN
F 3 "" H 6150 5800 50  0001 C CNN
	1    6150 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 5750 5900 5750
Connection ~ 6150 5750
Wire Wire Line
	6150 5750 6150 5800
Wire Wire Line
	5900 5750 5900 5800
Connection ~ 5900 5750
Wire Wire Line
	6400 5750 6400 5450
Connection ~ 6400 5750
Wire Wire Line
	6400 5750 6150 5750
Wire Wire Line
	6400 5800 6400 5750
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 614A1B2C
P 5900 5900
F 0 "H2" H 6100 5850 50  0000 R CNN
F 1 "MountingHole_Pad" H 6700 5950 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5900 5900 50  0001 C CNN
F 3 "~" H 5900 5900 50  0001 C CNN
	1    5900 5900
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 614A0B9B
P 5900 5350
F 0 "H1" H 6100 5400 50  0000 R CNN
F 1 "MountingHole_Pad" H 6650 5300 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 5900 5350 50  0001 C CNN
F 3 "~" H 5900 5350 50  0001 C CNN
	1    5900 5350
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 614A05D1
P 6400 5900
F 0 "H4" H 6300 5857 50  0000 R CNN
F 1 "MountingHole_Pad" H 6300 5948 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6400 5900 50  0001 C CNN
F 3 "~" H 6400 5900 50  0001 C CNN
	1    6400 5900
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 6149F52E
P 6400 5350
F 0 "H3" H 6500 5399 50  0000 L CNN
F 1 "MountingHole_Pad" H 6500 5308 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6400 5350 50  0001 C CNN
F 3 "~" H 6400 5350 50  0001 C CNN
	1    6400 5350
	1    0    0    -1  
$EndComp
$Sheet
S 2100 1800 1000 1500
U 5D7EDC34
F0 "Alimentation" 50
F1 "file5D7EDC33.sch" 50
F2 "VDD" O R 3100 2200 50 
F3 "RESET" O R 3100 1900 50 
F4 "RESET_PSoC" I R 3100 2100 50 
F5 "XRES" O R 3100 2000 50 
$EndSheet
Wire Wire Line
	3150 1400 3150 1900
Wire Wire Line
	6150 5750 6150 5700
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5D8DCB9D
P 6150 5600
F 0 "H6" H 6050 5557 50  0000 R CNN
F 1 "MountingHole_Pad" H 6050 5648 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6150 5600 50  0001 C CNN
F 3 "~" H 6150 5600 50  0001 C CNN
	1    6150 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 5450 5900 5750
Wire Wire Line
	10550 4450 10550 4800
Wire Wire Line
	10550 4800 10850 4800
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5D8C755D
P 10550 4250
AR Path="/5D7EDDD9/5D8C755D" Ref="J?"  Part="1" 
AR Path="/5D8C755D" Ref="J19"  Part="1" 
F 0 "J19" V 10650 4150 50  0000 L CNN
F 1 "Conn_ROTENSW" H 10050 4400 50  0000 L CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal" H 10550 4250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/144c/0900766b8144c299.pdf" H 10550 4250 50  0001 C CNN
F 4 "Connecteur" H 10550 4250 50  0001 C CNN "Catégorie"
F 5 " 896-7500" H 10550 4250 50  0001 C CNN "Code Fournisseur"
F 6 "10" H 10550 4250 50  0001 C CNN "Conditionnement"
F 7 "2,5 A" H 10550 4250 50  0001 C CNN "Courant"
F 8 "Embase pour CI Molex, Pico-SPOX, 2 pôles , 1.5mm 1 rangée, 2.5A, Verticale" H 10550 4250 50  0001 C CNN "Description"
F 9 "RS" H 10550 4250 50  0001 C CNN "Fournisseur"
F 10 "1,5mm" H 10550 4250 50  0001 C CNN "Height"
F 11 "Molex" H 10550 4250 50  0001 C CNN "Manufacturer_Name"
F 12 "87437-0243" H 10550 4250 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "4,51" H 10550 4250 50  0001 C CNN "Prix Conditionnement"
F 14 "0,451" H 10550 4250 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 10550 4250 50  0001 C CNN "Puissance"
F 16 "896-7500" H 10550 4250 50  0001 C CNN "RS Part Number"
F 17 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/8967500/" H 10550 4250 50  0001 C CNN "RS Price/Stock"
F 18 "250 V(cc/ca)" H 10550 4250 50  0001 C CNN "Tension"
F 19 "~" H 10550 4250 50  0001 C CNN "Tolérence"
	1    10550 4250
	0    1    -1   0   
$EndComp
Wire Wire Line
	9100 4800 9400 4800
Wire Wire Line
	9100 5000 9100 4800
Wire Wire Line
	9000 4700 9350 4700
Wire Wire Line
	9000 5000 9000 4700
Wire Wire Line
	8900 4600 8000 4600
Wire Wire Line
	8900 5000 8900 4600
$Comp
L Connector_Generic:Conn_01x03 J18
U 1 1 5D99A5AC
P 9000 5200
F 0 "J18" H 8918 5517 50  0000 C CNN
F 1 "Conn_ROTENDT" H 8918 5426 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0343_1x03_1MP_P1.50mm_Horizontal" H 9000 5200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13c4/0900766b813c4ad0.pdf" H 9000 5200 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 6706502" H 9000 5200 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 9000 5200 50  0001 C CNN "Catégorie"
F 6 "670-6502" H 9000 5200 50  0001 C CNN "Code Fournisseur"
F 7 "10" H 9000 5200 50  0001 C CNN "Conditionnement"
F 8 "3 A" H 9000 5200 50  0001 C CNN "Courant"
F 9 "Embase pour CI Molex, Pico-SPOX, 3 pôles , 1.5mm 1 rangée, 3A, Angle droit" H 9000 5200 50  0001 C CNN "Description"
F 10 "RS" H 9000 5200 50  0001 C CNN "Fournisseur"
F 11 "1,5mm" H 9000 5200 50  0001 C CNN "Height"
F 12 "Molex" H 9000 5200 50  0001 C CNN "Manufacturer_Name"
F 13 "87438-0343" H 9000 5200 50  0001 C CNN "Manufacturer_Part_Number"
F 14 "5,49" H 9000 5200 50  0001 C CNN "Prix Conditionnement"
F 15 "0,549" H 9000 5200 50  0001 C CNN "Prix Unitaire"
F 16 " 670-6502" H 9000 5200 50  0001 C CNN "RS Part Number"
F 17 "350 V(cc/ca)" H 9000 5200 50  0001 C CNN "Tension"
F 18 "~" H 9000 5200 50  0001 C CNN "Tolérence"
F 19 "~" H 9000 5200 50  0001 C CNN "Puissance"
	1    9000 5200
	0    -1   1    0   
$EndComp
Wire Wire Line
	8000 4600 8000 4950
Text Notes 9550 3750 2    50   ~ 10
Encodeur Rotatif
Wire Notes Line
	7450 3800 11150 3800
Wire Notes Line
	7450 6050 7450 3800
Wire Notes Line
	11150 6050 7450 6050
Wire Notes Line
	11150 3800 11150 6050
Wire Wire Line
	9650 4100 10850 4100
Wire Wire Line
	9650 4000 9850 4000
Wire Wire Line
	9650 4100 9650 4000
Connection ~ 9650 4100
Wire Wire Line
	8400 4100 9650 4100
Text Label 9850 4000 0    50   ~ 0
VDD
Wire Wire Line
	7800 4950 8000 4950
Connection ~ 8000 4950
Wire Wire Line
	8000 4950 8000 5150
Wire Wire Line
	7800 5100 8400 5100
Connection ~ 8400 5100
Wire Wire Line
	8400 5150 8400 5100
Text Label 10600 5100 2    50   ~ 0
ROTENSW
Text Label 7800 5100 2    50   ~ 0
ROTENB
Text Label 7800 4950 2    50   ~ 0
ROTENA
Wire Wire Line
	10850 4800 10850 5100
Wire Wire Line
	10850 5100 10850 5150
Connection ~ 10850 5100
Wire Wire Line
	10600 5100 10850 5100
Wire Wire Line
	10850 4200 10850 4100
Wire Wire Line
	10450 4450 10200 4450
Connection ~ 10200 4450
Wire Wire Line
	10200 4450 10200 4500
$Comp
L power:GND #PWR04
U 1 1 5E46C4E5
P 10200 4500
F 0 "#PWR04" H 10200 4250 50  0001 C CNN
F 1 "GND" H 10205 4327 50  0000 C CNN
F 2 "" H 10200 4500 50  0001 C CNN
F 3 "" H 10200 4500 50  0001 C CNN
	1    10200 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 4450 10200 4450
Wire Wire Line
	9350 4450 10050 4450
Connection ~ 10050 4450
Wire Wire Line
	10050 4600 10050 4450
Wire Wire Line
	10000 4600 10050 4600
Connection ~ 10550 4800
Wire Wire Line
	10000 4800 10550 4800
Wire Wire Line
	10850 5450 10850 5500
Connection ~ 10850 4800
Wire Wire Line
	10850 4500 10850 4800
Wire Wire Line
	9350 4700 9350 4450
$Comp
L power:GND #PWR05
U 1 1 5E44C74F
P 10850 5500
F 0 "#PWR05" H 10850 5250 50  0001 C CNN
F 1 "GND" H 10855 5327 50  0000 C CNN
F 2 "" H 10850 5500 50  0001 C CNN
F 3 "" H 10850 5500 50  0001 C CNN
	1    10850 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E44BF4D
P 10850 5300
F 0 "C4" H 10965 5346 50  0000 L CNN
F 1 "10n" H 10965 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10888 5150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 10850 5300 50  0001 C CNN
F 4 "Passif" H 10850 5300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 10850 5300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 10850 5300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 10850 5300 50  0001 C CNN "Description"
F 8 "RS" H 10850 5300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 10850 5300 50  0001 C CNN "Height"
F 10 "Murata" H 10850 5300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 10850 5300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 10850 5300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 10850 5300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 10850 5300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 10850 5300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 10850 5300 50  0001 C CNN "Tension"
F 17 "±10%" H 10850 5300 50  0001 C CNN "Tolérence"
F 18 "~" H 10850 5300 50  0001 C CNN "Puissance"
	1    10850 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E44B295
P 10850 4350
F 0 "R3" H 10920 4396 50  0000 L CNN
F 1 "10k" H 10920 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10780 4350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 10850 4350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 10850 4350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 10850 4350 50  0001 C CNN "Puissance"
F 6 "~" H 10850 4350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 10850 4350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 10850 4350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 10850 4350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 10850 4350 50  0001 C CNN "Conditionnement"
F 11 "~" H 10850 4350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 10850 4350 50  0001 C CNN "Description"
F 13 "RS" H 10850 4350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 10850 4350 50  0001 C CNN "Height"
F 15 "RS PRO" H 10850 4350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 10850 4350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 10850 4350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 10850 4350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 10850 4350 50  0001 C CNN "RS Part Number"
	1    10850 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E41469B
P 8200 5500
F 0 "#PWR03" H 8200 5250 50  0001 C CNN
F 1 "GND" H 8205 5327 50  0000 C CNN
F 2 "" H 8200 5500 50  0001 C CNN
F 3 "" H 8200 5500 50  0001 C CNN
	1    8200 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E414B99
P 8000 5300
F 0 "C2" H 8115 5346 50  0000 L CNN
F 1 "10n" H 8115 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8038 5150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 8000 5300 50  0001 C CNN
F 4 "Passif" H 8000 5300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 8000 5300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 8000 5300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 8000 5300 50  0001 C CNN "Description"
F 8 "RS" H 8000 5300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 8000 5300 50  0001 C CNN "Height"
F 10 "Murata" H 8000 5300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 8000 5300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 8000 5300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 8000 5300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 8000 5300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 8000 5300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 8000 5300 50  0001 C CNN "Tension"
F 17 "±10%" H 8000 5300 50  0001 C CNN "Tolérence"
F 18 "~" H 8000 5300 50  0001 C CNN "Puissance"
	1    8000 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E414E35
P 8400 5300
F 0 "C3" H 8515 5346 50  0000 L CNN
F 1 "10n" H 8515 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8438 5150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 8400 5300 50  0001 C CNN
F 4 "Passif" H 8400 5300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 8400 5300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 8400 5300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 8400 5300 50  0001 C CNN "Description"
F 8 "RS" H 8400 5300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 8400 5300 50  0001 C CNN "Height"
F 10 "Murata" H 8400 5300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 8400 5300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 8400 5300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 8400 5300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 8400 5300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 8400 5300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 8400 5300 50  0001 C CNN "Tension"
F 17 "±10%" H 8400 5300 50  0001 C CNN "Tolérence"
F 18 "~" H 8400 5300 50  0001 C CNN "Puissance"
	1    8400 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5450 8000 5500
Wire Wire Line
	8000 5500 8200 5500
Wire Wire Line
	8400 5500 8400 5450
Connection ~ 8200 5500
Wire Wire Line
	8200 5500 8400 5500
Wire Wire Line
	8400 4100 8400 4200
Connection ~ 8400 4100
Wire Wire Line
	8000 4100 8400 4100
Wire Wire Line
	8000 4200 8000 4100
Connection ~ 9100 4800
Wire Wire Line
	8400 4800 8400 5100
Wire Wire Line
	8400 4800 8400 4500
Connection ~ 8400 4800
Wire Wire Line
	8400 4800 9100 4800
Connection ~ 9350 4700
Wire Wire Line
	9400 4700 9350 4700
Connection ~ 8900 4600
Wire Wire Line
	9400 4600 8900 4600
Connection ~ 8000 4600
Wire Wire Line
	8000 4500 8000 4600
$Comp
L Device:R R2
U 1 1 5E413ADF
P 8400 4350
F 0 "R2" H 8470 4396 50  0000 L CNN
F 1 "10k" H 8470 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8330 4350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 8400 4350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 8400 4350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8400 4350 50  0001 C CNN "Puissance"
F 6 "~" H 8400 4350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8400 4350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8400 4350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 8400 4350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8400 4350 50  0001 C CNN "Conditionnement"
F 11 "~" H 8400 4350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8400 4350 50  0001 C CNN "Description"
F 13 "RS" H 8400 4350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8400 4350 50  0001 C CNN "Height"
F 15 "RS PRO" H 8400 4350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 8400 4350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 8400 4350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 8400 4350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 8400 4350 50  0001 C CNN "RS Part Number"
	1    8400 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E413915
P 8000 4350
F 0 "R1" H 8070 4396 50  0000 L CNN
F 1 "10k" H 8070 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7930 4350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 8000 4350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 8000 4350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8000 4350 50  0001 C CNN "Puissance"
F 6 "~" H 8000 4350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8000 4350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8000 4350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 8000 4350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8000 4350 50  0001 C CNN "Conditionnement"
F 11 "~" H 8000 4350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8000 4350 50  0001 C CNN "Description"
F 13 "RS" H 8000 4350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8000 4350 50  0001 C CNN "Height"
F 15 "RS PRO" H 8000 4350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 8000 4350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 8000 4350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 8000 4350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 8000 4350 50  0001 C CNN "RS Part Number"
	1    8000 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 5E4117F3
P 9700 4700
F 0 "SW1" H 9700 4350 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 9700 4450 50  0000 C CNN
F 2 "Misc_KropoteX:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 9550 4860 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0f4c/0900766b80f4c3d5.pdf" H 9700 4960 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 1727621 " H 9700 4700 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 9700 4700 50  0001 C CNN "Puissance"
F 6 "~" H 9700 4700 50  0001 C CNN "Tension"
F 7 "~" H 9700 4700 50  0001 C CNN "Tolérence"
F 8 "Passif" H 9700 4700 50  0001 C CNN "Catégorie"
F 9 "172-7621" H 9700 4700 50  0001 C CNN "Code Fournisseur"
F 10 "95" H 9700 4700 50  0001 C CNN "Conditionnement"
F 11 "0,5 mA" H 9700 4700 50  0001 C CNN "Courant"
F 12 "Encodeur rotatif mécanique, , Incrémental, 24 impulsions par tour, axe de 6 mm, Traversant" H 9700 4700 50  0001 C CNN "Description"
F 13 "RS" H 9700 4700 50  0001 C CNN "Fournisseur"
F 14 "1 mm, 2.1 x 2 mm" H 9700 4700 50  0001 C CNN "Height"
F 15 "Alps Alpine" H 9700 4700 50  0001 C CNN "Manufacturer_Name"
F 16 " 172-7621" H 9700 4700 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "45,03" H 9700 4700 50  0001 C CNN "Prix Conditionnement"
F 18 "0,474" H 9700 4700 50  0001 C CNN "Prix Unitaire"
F 19 " 172-7621 " H 9700 4700 50  0001 C CNN "RS Part Number"
	1    9700 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4550 6950 4550
Wire Wire Line
	6700 4550 6700 4600
Connection ~ 6700 4550
Wire Wire Line
	6550 4550 6700 4550
Wire Wire Line
	6950 4500 6950 4550
Wire Wire Line
	6950 4100 7100 4100
Wire Wire Line
	6950 4200 6950 4100
$Comp
L Device:C C?
U 1 1 5DA11B93
P 6950 4350
AR Path="/5D7EDDD9/5DA11B93" Ref="C?"  Part="1" 
AR Path="/5DA11B93" Ref="C27"  Part="1" 
F 0 "C27" H 7065 4396 50  0000 L CNN
F 1 "1µ" H 7065 4305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.4" H 6988 4200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13bc/0900766b813bccee.pdf" H 6950 4350 50  0001 C CNN
F 4 "Passif" H 6950 4350 50  0001 C CNN "Catégorie"
F 5 "844-0774" H 6950 4350 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 6950 4350 50  0001 C CNN "Conditionnement"
F 7 "Condensateur électrolytique aluminium Nichicon, 1μF, 50V c.c., série WX" H 6950 4350 50  0001 C CNN "Description"
F 8 "RS" H 6950 4350 50  0001 C CNN "Fournisseur"
F 9 "3 (Dia.) x 5.4mm" H 6950 4350 50  0001 C CNN "Height"
F 10 "Nichicon" H 6950 4350 50  0001 C CNN "Manufacturer_Name"
F 11 "UWX1H010MCL2GB" H 6950 4350 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "4,85" H 6950 4350 50  0001 C CNN "Prix Conditionnement"
F 13 "0,097" H 6950 4350 50  0001 C CNN "Prix Unitaire"
F 14 "~" H 6950 4350 50  0001 C CNN "Puissance"
F 15 "844-0774" H 6950 4350 50  0001 C CNN "RS Part Number"
F 16 "https://fr.rs-online.com/web/products/8440774/" H 6950 4350 50  0001 C CNN "RS Price/Stock"
F 17 "50V c.c." H 6950 4350 50  0001 C CNN "Tension"
F 18 "±20%" H 6950 4350 50  0001 C CNN "Tolérence"
	1    6950 4350
	1    0    0    -1  
$EndComp
Connection ~ 6950 4100
Wire Wire Line
	6550 4100 6950 4100
Wire Wire Line
	6550 4550 6550 4500
Connection ~ 6550 4550
Wire Wire Line
	6400 4550 6550 4550
$Comp
L power:GND #PWR02
U 1 1 5E51AF7B
P 6700 4600
F 0 "#PWR02" H 6700 4350 50  0001 C CNN
F 1 "GND" H 6705 4427 50  0000 C CNN
F 2 "" H 6700 4600 50  0001 C CNN
F 3 "" H 6700 4600 50  0001 C CNN
	1    6700 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EB32E7A
P 6550 4350
AR Path="/5EB32E7A" Ref="C1"  Part="1" 
AR Path="/5D7EDC34/5EB32E7A" Ref="C?"  Part="1" 
F 0 "C1" H 6665 4396 50  0000 L CNN
F 1 "100n" H 6665 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6588 4200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 6550 4350 50  0001 C CNN
F 4 "698-3251" H 6550 4350 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 6550 4350 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 6550 4350 50  0001 C CNN "Description"
F 7 "RS" H 6550 4350 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 6550 4350 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 6550 4350 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 6550 4350 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 6550 4350 50  0001 C CNN "Tension"
F 12 "±10%" H 6550 4350 50  0001 C CNN "Tolérence"
F 13 "~" H 6550 4350 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 6550 4350 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 6550 4350 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 6550 4350 50  0001 C CNN "Height"
F 17 "AVX" H 6550 4350 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 6550 4350 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 6550 4350 50  0001 C CNN "RS Part Number"
	1    6550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4100 6550 4200
Connection ~ 6550 4100
Wire Wire Line
	6350 4100 6550 4100
Wire Wire Line
	6400 4200 6400 4550
Text Notes 6850 3750 2    50   ~ 10
Connecteur\nNeoPixel
Wire Notes Line
	5950 4850 5950 3800
Wire Notes Line
	7300 4850 5950 4850
Wire Notes Line
	7300 3800 7300 4850
Wire Notes Line
	5950 3800 7300 3800
Text Label 7100 4100 0    50   ~ 0
VDD
Wire Wire Line
	6350 4200 6400 4200
Wire Wire Line
	6400 4000 6350 4000
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5E412E4E
P 6150 4100
F 0 "J1" H 6150 4300 50  0000 C CNN
F 1 "Conn_NeoPixel" V 6250 4100 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0343_1x03_1MP_P1.50mm_Horizontal" H 6150 4100 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13c4/0900766b813c4ad0.pdf" H 6150 4100 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 6706502" H 6150 4100 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 6150 4100 50  0001 C CNN "Catégorie"
F 6 "670-6502" H 6150 4100 50  0001 C CNN "Code Fournisseur"
F 7 "10" H 6150 4100 50  0001 C CNN "Conditionnement"
F 8 "3 A" H 6150 4100 50  0001 C CNN "Courant"
F 9 "Embase pour CI Molex, Pico-SPOX, 3 pôles , 1.5mm 1 rangée, 3A, Angle droit" H 6150 4100 50  0001 C CNN "Description"
F 10 "RS" H 6150 4100 50  0001 C CNN "Fournisseur"
F 11 "1,5mm" H 6150 4100 50  0001 C CNN "Height"
F 12 "Molex" H 6150 4100 50  0001 C CNN "Manufacturer_Name"
F 13 "87438-0343" H 6150 4100 50  0001 C CNN "Manufacturer_Part_Number"
F 14 "5,49" H 6150 4100 50  0001 C CNN "Prix Conditionnement"
F 15 "0,549" H 6150 4100 50  0001 C CNN "Prix Unitaire"
F 16 " 670-6502" H 6150 4100 50  0001 C CNN "RS Part Number"
F 17 "350 V(cc/ca)" H 6150 4100 50  0001 C CNN "Tension"
F 18 "~" H 6150 4100 50  0001 C CNN "Tolérence"
F 19 "~" H 6150 4100 50  0001 C CNN "Puissance"
	1    6150 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6400 3250 6400 4000
Text Notes 2300 1100 0    200  ~ 40
IHM-Générique — Feuille Principale du Projet
Text Notes 900  7650 0    61   ~ 12
Le projet se découpe en plusieurs partie visibles sur ce schéma, notamment :\n— L'alimentation\n— La connectique du PSoC\n— La connectique du Diablo\n— L'amplificateur Audio\n— L'encodeur Rotatif et la sortie de l'anneau de NeoPixel\n— La connectique des ports d'extention des cartes filles\n— La connectique de communication\n\nLes cartes d'extension ne sont pas présentes ici mais une procedure de réalisation sera disponible dans le dossier,\nainsi que des cartes d'extension déjà existante ainsi que la liste du matériel necessaire à leur réalisation.\n\nCe projet est conçu pour être adaptable à l'immense majorité des situations, c'est pourquoi il vous faudra réaliser des \nbrasures sont des SolderJumper. Vous pouvez également vous passer sans encombre de mettre en place tous les\npériphériques disponibles sur la carte à l'exception de l'écran µLCD.\n\nIl est à noter que l'encodeur rotatif les NeoPixel sont déportables via des connecteurs dédiés (J1, J18 et J19) tout\ncomme le bouton de reset général (J17).\n\nLe projet est disponible sous CERN Open Hardware Licence v1.2 avec fort copyleft, c'est pourquoi vous êtes fortement\nincité à le modifier à votre convenance et à partager vos changements ! 
Wire Notes Line width 40 rgb(0, 185, 6)
	9500 650  2150 650 
Wire Notes Line width 40 rgb(0, 185, 6)
	9500 1250 2150 1250
Wire Notes Line width 40 rgb(0, 185, 6)
	2150 650  2150 1250
Wire Notes Line width 40 rgb(0, 185, 6)
	9500 650  9500 1250
$EndSCHEMATC
