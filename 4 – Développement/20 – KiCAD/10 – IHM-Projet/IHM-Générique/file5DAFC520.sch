EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Rotary_Encoder_Switch SW?
U 1 1 5DB20765
P 7750 3700
AR Path="/5DB20765" Ref="SW?"  Part="1" 
AR Path="/5DAFC521/5DB20765" Ref="SW1"  Part="1" 
F 0 "SW1" H 7750 3350 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 7750 3450 50  0000 C CNN
F 2 "Misc_KropoteX:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 7600 3860 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0f4c/0900766b80f4c3d5.pdf" H 7750 3960 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 1727621 " H 7750 3700 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 7750 3700 50  0001 C CNN "Puissance"
F 6 "~" H 7750 3700 50  0001 C CNN "Tension"
F 7 "~" H 7750 3700 50  0001 C CNN "Tolérence"
F 8 "Passif" H 7750 3700 50  0001 C CNN "Catégorie"
F 9 "172-7621" H 7750 3700 50  0001 C CNN "Code Fournisseur"
F 10 "95" H 7750 3700 50  0001 C CNN "Conditionnement"
F 11 "0,5 mA" H 7750 3700 50  0001 C CNN "Courant"
F 12 "Encodeur rotatif mécanique, , Incrémental, 24 impulsions par tour, axe de 6 mm, Traversant" H 7750 3700 50  0001 C CNN "Description"
F 13 "RS" H 7750 3700 50  0001 C CNN "Fournisseur"
F 14 "1 mm, 2.1 x 2 mm" H 7750 3700 50  0001 C CNN "Height"
F 15 "Alps Alpine" H 7750 3700 50  0001 C CNN "Manufacturer_Name"
F 16 " 172-7621" H 7750 3700 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "45,03" H 7750 3700 50  0001 C CNN "Prix Conditionnement"
F 18 "0,474" H 7750 3700 50  0001 C CNN "Prix Unitaire"
F 19 " 172-7621 " H 7750 3700 50  0001 C CNN "RS Part Number"
	1    7750 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DB2077B
P 6050 3350
AR Path="/5DB2077B" Ref="R?"  Part="1" 
AR Path="/5DAFC521/5DB2077B" Ref="R1"  Part="1" 
F 0 "R1" H 6120 3396 50  0000 L CNN
F 1 "10k" H 6120 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5980 3350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 6050 3350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 6050 3350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 6050 3350 50  0001 C CNN "Puissance"
F 6 "~" H 6050 3350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 6050 3350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 6050 3350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 6050 3350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 6050 3350 50  0001 C CNN "Conditionnement"
F 11 "~" H 6050 3350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 6050 3350 50  0001 C CNN "Description"
F 13 "RS" H 6050 3350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 6050 3350 50  0001 C CNN "Height"
F 15 "RS PRO" H 6050 3350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 6050 3350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 6050 3350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 6050 3350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 6050 3350 50  0001 C CNN "RS Part Number"
	1    6050 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DB20791
P 6450 3350
AR Path="/5DB20791" Ref="R?"  Part="1" 
AR Path="/5DAFC521/5DB20791" Ref="R2"  Part="1" 
F 0 "R2" H 6520 3396 50  0000 L CNN
F 1 "10k" H 6520 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 3350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 6450 3350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 6450 3350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 6450 3350 50  0001 C CNN "Puissance"
F 6 "~" H 6450 3350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 6450 3350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 6450 3350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 6450 3350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 6450 3350 50  0001 C CNN "Conditionnement"
F 11 "~" H 6450 3350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 6450 3350 50  0001 C CNN "Description"
F 13 "RS" H 6450 3350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 6450 3350 50  0001 C CNN "Height"
F 15 "RS PRO" H 6450 3350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 6450 3350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 6450 3350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 6450 3350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 6450 3350 50  0001 C CNN "RS Part Number"
	1    6450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3500 6050 3600
Wire Wire Line
	7450 3600 6950 3600
Connection ~ 6050 3600
Wire Wire Line
	7450 3700 7400 3700
Wire Wire Line
	6450 3800 7150 3800
Connection ~ 6450 3800
Wire Wire Line
	6450 3800 6450 3500
Wire Wire Line
	6050 3200 6050 3100
Wire Wire Line
	6050 3100 6450 3100
Wire Wire Line
	6450 3100 6450 3200
Wire Wire Line
	6250 4500 6450 4500
Connection ~ 6250 4500
Wire Wire Line
	6450 4500 6450 4450
Wire Wire Line
	6050 4500 6250 4500
Wire Wire Line
	6050 4450 6050 4500
$Comp
L Device:C C?
U 1 1 5DB207B5
P 6450 4300
AR Path="/5DB207B5" Ref="C?"  Part="1" 
AR Path="/5DAFC521/5DB207B5" Ref="C3"  Part="1" 
F 0 "C3" H 6565 4346 50  0000 L CNN
F 1 "10n" H 6565 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6488 4150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 6450 4300 50  0001 C CNN
F 4 "Passif" H 6450 4300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 6450 4300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 6450 4300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 6450 4300 50  0001 C CNN "Description"
F 8 "RS" H 6450 4300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 6450 4300 50  0001 C CNN "Height"
F 10 "Murata" H 6450 4300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 6450 4300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 6450 4300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 6450 4300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 6450 4300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 6450 4300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 6450 4300 50  0001 C CNN "Tension"
F 17 "±10%" H 6450 4300 50  0001 C CNN "Tolérence"
F 18 "~" H 6450 4300 50  0001 C CNN "Puissance"
	1    6450 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DB207CA
P 6050 4300
AR Path="/5DB207CA" Ref="C?"  Part="1" 
AR Path="/5DAFC521/5DB207CA" Ref="C2"  Part="1" 
F 0 "C2" H 6165 4346 50  0000 L CNN
F 1 "10n" H 6165 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6088 4150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 6050 4300 50  0001 C CNN
F 4 "Passif" H 6050 4300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 6050 4300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 6050 4300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 6050 4300 50  0001 C CNN "Description"
F 8 "RS" H 6050 4300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 6050 4300 50  0001 C CNN "Height"
F 10 "Murata" H 6050 4300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 6050 4300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 6050 4300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 6050 4300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 6050 4300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 6050 4300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 6050 4300 50  0001 C CNN "Tension"
F 17 "±10%" H 6050 4300 50  0001 C CNN "Tolérence"
F 18 "~" H 6050 4300 50  0001 C CNN "Puissance"
	1    6050 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DB207D0
P 6250 4500
AR Path="/5DB207D0" Ref="#PWR?"  Part="1" 
AR Path="/5DAFC521/5DB207D0" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 6250 4250 50  0001 C CNN
F 1 "GND" H 6255 4327 50  0000 C CNN
F 2 "" H 6250 4500 50  0001 C CNN
F 3 "" H 6250 4500 50  0001 C CNN
	1    6250 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DB207E6
P 8900 3350
AR Path="/5DB207E6" Ref="R?"  Part="1" 
AR Path="/5DAFC521/5DB207E6" Ref="R3"  Part="1" 
F 0 "R3" H 8970 3396 50  0000 L CNN
F 1 "10k" H 8970 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 3350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 8900 3350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 8900 3350 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 8900 3350 50  0001 C CNN "Puissance"
F 6 "~" H 8900 3350 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 8900 3350 50  0001 C CNN "Tolérence"
F 8 "Passif" H 8900 3350 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 8900 3350 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 8900 3350 50  0001 C CNN "Conditionnement"
F 11 "~" H 8900 3350 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 8900 3350 50  0001 C CNN "Description"
F 13 "RS" H 8900 3350 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 8900 3350 50  0001 C CNN "Height"
F 15 "RS PRO" H 8900 3350 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 8900 3350 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 8900 3350 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 8900 3350 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 8900 3350 50  0001 C CNN "RS Part Number"
	1    8900 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DB207FB
P 8900 4300
AR Path="/5DB207FB" Ref="C?"  Part="1" 
AR Path="/5DAFC521/5DB207FB" Ref="C4"  Part="1" 
F 0 "C4" H 9015 4346 50  0000 L CNN
F 1 "10n" H 9015 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8938 4150 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0ef9/0900766b80ef92d0.pdf" H 8900 4300 50  0001 C CNN
F 4 "Passif" H 8900 4300 50  0001 C CNN "Catégorie"
F 5 "723-5017" H 8900 4300 50  0001 C CNN "Code Fournisseur"
F 6 "200" H 8900 4300 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 10nF, ±10%, 100V cc, diélectrique : X7R, boitier 0603 (1608M) " H 8900 4300 50  0001 C CNN "Description"
F 8 "RS" H 8900 4300 50  0001 C CNN "Fournisseur"
F 9 "1.6 x 0.8 x 0.8mm" H 8900 4300 50  0001 C CNN "Height"
F 10 "Murata" H 8900 4300 50  0001 C CNN "Manufacturer_Name"
F 11 "GCM188R72A103KA37D" H 8900 4300 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,60 " H 8900 4300 50  0001 C CNN "Prix Conditionnement"
F 13 "0,003" H 8900 4300 50  0001 C CNN "Prix Unitaire"
F 14 " 723-5017" H 8900 4300 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/ 723-5017/" H 8900 4300 50  0001 C CNN "RS Price/Stock"
F 16 "100V cc" H 8900 4300 50  0001 C CNN "Tension"
F 17 "±10%" H 8900 4300 50  0001 C CNN "Tolérence"
F 18 "~" H 8900 4300 50  0001 C CNN "Puissance"
	1    8900 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DB20801
P 8900 4500
AR Path="/5DB20801" Ref="#PWR?"  Part="1" 
AR Path="/5DAFC521/5DB20801" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 8900 4250 50  0001 C CNN
F 1 "GND" H 8905 4327 50  0000 C CNN
F 2 "" H 8900 4500 50  0001 C CNN
F 3 "" H 8900 4500 50  0001 C CNN
	1    8900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3700 7400 3450
Wire Wire Line
	8900 3500 8900 3800
Wire Wire Line
	8900 4450 8900 4500
Wire Wire Line
	8050 3800 8600 3800
Connection ~ 8900 3800
Wire Wire Line
	8050 3600 8100 3600
Wire Wire Line
	8100 3600 8100 3450
$Comp
L power:GND #PWR?
U 1 1 5DB2080E
P 8250 3500
AR Path="/5DB2080E" Ref="#PWR?"  Part="1" 
AR Path="/5DAFC521/5DB2080E" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 8250 3250 50  0001 C CNN
F 1 "GND" H 8255 3327 50  0000 C CNN
F 2 "" H 8250 3500 50  0001 C CNN
F 3 "" H 8250 3500 50  0001 C CNN
	1    8250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3450 8100 3450
Wire Wire Line
	8100 3450 8250 3450
Wire Wire Line
	8250 3450 8250 3500
Connection ~ 8100 3450
Wire Wire Line
	8900 3200 8900 3100
Connection ~ 6450 3100
Wire Wire Line
	8650 4100 8900 4100
Connection ~ 8900 4100
Wire Wire Line
	8900 4100 8900 4150
Wire Wire Line
	6450 4150 6450 4100
Connection ~ 6450 4100
Wire Wire Line
	5850 4100 6450 4100
Wire Wire Line
	6050 3950 6050 4150
Connection ~ 6050 3950
Wire Wire Line
	5850 3950 6050 3950
Wire Wire Line
	6450 3100 7700 3100
Wire Wire Line
	7700 3100 7700 3000
Connection ~ 7700 3100
Wire Wire Line
	7700 3100 8900 3100
Wire Notes Line
	9200 2800 9200 5050
Wire Notes Line
	9200 5050 5500 5050
Wire Notes Line
	5500 5050 5500 2800
Wire Notes Line
	5500 2800 9200 2800
Text Notes 7100 2750 2    50   ~ 0
Encodeur Rotatif
Wire Wire Line
	6050 3600 6050 3950
Wire Wire Line
	6450 3800 6450 4100
Wire Wire Line
	8900 3800 8900 4100
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5DB20844
P 7050 4200
AR Path="/5DB20844" Ref="J?"  Part="1" 
AR Path="/5DAFC521/5DB20844" Ref="J18"  Part="1" 
F 0 "J18" H 6968 4517 50  0000 C CNN
F 1 "Conn_ROTENDT" H 6968 4426 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0343_1x03_1MP_P1.50mm_Horizontal" H 7050 4200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13c4/0900766b813c4ad0.pdf" H 7050 4200 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 6706502" H 7050 4200 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 7050 4200 50  0001 C CNN "Catégorie"
F 6 "670-6502" H 7050 4200 50  0001 C CNN "Code Fournisseur"
F 7 "10" H 7050 4200 50  0001 C CNN "Conditionnement"
F 8 "3 A" H 7050 4200 50  0001 C CNN "Courant"
F 9 "Embase pour CI Molex, Pico-SPOX, 3 pôles , 1.5mm 1 rangée, 3A, Angle droit" H 7050 4200 50  0001 C CNN "Description"
F 10 "RS" H 7050 4200 50  0001 C CNN "Fournisseur"
F 11 "1,5mm" H 7050 4200 50  0001 C CNN "Height"
F 12 "Molex" H 7050 4200 50  0001 C CNN "Manufacturer_Name"
F 13 "87438-0343" H 7050 4200 50  0001 C CNN "Manufacturer_Part_Number"
F 14 "5,49" H 7050 4200 50  0001 C CNN "Prix Conditionnement"
F 15 "0,549" H 7050 4200 50  0001 C CNN "Prix Unitaire"
F 16 " 670-6502" H 7050 4200 50  0001 C CNN "RS Part Number"
F 17 "350 V(cc/ca)" H 7050 4200 50  0001 C CNN "Tension"
F 18 "~" H 7050 4200 50  0001 C CNN "Tolérence"
F 19 "~" H 7050 4200 50  0001 C CNN "Puissance"
	1    7050 4200
	0    -1   1    0   
$EndComp
Wire Wire Line
	6950 4000 6950 3600
Connection ~ 6950 3600
Wire Wire Line
	6950 3600 6050 3600
Wire Wire Line
	7050 4000 7050 3700
Wire Wire Line
	7050 3700 7400 3700
Connection ~ 7400 3700
Wire Wire Line
	7150 4000 7150 3800
Connection ~ 7150 3800
Wire Wire Line
	7150 3800 7450 3800
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5DB20863
P 8600 3250
AR Path="/5D7EDDD9/5DB20863" Ref="J?"  Part="1" 
AR Path="/5DB20863" Ref="J?"  Part="1" 
AR Path="/5DAFC521/5DB20863" Ref="J19"  Part="1" 
F 0 "J19" H 8680 3242 50  0000 L CNN
F 1 "Conn_ROTENSW" V 8850 2950 50  0000 L CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0243-_1x02_1MP_P1.50mm_Horizontal" H 8600 3250 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/144c/0900766b8144c299.pdf" H 8600 3250 50  0001 C CNN
F 4 "Connecteur" H 8600 3250 50  0001 C CNN "Catégorie"
F 5 " 896-7500" H 8600 3250 50  0001 C CNN "Code Fournisseur"
F 6 "10" H 8600 3250 50  0001 C CNN "Conditionnement"
F 7 "2,5 A" H 8600 3250 50  0001 C CNN "Courant"
F 8 "Embase pour CI Molex, Pico-SPOX, 2 pôles , 1.5mm 1 rangée, 2.5A, Verticale" H 8600 3250 50  0001 C CNN "Description"
F 9 "RS" H 8600 3250 50  0001 C CNN "Fournisseur"
F 10 "1,5mm" H 8600 3250 50  0001 C CNN "Height"
F 11 "Molex" H 8600 3250 50  0001 C CNN "Manufacturer_Name"
F 12 "87437-0243" H 8600 3250 50  0001 C CNN "Manufacturer_Part_Number"
F 13 "4,51" H 8600 3250 50  0001 C CNN "Prix Conditionnement"
F 14 "0,451" H 8600 3250 50  0001 C CNN "Prix Unitaire"
F 15 "~" H 8600 3250 50  0001 C CNN "Puissance"
F 16 "896-7500" H 8600 3250 50  0001 C CNN "RS Part Number"
F 17 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/8967500/" H 8600 3250 50  0001 C CNN "RS Price/Stock"
F 18 "250 V(cc/ca)" H 8600 3250 50  0001 C CNN "Tension"
F 19 "~" H 8600 3250 50  0001 C CNN "Tolérence"
	1    8600 3250
	0    1    -1   0   
$EndComp
Connection ~ 8600 3800
Wire Wire Line
	8600 3800 8900 3800
Wire Wire Line
	8600 3450 8600 3800
Wire Wire Line
	8500 3450 8250 3450
Connection ~ 8250 3450
Text HLabel 5850 3950 0    50   Output ~ 0
ROTENA
Text HLabel 5850 4100 0    50   Output ~ 0
ROTENB
Text HLabel 8650 4100 0    50   Output ~ 0
ROTENSW
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5DB146B5
P 2850 3650
AR Path="/5DB146B5" Ref="J?"  Part="1" 
AR Path="/5DAFC521/5DB146B5" Ref="J1"  Part="1" 
F 0 "J1" H 2768 3967 50  0000 C CNN
F 1 "Conn_NeoPixel" H 2768 3876 50  0000 C CNN
F 2 "Misc_KropoteX:Molex_Pico-Spox_87438-0343_1x03_1MP_P1.50mm_Horizontal" H 2850 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13c4/0900766b813c4ad0.pdf" H 2850 3650 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/ 6706502" H 2850 3650 50  0001 C CNN "RS Price/Stock"
F 5 "Connecteur" H 2850 3650 50  0001 C CNN "Catégorie"
F 6 "670-6502" H 2850 3650 50  0001 C CNN "Code Fournisseur"
F 7 "10" H 2850 3650 50  0001 C CNN "Conditionnement"
F 8 "3 A" H 2850 3650 50  0001 C CNN "Courant"
F 9 "Embase pour CI Molex, Pico-SPOX, 3 pôles , 1.5mm 1 rangée, 3A, Angle droit" H 2850 3650 50  0001 C CNN "Description"
F 10 "RS" H 2850 3650 50  0001 C CNN "Fournisseur"
F 11 "1,5mm" H 2850 3650 50  0001 C CNN "Height"
F 12 "Molex" H 2850 3650 50  0001 C CNN "Manufacturer_Name"
F 13 "87438-0343" H 2850 3650 50  0001 C CNN "Manufacturer_Part_Number"
F 14 "5,49" H 2850 3650 50  0001 C CNN "Prix Conditionnement"
F 15 "0,549" H 2850 3650 50  0001 C CNN "Prix Unitaire"
F 16 " 670-6502" H 2850 3650 50  0001 C CNN "RS Part Number"
F 17 "350 V(cc/ca)" H 2850 3650 50  0001 C CNN "Tension"
F 18 "~" H 2850 3650 50  0001 C CNN "Tolérence"
F 19 "~" H 2850 3650 50  0001 C CNN "Puissance"
	1    2850 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3200 3550 3050 3550
Wire Wire Line
	3050 3750 3100 3750
Text Label 3800 3650 0    50   ~ 0
VDD
Wire Notes Line
	2650 3250 4000 3250
Wire Notes Line
	4000 3250 4000 4400
Wire Notes Line
	4000 4400 2650 4400
Wire Notes Line
	2650 4400 2650 3250
Text Notes 2950 3150 0    50   ~ 0
Connecteur NeoPixel
Wire Wire Line
	3100 3750 3100 4100
Wire Wire Line
	3050 3650 3250 3650
Wire Wire Line
	3250 3650 3250 3750
$Comp
L Device:C C?
U 1 1 5DB146D6
P 3250 3900
AR Path="/5DB146D6" Ref="C?"  Part="1" 
AR Path="/5D7EDC34/5DB146D6" Ref="C?"  Part="1" 
AR Path="/5DAFC521/5DB146D6" Ref="C1"  Part="1" 
F 0 "C1" H 3365 3946 50  0000 L CNN
F 1 "100n" H 3365 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3288 3750 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13ec/0900766b813ece38.pdf" H 3250 3900 50  0001 C CNN
F 4 "698-3251" H 3250 3900 50  0001 C CNN "Code Fournisseur"
F 5 "100" H 3250 3900 50  0001 C CNN "Conditionnement"
F 6 "MLCC, CMS, 100nF, ±10%, 50V cc, diélectrique : X7R, boitier 0603 (1608M)" H 3250 3900 50  0001 C CNN "Description"
F 7 "RS" H 3250 3900 50  0001 C CNN "Fournisseur"
F 8 "1,10" H 3250 3900 50  0001 C CNN "Prix Conditionnement"
F 9 "0,011" H 3250 3900 50  0001 C CNN "Prix Unitaire"
F 10 "~" H 3250 3900 50  0001 C CNN "Puissance"
F 11 "50 V c. c." H 3250 3900 50  0001 C CNN "Tension"
F 12 "±10%" H 3250 3900 50  0001 C CNN "Tolérence"
F 13 "~" H 3250 3900 50  0001 C CNN "Documentation Interne"
F 14 "Passif" H 3250 3900 50  0001 C CNN "Catégorie"
F 15 "https://fr.rs-online.com/web/p/condensateurs-ceramique-multicouches/6983251" H 3250 3900 50  0001 C CNN "RS Price/Stock"
F 16 "1.6 x 0.8 x 0.8mm" H 3250 3900 50  0001 C CNN "Height"
F 17 "AVX" H 3250 3900 50  0001 C CNN "Manufacturer_Name"
F 18 "06035C104K4T2A" H 3250 3900 50  0001 C CNN "Manufacturer_Part_Number"
F 19 "698-3251" H 3250 3900 50  0001 C CNN "RS Part Number"
	1    3250 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DB146DC
P 3400 4150
AR Path="/5DB146DC" Ref="#PWR?"  Part="1" 
AR Path="/5DAFC521/5DB146DC" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 3400 3900 50  0001 C CNN
F 1 "GND" H 3405 3977 50  0000 C CNN
F 2 "" H 3400 4150 50  0001 C CNN
F 3 "" H 3400 4150 50  0001 C CNN
	1    3400 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4100 3250 4100
Wire Wire Line
	3250 4100 3250 4050
Wire Wire Line
	3250 3650 3650 3650
Connection ~ 3250 3650
$Comp
L Device:C C?
U 1 1 5DB146F5
P 3650 3900
AR Path="/5D7EDDD9/5DB146F5" Ref="C?"  Part="1" 
AR Path="/5DB146F5" Ref="C?"  Part="1" 
AR Path="/5DAFC521/5DB146F5" Ref="C27"  Part="1" 
F 0 "C27" H 3765 3946 50  0000 L CNN
F 1 "1µ" H 3765 3855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.4" H 3688 3750 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/13bc/0900766b813bccee.pdf" H 3650 3900 50  0001 C CNN
F 4 "Passif" H 3650 3900 50  0001 C CNN "Catégorie"
F 5 "844-0774" H 3650 3900 50  0001 C CNN "Code Fournisseur"
F 6 "50" H 3650 3900 50  0001 C CNN "Conditionnement"
F 7 "Condensateur électrolytique aluminium Nichicon, 1μF, 50V c.c., série WX" H 3650 3900 50  0001 C CNN "Description"
F 8 "RS" H 3650 3900 50  0001 C CNN "Fournisseur"
F 9 "3 (Dia.) x 5.4mm" H 3650 3900 50  0001 C CNN "Height"
F 10 "Nichicon" H 3650 3900 50  0001 C CNN "Manufacturer_Name"
F 11 "UWX1H010MCL2GB" H 3650 3900 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "4,85" H 3650 3900 50  0001 C CNN "Prix Conditionnement"
F 13 "0,097" H 3650 3900 50  0001 C CNN "Prix Unitaire"
F 14 "~" H 3650 3900 50  0001 C CNN "Puissance"
F 15 "844-0774" H 3650 3900 50  0001 C CNN "RS Part Number"
F 16 "https://fr.rs-online.com/web/products/8440774/" H 3650 3900 50  0001 C CNN "RS Price/Stock"
F 17 "50V c.c." H 3650 3900 50  0001 C CNN "Tension"
F 18 "±20%" H 3650 3900 50  0001 C CNN "Tolérence"
	1    3650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3750 3650 3650
Connection ~ 3650 3650
Wire Wire Line
	3650 3650 3800 3650
Wire Wire Line
	3650 4050 3650 4100
Connection ~ 3250 4100
Wire Wire Line
	3250 4100 3400 4100
Wire Wire Line
	3400 4100 3400 4150
Connection ~ 3400 4100
Wire Wire Line
	3400 4100 3650 4100
Text HLabel 3200 3550 2    50   Input ~ 0
NP_DATA
Wire Wire Line
	7700 3000 7300 3000
Text HLabel 7300 3000 0    50   Input ~ 0
VDD
$EndSCHEMATC
