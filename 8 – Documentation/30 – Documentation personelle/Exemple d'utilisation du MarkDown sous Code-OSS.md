# README

Table des Matières
[README](#readme)
  - [Présentation](#présentation)
  - [Gestion des numéros de version](#gestion-des-numéros-de-version)
  - [Phases de développement](#phases-de-développement)

[[TOC]]


## Présentation

Ce projet d'IHM est un projet Libre et OpenHardWare, développé (quasi, à des fins pédagogiques) uniquement avec des logiciels libre, conjointement avec le service électronique du laboratoire LAPLACE du CNRS de Toulouse et un apprenti technicien supérieur issu d'une formation ETD (Électronicien de Test et de Développement) à Balma.

**Présentation et description du projet d'IHM.**

------

## Gestion des numéros de version

Les numéros des version se compose comme suit:
v1.2-3.4:
1.  Numéro de la version de la carte (actuellement 0 mais en passe de devenir 1);
2. Numéro de release correspondant à la phase de développement et à la semaine en cours(actuellement la 1)
3. Numéro de Phase (actuellement phase 0 : Fixation du cahier des charges, va passé en phase 1 : Choix de la pré-BOM, puis en phase 3 dans la foulée avec le développement du PCB puis fabrication);
4. Numéro de semaine en cours;

## Phases de développement
- [x] 0. Reflexion sur les contraintes et ojectifs du PCB
- [x] 1. Choix d'une pré-BOM
- [x] 2. Développement du PCB
- [x] 3. Validation et fabrication du PCB
- [ ] 4. Réflexion sur les contraintes et objectifs des librairies
- [ ] 5. Écritures des librairies d'utilisation du µLCD, des NeoPixels et de l'encodeur rotatif
- [ ] 6. Développement d'une carte d'extension
- [ ] 7. Écriture d'une librairie pour une carte d'extension
- [ ] 8. Test
- [ ] 9. Écriture de Librairies supplémentaires

Extended Syntaxes
Admonition
Inspired by MkDocs

Nesting supported (by indent) admonition, the following shows a danger admonition nested by a note admonition.

!!! note

    This is the **note** admonition body

    !!! danger Danger Title
        This is the **danger** admonition body
admonition-demo

Removing Admonition Title
!!! danger ""
    This is the danger admonition body
admonition-demo

Supported Qualifiers
note | summary, abstract, tldr | info, todo | tip, hint | success, check, done | question, help, faq | warning, attention, caution | failure, fail, missing | danger, error, bug | example, snippet | quote, cite

!!! bug

!!! danger

!!! error

!!! tldr

!!! info

!!! tip

!!! success

!!! question

!!! warning

!!! attention

!!! example

!!! quote

Enhanced Anchor Link
Now, you're able to write anchor links consistent to heading texts.

Go to 
[简体中文](#简体中文), 
[Español Título](#Español-Título).

## 简体中文

Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Aenean euismod bibendum laoreet.

## Español Título

Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Aenean euismod bibendum laoreet.
markdown-it-table-of-contents
[[TOC]]
Markdown Extended Readme
Features
Requirements
Demos
markdown-it-footnote
Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.
[^longnote]: Here's one with multiple blocks.
Here is a footnote reference,[1] and another.[2]

markdown-it-abbr
*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium
The HTML specification
is maintained by the W3C.
The HTML specification is maintained by the W3C.

markdown-it-deflist
Apple
:   Pomaceous fruit of plants of the genus Malus in the family Rosaceae.
Apple
Pomaceous fruit of plants of the genus Malus in the family Rosaceae.
markdown-it-sup markdown-it-sub
29^th^, H~2~O
29th, H2O

markdown-it-checkbox
[ ] unchecked
[x] checked
unchecked checked

markdown-it-attrs
item **bold red**{style="color:red"}
item bold red

markdown-it-kbd
[[Ctrl+Esc]]
Ctrl+Esc

markdown-it-underline
_underline_
underline

markdown-it-container

::::: container
:::: row
::: col-xs-6 alert alert-success
success text
:::
::: col-xs-6 alert alert-warning
warning text
:::
::::
:::::

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```
"markdown.styles": [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
]

---
puppeteer:
    pdf:
        format: A4
        displayHeaderFooter: true
        margin:
            top: 1cm
            right: 1cm
            bottom: 1cm
            left: 1cm
    image:
        quality: 90
        fullPage: true
---
contents goes here...


Column A | Column B | Column C
---------|----------|---------
 A1 | B1 | C1
 A2 | B2 | C2
 A3 | B3 | C3


> Fonction 1
>
>$$\cos x=\sum_{k=0}^{\infty}\frac{(-1)^k}{(2k)!}x^{2k}$$



>Fonction 2
>
>$$\left\{
  \begin{array}{rcr}
    x+2y & = & -1 \\
    -x+4y & = & 0 \\
  \end{array}
  \right.$$

> Valeur de l'hypotenus par le théorême de Pythagore
>
> $$c^2 = \sqrt{a^2 + b^2}$$

$$
x = \sin \left (\frac{1}{2} \right ) \\
y = \cos | x | \\
x = \sin \mathopen{}\left ( \frac{1}{2} \right )\mathclose{} \\
y = \arccos \mathopen{|} x \mathclose{|}
$$


$$c^2 = {{\sqrt{a^2 + b^2} \over{2 × \omega}}\over{\sqrt{2jcω}}}$$

 $$-c{\bold e}_x = a{\bold e}_\alpha + b\tilde{\bold e}_\alpha$$