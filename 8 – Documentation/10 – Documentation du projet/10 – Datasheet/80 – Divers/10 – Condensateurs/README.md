## DATASHEET des condensateurs

--------------------------

La nomenclature titres est la suivante :\
GGGGG - MMM - CCCC - TTTT - VVVcc - DDD - BBBB - (FFFFFFF optionnel)
* G         = Description du genre de condensateur / Série (MCC / MVE / WX)
* MMM       = Type de montage (CMS ou TRV)
* CCCC      = Capacité
* TTTT      = Tolérence
* VVVcc     = Tension (en Vcc)
* DDD       = Type du diélectrique
* BBBB      = Taille du boîtier
* F         = Nom du fabricant