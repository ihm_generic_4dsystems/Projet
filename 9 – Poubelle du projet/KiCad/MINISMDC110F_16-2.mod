PCBNEW-LibModule-V1  2019-09-29 08:37:36
# encoding utf-8
Units mm
$INDEX
FUSC4632X48N
$EndINDEX
$MODULE FUSC4632X48N
Po 0 0 0 15 5d905f40 00000000 ~~
Li FUSC4632X48N
Cd MINISMDC110F/16-2
Kw Fuse
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "F**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "FUSC4632X48N"
DS -3.025 -1.965 3.025 -1.965 0.05 24
DS 3.025 -1.965 3.025 1.965 0.05 24
DS 3.025 1.965 -3.025 1.965 0.05 24
DS -3.025 1.965 -3.025 -1.965 0.05 24
DS -2.3 -1.62 2.3 -1.62 0.1 24
DS 2.3 -1.62 2.3 1.62 0.1 24
DS 2.3 1.62 -2.3 1.62 0.1 24
DS -2.3 1.62 -2.3 -1.62 0.1 24
DS 0 -1.52 0 1.52 0.2 21
$PAD
Po -2.1 0
Sh "1" R 1.35 3.43 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.1 0
Sh "2" R 1.35 3.43 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE FUSC4632X48N
$EndLIBRARY
