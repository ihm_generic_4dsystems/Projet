EESchema Schematic File Version 4
LIBS:IHM-Générique-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Schéma électronique — IHM-Générique"
Date "2019-09-15"
Rev "0.1-0.2"
Comp "Laboratoire plasma et conversion d'énergie – CNRS – UPS — AFPA"
Comment1 "Licence CERN OHL v1.2"
Comment2 "Auteurs : Thomas TROUPEAU / Arnauld BIGANZOLI"
Comment3 "Interface Homme-Machine Multi-usage, multi-taille"
Comment4 ""
$EndDescr
$Comp
L IHM-Générique-rescue:TPA6205A1DGN-Misc_KropoteX IC1
U 1 1 5DAD6975
P 6450 3250
F 0 "IC1" H 6825 3415 50  0000 C CNN
F 1 "TPA6205A1DGN" H 6825 3324 50  0000 C CNN
F 2 "Misc_KropoteX:SOP65P490X110-9N" H 8575 3145 50  0001 C CNN
F 3 "" H 6450 3200 50  0001 C CNN
F 4 "Texas Instruments,TPA6205A1DGN,IC,Audio Texas Instruments TPA6205A1DGN Audio Amplifier IC, Class-AB Mono -70dB, 8-Pin MSOP" H 10455 3050 50  0001 C CNN "Description"
F 5 "1.1mm" H 7955 2950 50  0001 C CNN "Height"
F 6 "8124268P" H 8085 2845 50  0001 C CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/8124268P" H 8915 2750 50  0001 C CNN "RS Price/Stock"
F 8 "Texas Instruments" H 8240 2650 50  0001 C CNN "Manufacturer_Name"
F 9 "TPA6205A1DGN" H 8180 2550 50  0001 C CNN "Allied_Number"
F 10 "Circuit Intégré" H 6450 3250 50  0001 C CNN "Catégorie"
F 11 "RS" H 6450 3250 50  0001 C CNN "Fournisseur"
F 12 "~" H 6450 3250 50  0001 C CNN "Tension"
F 13 "~" H 6450 3250 50  0001 C CNN "Tolérence"
F 14 "~" H 6450 3250 50  0001 C CNN "Puissance"
	1    6450 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5DAE0A48
P 5550 3500
F 0 "C8" V 5298 3500 50  0000 C CNN
F 1 "220n" V 5389 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5588 3350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 5550 3500 50  0001 C CNN
F 4 "Passif" H 5550 3500 50  0001 C CNN "Catégorie"
F 5 "185-1905" H 5550 3500 50  0001 C CNN "Code Fournisseur"
F 6 "1000" H 5550 3500 50  0001 C CNN "Conditionnement"
F 7 "MLCC SMD 0201 0.22uF 10V X5R 10% General" H 5550 3500 50  0001 C CNN "Description"
F 8 "RS" H 5550 3500 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 5550 3500 50  0001 C CNN "Height"
F 10 "Murata" H 5550 3500 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 5550 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "11" H 5550 3500 50  0001 C CNN "Prix Conditionnement"
F 13 "0,011" H 5550 3500 50  0001 C CNN "Prix Unitaire"
F 14 "185-1905" H 5550 3500 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products" H 5550 3500 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 5550 3500 50  0001 C CNN "Tension"
F 17 "±10%" H 5550 3500 50  0001 C CNN "Tolérence"
F 18 "~" H 5550 3500 50  0001 C CNN "Puissance"
	1    5550 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5DAE4A78
P 5850 3500
F 0 "R9" V 6057 3500 50  0000 C CNN
F 1 "10k" V 5966 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5780 3500 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 5850 3500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 5850 3500 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 5850 3500 50  0001 C CNN "Puissance"
F 6 "~" H 5850 3500 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 5850 3500 50  0001 C CNN "Tolérence"
F 8 "Passif" H 5850 3500 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 5850 3500 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 5850 3500 50  0001 C CNN "Conditionnement"
F 11 "~" H 5850 3500 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 5850 3500 50  0001 C CNN "Description"
F 13 "RS" H 5850 3500 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 5850 3500 50  0001 C CNN "Height"
F 15 "RS PRO" H 5850 3500 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 5850 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 5850 3500 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 5850 3500 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 5850 3500 50  0001 C CNN "RS Part Number"
	1    5850 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R10
U 1 1 5DAE51BD
P 5850 3650
F 0 "R10" V 5650 3650 50  0000 C CNN
F 1 "10k" V 5750 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5780 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb5c.pdf" H 5850 3650 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048921" H 5850 3650 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 5850 3650 50  0001 C CNN "Puissance"
F 6 "~" H 5850 3650 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 5850 3650 50  0001 C CNN "Tolérence"
F 8 "Passif" H 5850 3650 50  0001 C CNN "Catégorie"
F 9 "804-8921" H 5850 3650 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 5850 3650 50  0001 C CNN "Conditionnement"
F 11 "~" H 5850 3650 50  0001 C CNN "Courant"
F 12 "Résistance CMS 10kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 5850 3650 50  0001 C CNN "Description"
F 13 "RS" H 5850 3650 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 5850 3650 50  0001 C CNN "Height"
F 15 "RS PRO" H 5850 3650 50  0001 C CNN "Manufacturer_Name"
F 16 " 804-8921" H 5850 3650 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,43" H 5850 3650 50  0001 C CNN "Prix Conditionnement"
F 18 "7,43" H 5850 3650 50  0001 C CNN "Prix Unitaire"
F 19 " 804-8921" H 5850 3650 50  0001 C CNN "RS Part Number"
	1    5850 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DAE7220
P 6800 4400
F 0 "R12" V 6593 4400 50  0000 C CNN
F 1 "100k" V 6684 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6730 4400 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb74.pdf" H 6800 4400 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8049000" H 6800 4400 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 6800 4400 50  0001 C CNN "Puissance"
F 6 "~" H 6800 4400 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 6800 4400 50  0001 C CNN "Tolérence"
F 8 "Passif" H 6800 4400 50  0001 C CNN "Catégorie"
F 9 "804-9000" H 6800 4400 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 6800 4400 50  0001 C CNN "Conditionnement"
F 11 "~" H 6800 4400 50  0001 C CNN "Courant"
F 12 "Résistance CMS 100kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 6800 4400 50  0001 C CNN "Description"
F 13 "RS" H 6800 4400 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 6800 4400 50  0001 C CNN "Height"
F 15 "RS PRO" H 6800 4400 50  0001 C CNN "Manufacturer_Name"
F 16 "804-9000" H 6800 4400 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,47" H 6800 4400 50  0001 C CNN "Prix Conditionnement"
F 18 "7,47" H 6800 4400 50  0001 C CNN "Prix Unitaire"
F 19 "804-9000" H 6800 4400 50  0001 C CNN "RS Part Number"
	1    6800 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5DAE994B
P 4950 4350
F 0 "#PWR0105" H 4950 4100 50  0001 C CNN
F 1 "GND" H 4955 4177 50  0000 C CNN
F 2 "" H 4950 4350 50  0001 C CNN
F 3 "" H 4950 4350 50  0001 C CNN
	1    4950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5DAE9D81
P 7550 4000
F 0 "#PWR0106" H 7550 3750 50  0001 C CNN
F 1 "GND" H 7555 3827 50  0000 C CNN
F 2 "" H 7550 4000 50  0001 C CNN
F 3 "" H 7550 4000 50  0001 C CNN
	1    7550 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3950 7550 3950
Wire Wire Line
	7550 3950 7550 4000
Wire Wire Line
	7550 3950 7550 3800
Connection ~ 7550 3950
Wire Wire Line
	7300 3800 7550 3800
Wire Wire Line
	7300 3350 7550 3350
Wire Wire Line
	6350 3650 6300 3650
Wire Wire Line
	5700 3650 5550 3650
Wire Wire Line
	5550 3650 5550 3950
Wire Wire Line
	5550 4250 5550 4300
Wire Wire Line
	4950 4300 4950 4350
Connection ~ 4950 4300
Wire Wire Line
	5750 3950 5750 3900
Wire Wire Line
	6300 3650 6300 4400
Wire Wire Line
	6300 4400 6650 4400
Wire Wire Line
	6950 4400 7400 4400
Wire Wire Line
	7400 4400 7400 3650
Wire Wire Line
	7400 3650 7300 3650
Wire Wire Line
	7300 3500 7400 3500
Wire Wire Line
	7400 3500 7400 3250
Wire Wire Line
	7400 2950 6950 2950
Wire Wire Line
	6650 2950 6300 2950
Wire Wire Line
	6300 2950 6300 3500
Wire Wire Line
	6300 3500 6350 3500
Wire Wire Line
	7300 4100 7300 3950
Wire Wire Line
	7400 4400 7900 4400
Connection ~ 7400 4400
Wire Wire Line
	5750 4300 5750 4250
$Comp
L Device:C C9
U 1 1 5DB09D72
P 5550 4100
F 0 "C9" H 5800 4050 50  0000 R CNN
F 1 "220n" H 5900 4150 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5588 3950 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 5550 4100 50  0001 C CNN
F 4 "Passif" H 5550 4100 50  0001 C CNN "Catégorie"
F 5 "185-1905" H 5550 4100 50  0001 C CNN "Code Fournisseur"
F 6 "1000" H 5550 4100 50  0001 C CNN "Conditionnement"
F 7 "MLCC SMD 0201 0.22uF 10V X5R 10% General" H 5550 4100 50  0001 C CNN "Description"
F 8 "RS" H 5550 4100 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 5550 4100 50  0001 C CNN "Height"
F 10 "Murata" H 5550 4100 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 5550 4100 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "11" H 5550 4100 50  0001 C CNN "Prix Conditionnement"
F 13 "0,011" H 5550 4100 50  0001 C CNN "Prix Unitaire"
F 14 "185-1905" H 5550 4100 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products" H 5550 4100 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 5550 4100 50  0001 C CNN "Tension"
F 17 "±10%" H 5550 4100 50  0001 C CNN "Tolérence"
F 18 "~" H 5550 4100 50  0001 C CNN "Puissance"
	1    5550 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C10
U 1 1 5DB0A3C1
P 5750 4100
F 0 "C10" H 5635 4054 50  0000 R CNN
F 1 "220n" H 5635 4145 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5788 3950 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/16ee/0900766b816ee884.pdf" H 5750 4100 50  0001 C CNN
F 4 "Passif" H 5750 4100 50  0001 C CNN "Catégorie"
F 5 "185-1905" H 5750 4100 50  0001 C CNN "Code Fournisseur"
F 6 "1000" H 5750 4100 50  0001 C CNN "Conditionnement"
F 7 "MLCC SMD 0201 0.22uF 10V X5R 10% General" H 5750 4100 50  0001 C CNN "Description"
F 8 "RS" H 5750 4100 50  0001 C CNN "Fournisseur"
F 9 "0.6 x 0.3 x 0.3mm" H 5750 4100 50  0001 C CNN "Height"
F 10 "Murata" H 5750 4100 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM033R61A224KE90D" H 5750 4100 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "11" H 5750 4100 50  0001 C CNN "Prix Conditionnement"
F 13 "0,011" H 5750 4100 50  0001 C CNN "Prix Unitaire"
F 14 "185-1905" H 5750 4100 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products" H 5750 4100 50  0001 C CNN "RS Price/Stock"
F 16 "10 V c. c." H 5750 4100 50  0001 C CNN "Tension"
F 17 "±10%" H 5750 4100 50  0001 C CNN "Tolérence"
F 18 "~" H 5750 4100 50  0001 C CNN "Puissance"
	1    5750 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5DB20780
P 6800 2950
F 0 "R11" V 6593 2950 50  0000 C CNN
F 1 "100k" V 6684 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6730 2950 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/157c/0900766b8157cb74.pdf" H 6800 2950 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8049000" H 6800 2950 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 6800 2950 50  0001 C CNN "Puissance"
F 6 "~" H 6800 2950 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 6800 2950 50  0001 C CNN "Tolérence"
F 8 "Passif" H 6800 2950 50  0001 C CNN "Catégorie"
F 9 "804-9000" H 6800 2950 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 6800 2950 50  0001 C CNN "Conditionnement"
F 11 "~" H 6800 2950 50  0001 C CNN "Courant"
F 12 "Résistance CMS 100kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 6800 2950 50  0001 C CNN "Description"
F 13 "RS" H 6800 2950 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 6800 2950 50  0001 C CNN "Height"
F 15 "RS PRO" H 6800 2950 50  0001 C CNN "Manufacturer_Name"
F 16 "804-9000" H 6800 2950 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "7,47" H 6800 2950 50  0001 C CNN "Prix Conditionnement"
F 18 "7,47" H 6800 2950 50  0001 C CNN "Prix Unitaire"
F 19 "804-9000" H 6800 2950 50  0001 C CNN "RS Part Number"
	1    6800 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5DB216F9
P 4550 3500
F 0 "R8" V 4757 3500 50  0000 C CNN
F 1 "1k" V 4666 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4480 3500 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 4550 3500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 4550 3500 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4550 3500 50  0001 C CNN "Puissance"
F 6 "~" H 4550 3500 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4550 3500 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4550 3500 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 4550 3500 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4550 3500 50  0001 C CNN "Conditionnement"
F 11 "~" H 4550 3500 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4550 3500 50  0001 C CNN "Description"
F 13 "RS" H 4550 3500 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4550 3500 50  0001 C CNN "Height"
F 15 "RS PRO" H 4550 3500 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 4550 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 4550 3500 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 4550 3500 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 4550 3500 50  0001 C CNN "RS Part Number"
	1    4550 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 5DB21DA9
P 4800 3800
F 0 "C7" H 5050 3750 50  0000 R CNN
F 1 "6.8n" H 5150 3850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4838 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0c22/0900766b80c22d2b.pdf" H 4800 3800 50  0001 C CNN
F 4 "Passif" H 4800 3800 50  0001 C CNN "Catégorie"
F 5 "140-543" H 4800 3800 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 4800 3800 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 25V cc, diélectrique : B, boitier 0402" H 4800 3800 50  0001 C CNN "Description"
F 8 "RS" H 4800 3800 50  0001 C CNN "Fournisseur"
F 9 "1 x 0.5 x 0.5mm" H 4800 3800 50  0001 C CNN "Height"
F 10 "Murata" H 4800 3800 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM155B11E682KA01D" H 4800 3800 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,5" H 4800 3800 50  0001 C CNN "Prix Conditionnement"
F 13 "0,005" H 4800 3800 50  0001 C CNN "Prix Unitaire"
F 14 "140-543" H 4800 3800 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/140543/" H 4800 3800 50  0001 C CNN "RS Price/Stock"
F 16 "25 V c. c." H 4800 3800 50  0001 C CNN "Tension"
F 17 "±10%" H 4800 3800 50  0001 C CNN "Tolérence"
F 18 "~" H 4800 3800 50  0001 C CNN "Puissance"
	1    4800 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 3500 4800 3500
Wire Wire Line
	4800 3500 4800 3650
Connection ~ 4800 3500
$Comp
L Device:R R7
U 1 1 5DB28211
P 4000 3500
F 0 "R7" V 4207 3500 50  0000 C CNN
F 1 "1k" V 4116 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3930 3500 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 4000 3500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 4000 3500 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 4000 3500 50  0001 C CNN "Puissance"
F 6 "~" H 4000 3500 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 4000 3500 50  0001 C CNN "Tolérence"
F 8 "Passif" H 4000 3500 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 4000 3500 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 4000 3500 50  0001 C CNN "Conditionnement"
F 11 "~" H 4000 3500 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 4000 3500 50  0001 C CNN "Description"
F 13 "RS" H 4000 3500 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 4000 3500 50  0001 C CNN "Height"
F 15 "RS PRO" H 4000 3500 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 4000 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 4000 3500 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 4000 3500 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 4000 3500 50  0001 C CNN "RS Part Number"
	1    4000 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C6
U 1 1 5DB28446
P 4300 3800
F 0 "C6" H 4550 3750 50  0000 R CNN
F 1 "6.8n" H 4650 3850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4338 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0c22/0900766b80c22d2b.pdf" H 4300 3800 50  0001 C CNN
F 4 "Passif" H 4300 3800 50  0001 C CNN "Catégorie"
F 5 "140-543" H 4300 3800 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 4300 3800 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 25V cc, diélectrique : B, boitier 0402" H 4300 3800 50  0001 C CNN "Description"
F 8 "RS" H 4300 3800 50  0001 C CNN "Fournisseur"
F 9 "1 x 0.5 x 0.5mm" H 4300 3800 50  0001 C CNN "Height"
F 10 "Murata" H 4300 3800 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM155B11E682KA01D" H 4300 3800 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,5" H 4300 3800 50  0001 C CNN "Prix Conditionnement"
F 13 "0,005" H 4300 3800 50  0001 C CNN "Prix Unitaire"
F 14 "140-543" H 4300 3800 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/140543/" H 4300 3800 50  0001 C CNN "RS Price/Stock"
F 16 "25 V c. c." H 4300 3800 50  0001 C CNN "Tension"
F 17 "±10%" H 4300 3800 50  0001 C CNN "Tolérence"
F 18 "~" H 4300 3800 50  0001 C CNN "Puissance"
	1    4300 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 3500 4300 3500
Wire Wire Line
	4300 3500 4300 3650
Connection ~ 4300 3500
Wire Wire Line
	4300 3500 4400 3500
Wire Notes Line
	2600 3150 2600 4050
Wire Notes Line
	2600 4050 4950 4050
Wire Notes Line
	4950 4050 4950 3200
Wire Notes Line
	4950 3200 2600 3150
Text Notes 4100 3150 0    50   ~ 0
Filtre passe-bas\n  23,405 KHz
Wire Wire Line
	6000 3500 6300 3500
Connection ~ 6300 3500
Wire Wire Line
	6000 3650 6300 3650
Connection ~ 6300 3650
Wire Wire Line
	5750 3900 6350 3900
Wire Wire Line
	6200 4050 6200 4600
Wire Wire Line
	6200 4050 6350 4050
Wire Wire Line
	4300 3950 4300 4100
Wire Wire Line
	4300 4100 4550 4100
Wire Wire Line
	4800 4100 4800 3950
Wire Wire Line
	4550 4100 4550 4300
Wire Wire Line
	4550 4300 4950 4300
Connection ~ 4550 4100
Wire Wire Line
	4550 4100 4800 4100
Connection ~ 5550 4300
Wire Wire Line
	5550 4300 5750 4300
Wire Wire Line
	4800 3500 5400 3500
Wire Wire Line
	4950 4300 5550 4300
Wire Notes Line
	5150 3200 5150 4350
Wire Notes Line
	5150 4350 6100 4350
Wire Notes Line
	6100 4350 6100 3200
Wire Notes Line
	6100 3200 5150 3200
Text Notes 5350 3150 0    50   ~ 0
Filtre passe-haut\n   72,343 KHz
$Comp
L Connector:AudioJack3 J3
U 1 1 5DBBA3D8
P 8850 3850
F 0 "J3" H 8570 3875 50  0000 R CNN
F 1 "AudioJack3" H 8570 3784 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3535NG_Horizontal" H 8850 3850 50  0001 C CNN
F 3 "~" H 8850 3850 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8786800" H 8850 3850 50  0001 C CNN "RS Price/Stock"
F 5 "~" H 8850 3850 50  0001 C CNN "Puissance"
F 6 "250 V c.a." H 8850 3850 50  0001 C CNN "Tension"
F 7 "~" H 8850 3850 50  0001 C CNN "Tolérence"
F 8 "RS" H 8850 3850 50  0001 C CNN "Fournisseur"
F 9 "Connecteur" H 8850 3850 50  0001 C CNN "Catégorie"
F 10 "878-6800" H 8850 3850 50  0001 C CNN "Code Fournisseur"
F 11 "50" H 8850 3850 50  0001 C CNN "Conditionnement"
F 12 "Stéréo, 3,5 mm Montage sur CI Droit, 250 V c.a. série 35RAPC 2" H 8850 3850 50  0001 C CNN "Description"
F 13 "3,5 mm" H 8850 3850 50  0001 C CNN "Height"
F 14 "Switchcraft" H 8850 3850 50  0001 C CNN "Manufacturer_Name"
F 15 "35RAPC2AV" H 8850 3850 50  0001 C CNN "Manufacturer_Part_Number"
F 16 "43,05" H 8850 3850 50  0001 C CNN "Prix Conditionnement"
F 17 "0,861" H 8850 3850 50  0001 C CNN "Prix Unitaire"
F 18 "878-6800" H 8850 3850 50  0001 C CNN "RS Part Number"
	1    8850 3850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D90EF87
P 3450 3500
F 0 "R?" V 3657 3500 50  0000 C CNN
F 1 "1k" V 3566 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3380 3500 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 3450 3500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 3450 3500 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 3450 3500 50  0001 C CNN "Puissance"
F 6 "~" H 3450 3500 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 3450 3500 50  0001 C CNN "Tolérence"
F 8 "Passif" H 3450 3500 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 3450 3500 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 3450 3500 50  0001 C CNN "Conditionnement"
F 11 "~" H 3450 3500 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 3450 3500 50  0001 C CNN "Description"
F 13 "RS" H 3450 3500 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 3450 3500 50  0001 C CNN "Height"
F 15 "RS PRO" H 3450 3500 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 3450 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 3450 3500 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 3450 3500 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 3450 3500 50  0001 C CNN "RS Part Number"
	1    3450 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5D90EF9C
P 3700 3800
F 0 "C?" H 3950 3750 50  0000 R CNN
F 1 "6.8n" H 4050 3850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3738 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0c22/0900766b80c22d2b.pdf" H 3700 3800 50  0001 C CNN
F 4 "Passif" H 3700 3800 50  0001 C CNN "Catégorie"
F 5 "140-543" H 3700 3800 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 3700 3800 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 25V cc, diélectrique : B, boitier 0402" H 3700 3800 50  0001 C CNN "Description"
F 8 "RS" H 3700 3800 50  0001 C CNN "Fournisseur"
F 9 "1 x 0.5 x 0.5mm" H 3700 3800 50  0001 C CNN "Height"
F 10 "Murata" H 3700 3800 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM155B11E682KA01D" H 3700 3800 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,5" H 3700 3800 50  0001 C CNN "Prix Conditionnement"
F 13 "0,005" H 3700 3800 50  0001 C CNN "Prix Unitaire"
F 14 "140-543" H 3700 3800 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/140543/" H 3700 3800 50  0001 C CNN "RS Price/Stock"
F 16 "25 V c. c." H 3700 3800 50  0001 C CNN "Tension"
F 17 "±10%" H 3700 3800 50  0001 C CNN "Tolérence"
F 18 "~" H 3700 3800 50  0001 C CNN "Puissance"
	1    3700 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 3500 3700 3500
Wire Wire Line
	3700 3500 3700 3650
Connection ~ 3700 3500
$Comp
L Device:R R?
U 1 1 5D90EFB5
P 2900 3500
F 0 "R?" V 3107 3500 50  0000 C CNN
F 1 "1k" V 3016 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2830 3500 50  0001 C CNN
F 3 "https://fr.rs-online.com/web/c/connecteurs/connecteurs-pour-circuit-imprime/embases-circuits-imprimes/?applied-dimensions=4294555622,4294562568,4294497692,4294561424,4291038784" H 2900 3500 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/products/8048845" H 2900 3500 50  0001 C CNN "RS Price/Stock"
F 5 "0,1 W" H 2900 3500 50  0001 C CNN "Puissance"
F 6 "~" H 2900 3500 50  0001 C CNN "Tension"
F 7 "±1% – ±100ppm/°C" H 2900 3500 50  0001 C CNN "Tolérence"
F 8 "Passif" H 2900 3500 50  0001 C CNN "Catégorie"
F 9 "804-8845" H 2900 3500 50  0001 C CNN "Code Fournisseur"
F 10 "1×5000" H 2900 3500 50  0001 C CNN "Conditionnement"
F 11 "~" H 2900 3500 50  0001 C CNN "Courant"
F 12 "Résistance CMS 1kΩ ±1%, boitier 0603 (1608M) 0.1W, ±100ppm/°C" H 2900 3500 50  0001 C CNN "Description"
F 13 "RS" H 2900 3500 50  0001 C CNN "Fournisseur"
F 14 "1,6×0,8mm" H 2900 3500 50  0001 C CNN "Height"
F 15 "RS PRO" H 2900 3500 50  0001 C CNN "Manufacturer_Name"
F 16 "804-8845" H 2900 3500 50  0001 C CNN "Manufacturer_Part_Number"
F 17 "8,17" H 2900 3500 50  0001 C CNN "Prix Conditionnement"
F 18 "8,17" H 2900 3500 50  0001 C CNN "Prix Unitaire"
F 19 "804-8845" H 2900 3500 50  0001 C CNN "RS Part Number"
	1    2900 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5D90EFCA
P 3200 3800
F 0 "C?" H 3450 3750 50  0000 R CNN
F 1 "6.8n" H 3550 3850 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3238 3650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0c22/0900766b80c22d2b.pdf" H 3200 3800 50  0001 C CNN
F 4 "Passif" H 3200 3800 50  0001 C CNN "Catégorie"
F 5 "140-543" H 3200 3800 50  0001 C CNN "Code Fournisseur"
F 6 "100" H 3200 3800 50  0001 C CNN "Conditionnement"
F 7 "MLCC, CMS, 6,8nF, ±10%, 25V cc, diélectrique : B, boitier 0402" H 3200 3800 50  0001 C CNN "Description"
F 8 "RS" H 3200 3800 50  0001 C CNN "Fournisseur"
F 9 "1 x 0.5 x 0.5mm" H 3200 3800 50  0001 C CNN "Height"
F 10 "Murata" H 3200 3800 50  0001 C CNN "Manufacturer_Name"
F 11 "GRM155B11E682KA01D" H 3200 3800 50  0001 C CNN "Manufacturer_Part_Number"
F 12 "0,5" H 3200 3800 50  0001 C CNN "Prix Conditionnement"
F 13 "0,005" H 3200 3800 50  0001 C CNN "Prix Unitaire"
F 14 "140-543" H 3200 3800 50  0001 C CNN "RS Part Number"
F 15 "https://fr.rs-online.com/web/products/140543/" H 3200 3800 50  0001 C CNN "RS Price/Stock"
F 16 "25 V c. c." H 3200 3800 50  0001 C CNN "Tension"
F 17 "±10%" H 3200 3800 50  0001 C CNN "Tolérence"
F 18 "~" H 3200 3800 50  0001 C CNN "Puissance"
	1    3200 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3050 3500 3200 3500
Wire Wire Line
	3200 3500 3200 3650
Connection ~ 3200 3500
Wire Wire Line
	3200 3500 3300 3500
Wire Wire Line
	3200 3950 3200 4100
Wire Wire Line
	3200 4100 3450 4100
Wire Wire Line
	3700 4100 3700 3950
Wire Wire Line
	3450 4100 3450 4300
Connection ~ 3450 4100
Wire Wire Line
	3450 4100 3700 4100
Wire Wire Line
	3700 3500 3850 3500
Wire Wire Line
	3450 4300 4550 4300
Connection ~ 4550 4300
$EndSCHEMATC
