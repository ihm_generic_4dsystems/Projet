# Conception détaillée

-----

**C'est ici que nous trouverons la documentation liée à la conception qui interesse directement au client**

Par exemple, c'est ici que se situe le Dossier Justificatif de Décision, les simulations, les schémas, les nomenclatures, la BOM, ainsi que toutes les données concernant les technologies misent en œuvre.
